﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-21-2015
// ***********************************************************************
// <copyright file="Enumerations.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Enumerating all the modes for deleting a link.
    /// </summary>
    public enum DeleteLinkMode
    {
        /// <summary>Elimina solo il file link</summary>
        OnlyLink,
        /// <summary>Elimina il file link e l'elemento puntato (file o cartella)</summary>
        LinkAndTarget,
        /// <summary>Elimina il file link e l'elemento puntato, (se è un file cancella anche la cartella padre altrimenti solo la cartella)</summary>
        LinkTargetFileAndFolderParent,
    }
}