﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-21-2015
// ***********************************************************************
// <copyright file="Enumerations.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Enumerating all the possible actions that can be done for a copy of one or more elements of the file system.
    /// </summary>
    public enum ActionsCopy
    {
        /// <summary>
        /// No setting
        /// </summary>
        None,
        /// <summary>
        /// The result of the copy of one or more file system objects will be the same number of new compressed file system objects.
        /// </summary>
        Compress,
        /// <summary>
        /// The result of the copy of one or more file system objects will be a new compressed file system objects.
        /// </summary>
        CompressInSingleFile,
        /// <summary>
        /// The result of the copy of a previously file system object compressed will be a new  file system object decompressed
        /// </summary>
        Extract,
        /// <summary>
        /// The result of the copy will be encrypted
        /// </summary>
        Encrypt,
        /// <summary>
        /// The result of the copy will be decrypted (only if it was previously encrypted)
        /// </summary>
        Decrypt,
        /// <summary>
        /// The result of the copy will be a new compressed and encrypted file system objects
        /// </summary>
        CompressEncrypt,
        /// <summary>
        /// The result of the copy of a previously file system object compressed and encrypted will be a new file system object decompressed and decrypted.
        /// </summary>
        ExtractDecrypt,
        
        
    }


    /// <summary>
    /// Enumerating all the possible results of the copy of a file system object
    /// </summary>
    public enum ResultCopyCompress
    {
        /// <summary>
        /// The result wil be a single file (only for <see cref="Mlc.Shell.IO.ActionsCopy.Compress"/>)
        /// </summary>
        SingleObject,
        /// <summary>
        /// The result wil be a multi file (only for <see cref="Mlc.Shell.IO.ActionsCopy.Compress"/>)
        /// </summary>
        MultiObject,
    }

}