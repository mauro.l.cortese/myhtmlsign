// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 01-26-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-26-2016
// ***********************************************************************
// <copyright file="ItemSettingAttribute.cs" company="Personale">
//     Copyright � Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class ClientSettingAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ClientSettingAttribute : Attribute
    {
    }

    /// <summary>
    /// Class RoamingUserSettingAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RoamingUserSettingAttribute : Attribute
    {
    }

    /// <summary>
    /// Class ServerSettingAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ServerSettingAttribute : Attribute
    {
    }

    /// <summary>
    /// Class SubSettingsAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SubSettingsAttribute : Attribute
    {
    }
    
    /// <summary>
    /// Class SubSettingsAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CryptoSettingsAttribute : Attribute
    {
    }
}
