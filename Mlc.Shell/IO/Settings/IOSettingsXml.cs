using System;
using System.IO;
using System.Data.Common;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Serializza o deserializza un'istanza che eredita da <see cref="Mlc.Shell.IO.SettingBase"/>
    /// </summary>
    internal class IOSettingsXml
    {
        private const string STR_DbConnectionssetting = "{0}\\DbConnections.setting";
        private const string STR_DbConnections = "DbConnections";
        private const string STR_DbConnectionSet = "DbConnectionSet";
        private const string STR_Name = "Name";
        private const string STR_DbConnection = "DbConnection";
        private const string STR_Password = "password";
        private const string STR_Key = "Key";
        private const string STR_Value = "Value";
        private const string STR_KeyValue = "KeyValue";
        Dictionary<string, ListProperty> clientSettingFamily = new Dictionary<string, ListProperty>();
        Dictionary<string, ListProperty> userRoamingSettingFamily = new Dictionary<string, ListProperty>();
        Dictionary<string, ListProperty> serverSettingFamily = new Dictionary<string, ListProperty>();
        private SettingBase settings = null;

        internal IOSettingsXml(SettingBase settings)
        {
            this.settings = settings;
        }

        internal bool Deserialize(SettingFiles settingFiles)
        {
            if (!test()) return false;
            this.readSettingsProperties();
            SettingBase.Folders folders = this.settings.FsoSettings;
            this.read(clientSettingFamily, folders.FileSettingClient);

            if (!string.IsNullOrEmpty(this.settings.DefaultSettings.RoamingFolder) &&
                Directory.Exists(this.settings.DefaultSettings.RoamingFolder))
                settings.SetRoamingFolder(this.settings.DefaultSettings.RoamingFolder);

            this.read(userRoamingSettingFamily, folders.FileSettingUserRoaming);
            this.read(serverSettingFamily, folders.FileSettingServer);
            this.readDbConnections(folders.FolderServer);
            return true;
        }



        internal bool Serialize(SettingFiles settingFiles)
        {
            if (!test()) return false;
            this.readSettingsProperties();
            SettingBase.Folders folders = this.settings.FsoSettings;
            this.write(clientSettingFamily, folders.FileSettingClient);
            this.write(userRoamingSettingFamily, folders.FileSettingUserRoaming);
            this.write(serverSettingFamily, folders.FileSettingServer);
            this.writeDbConnections(folders.FolderServer);
            return true;
        }

        private class ListProperty : Dictionary<string, PropertyInfo>
        {
            public void Add(PropertyInfo property)
            {
                base.Add(property.Name, property);
            }

            public ListProperty(object instance, string decription, bool crypto)
            {
                this.Instance = instance;
                this.Description = decription;
                this.Crypto = crypto;
            }

            public object Instance { get; set; }

            public bool Crypto { get; set; }

            public string Description { get; set; }
        }

        private void readSettingsProperties()
        {
            // Ottengo tutte le proprietà dell'istanza dei settaggi
            PropertyInfo[] propertyInfos = this.settings.GetType().GetProperties();

            // ciclo le proprietà ottenute e le ragguppo nei dizionari client e server per liste di categorie
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                // ottengo la categoria
                string category = "";

                object[] attribCats = propertyInfo.GetCustomAttributes(typeof(SubSettingsAttribute), true);
                if (attribCats.Length == 1)
                    category = propertyInfo.Name;

                string desc = getDescription(propertyInfo);

                // ottengo le proprietà della categoria
                foreach (PropertyInfo property in propertyInfo.PropertyType.GetProperties())
                {
                    // considero solo le proprietà che si possono leggere e scrivere
                    if (property.CanWrite && property.CanRead)
                    {
                        bool crypto = checkAttrib(typeof(CryptoSettingsAttribute), property);

                        // CLIENT
                        if (checkAttrib(typeof(ClientSettingAttribute), property))
                            regProperty(clientSettingFamily, propertyInfo, category, desc, property, crypto);

                        // USER ROAMING
                        if (checkAttrib(typeof(RoamingUserSettingAttribute), property))
                            regProperty(userRoamingSettingFamily, propertyInfo, category, desc, property, crypto);

                        // SERVER
                        if (checkAttrib(typeof(ServerSettingAttribute), property))
                            regProperty(serverSettingFamily, propertyInfo, category, desc, property, crypto);
                    }
                }
            }
        }

        private bool checkAttrib(Type type, PropertyInfo property)
        {
            return property.GetCustomAttributes(type, true).Length == 1 ? true : false;
        }

        private void regProperty(Dictionary<string, ListProperty> dictionary, PropertyInfo propertyInfo, string category, string desc, PropertyInfo property, bool crypto)
        {
            if (!dictionary.ContainsKey(category))
                dictionary.Add(category, new ListProperty(propertyInfo.GetValue(this.settings, null), desc, crypto));
            dictionary[category].Add(property);
        }

        private string getDescription(PropertyInfo propertyInfo)
        {
            object[] descs = propertyInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (descs.Length == 1)
                return ((DescriptionAttribute)descs[0]).Description;
            return string.Empty;
        }

        private void write(Dictionary<string, ListProperty> dicCatProperties, FileSystemMgr file)
        {
            try
            {
                if (dicCatProperties.Count == 0)
                    return;

                if (!file.Create(FileSystemTypes.File).Exists) return;

                using (XmlFileWriter writer = new XmlFileWriter(file.Path))
                {
                    writer.OpenXML("Settings");
                    writer.OpenNode("IDSettings");
                    writer.AddAttribute("Guid", this.settings.IdSettings.ToString());
                    writer.CloseNode();

                    // ciclo per tutte le categorie
                    foreach (KeyValuePair<string, ListProperty> key in dicCatProperties)
                    {
                        writer.WriteCommento(key.Value.Description);
                        writer.OpenNode("Category");
                        writer.AddAttribute(STR_Name, key.Key);
                        foreach (PropertyInfo property in key.Value.Values)
                        {
                            writer.WriteCommento(getDescription(property));
                            writer.OpenNode("Setting");
                            writer.AddAttribute(STR_Name, property.Name);

                            object value = null;
                            if (key.Value.Instance != null)
                                value = property.GetValue(key.Value.Instance, null);

                            if (value is DateTime)
                                value = ((DateTime)value).Serialize();
                            if (value is Font)
                                value = ((Font)value).FontToString();

                            if (value == null) value = string.Empty;
                            if (!key.Value.Crypto || string.IsNullOrEmpty(value.ToString()))
                                writer.AddAttribute(STR_Value, value.ToString());
                            else
                                writer.AddAttribute(STR_Value, value.ToString().ToByteStringFormat());
                            writer.AddAttribute("Type", property.PropertyType.FullName);
                            writer.CloseNode();
                        }
                        writer.CloseNode();
                    }
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

        }

        private void read(Dictionary<string, ListProperty> dicCatProperties, FileSystemMgr file)
        {
            if (!file.Exists || file.Size == 0) return;

            try
            {
                XmlDocument xdoc = new XmlDocument();

                xdoc.Load(file.Path);

                string ids = xdoc.SelectSingleNode("//Settings/IDSettings").Attributes["Guid"].Value;

                Guid id = new Guid(xdoc.SelectSingleNode("//Settings/IDSettings").Attributes["Guid"].Value);
                if (id != this.settings.IdSettings)
                    throw new Exception(string.Format("Il file {0} ID={1} contiene settaggi non compatibili per l'applicazione ID={2}", file, id, this.settings.IdSettings.ToString()));

                ReflectionTools rt = new ReflectionTools();

                foreach (KeyValuePair<string, ListProperty> item in dicCatProperties)
                {
                    XmlNodeList xNodes = xdoc.SelectNodes(string.Format("//Settings/Category[@Name='{0}']/Setting", item.Key));

                    foreach (XmlNode node in xNodes)
                    {
                        string Name = node.Attributes[STR_Name].Value;

                        string Value = string.Empty;
                        if (!item.Value.Crypto)
                            Value = node.Attributes[STR_Value].Value;
                        else
                            Value = node.Attributes[STR_Value].Value.FromBytesStringFormat();

                        string Type = node.Attributes["Type"].Value;

                        if (item.Value.ContainsKey(Name))
                            rt.SetProperty(item.Value.Instance, item.Value[Name], Value);
                    }
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        private void readDbConnections(FileSystemMgr folderServer)
        {
            string fileXml = String.Format(STR_DbConnectionssetting, folderServer);
            if (new FileSystemMgr(fileXml).Exists)
            {
                XmlDocument xdoc = new XmlDocument();

                xdoc.Load(fileXml);
                XmlNodeList nodes = xdoc.SelectNodes("//" + STR_DbConnectionSet);
                foreach (XmlNode node in nodes)
                {
                    SettingBase.DbConnections dbcns = new SettingBase.DbConnections();
                    dbcns.SetConnectionName = node.AttributeValue<string>(STR_Name);


                    XmlNodeList nodechilds = node.SelectNodes(STR_DbConnection);
                    foreach (XmlNode nodec in nodechilds)
                    {
                        DbConnectionStringBuilder dbcsb = new DbConnectionStringBuilder();
                        string name = nodec.AttributeValue<string>(STR_Name);

                        XmlNodeList nodeKs = nodec.SelectNodes(STR_KeyValue);
                        foreach (XmlNode nodeK in nodeKs)
                        {
                            string key = nodeK.AttributeValue<string>(STR_Key);
                            object value = nodeK.AttributeValue<object>(STR_Value);

                            if (key.ToLower() == STR_Password)
                                try
                                {
                                    value = value.ToString().FromBytesStringFormat().Decrypt();
                                }
                                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                            dbcsb.Add(key, value);
                            
                        }
                        dbcns.Add(name, dbcsb);
                    }
                    this.settings.DbConns.Add(dbcns);
                }
            }

        }

        private void writeDbConnections(FileSystemMgr folderServer)
        {
            if (this.settings.DbConns.Count != 0)
                using (XmlFileWriter writer = new XmlFileWriter(String.Format(STR_DbConnectionssetting, folderServer)))
                {
                    writer.OpenXML(STR_DbConnections);
                    foreach (KeyValuePair<string, SettingBase.DbConnections> setConn in this.settings.DbConns)
                    {
                        writer.OpenNode(STR_DbConnectionSet);
                        writer.AddAttribute(STR_Name, setConn.Key);

                        foreach (KeyValuePair<string, DbConnectionStringBuilder> item in setConn.Value)
                        {
                            writer.OpenNode(STR_DbConnection);
                            writer.AddAttribute(STR_Name, item.Key);
                            foreach (KeyValuePair<string, object> o in item.Value)
                            {
                                writer.OpenNode(STR_KeyValue);
                                string key = o.Key;
                                string value;

                                if (key.ToLower() == STR_Password)
                                    value = o.Value.ToString().Encrypt().ToByteStringFormat();
                                else
                                    value = o.Value.ToString();
                                writer.AddAttribute(STR_Key, o.Key);
                                writer.AddAttribute(STR_Value, value);

                                writer.CloseNode();
                            }
                            writer.CloseNode();
                        }
                        writer.CloseNode();
                    }
                    writer.CloseXML();
                }
        }

        private bool test()
        {
            SettingBase.Folders folders = this.settings.FsoSettings;
            if (
                this.settings == null ||
                !folders.FileSettingClient.Create(FileSystemTypes.File).Exists ||
                !folders.FileSettingServer.Create(FileSystemTypes.File).Exists ||
                !folders.FileSettingUserRoaming.Create(FileSystemTypes.File).Exists)
                return false;
            else
                return true;
        }
    }
}
