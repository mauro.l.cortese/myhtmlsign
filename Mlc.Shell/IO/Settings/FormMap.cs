﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
namespace Mlc.Shell.IO
{
    public class FormMap
    {

        private Form form;
        private Screen screen;



        public FormMap()
        {
        }


        public FormMap(Form form)
        {
            this.form = form;
            this.size = this.form.Size;
            this.location = this.form.Location;
            this.screen = Screen.FromControl(this.form);
            this.screenName = this.screen.DeviceName;
        }



        private Size size = default(Size);
        public Size Size
        {
            get { return this.size; }
            set
            {
                this.size = value;
                if (this.form != null)
                    this.form.Size = this.size;
            }
        }

        private Point location = default(Point);
        public Point Location
        {
            get { return this.location; }
            set
            {
                this.location = value;

                if (this.form != null)
                    this.form.Location = this.location;

            }
        }

        /// <summary>
        /// The screen name
        /// </summary>
        private string screenName = string.Empty;
        /// <summary>
        /// Gets or sets the name of the screen.
        /// </summary>
        /// <value>The name of the screen.</value>
        public string ScreenName
        {
            get { return this.screenName; }
            set
            {
                this.screenName = value;

            }
        }

        /// <summary>
        /// Initializes the form.
        /// </summary>
        /// <param name="form">The form.</param>
        public void InitForm(Form form)
        {
            this.form = form;
            this.form.Size = this.size;
            this.form.Location = this.location;
            this.screen = Screen.PrimaryScreen;
            foreach (Screen scr in Screen.AllScreens)
            {
                if (scr.DeviceName == this.screenName)
                {
                    this.screen = scr;
                    break;
                }
            }

            //this.form.Left = this.screen.WorkingArea.Left + this.location.X;
            //this.form.Top = this.screen.WorkingArea.Top + this.location.Y;



            //this.screen = Screen.FromControl(this.form);
        }
    }
}
