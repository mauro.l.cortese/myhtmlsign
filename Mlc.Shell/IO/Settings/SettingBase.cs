using System;
using System.Data.Common;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.ComponentModel.Composition;

using Mlc.Shell.IO;
using System.Drawing;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Ereditare da questa classe per tipi che contengono i settaggi di un'applicazione
    /// </summary>
    public abstract class SettingBase
    {
        #region Campi
        #endregion

        #region Campi interni
        /// <summary>Identificativo del set di settaggi</summary>
        internal Guid IdSettings = default(Guid);
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="IdSettings">Guid per il set dei settaggi</param>
        /// <param name="pathLocalSetting">Path per i settaggi locali ("C:\Program Files\AppName\xml")</param>
        /// <param name="folderLocalSettingName">Nome della cartella dei settaggi locali ("Production")</param>
        protected SettingBase(Guid IdSettings, string pathLocalSetting, string folderLocalSettingName)
        {
            this.IdSettings = IdSettings;
            this.fsoSettings = new Folders(new FileSystemMgr(pathLocalSetting), folderLocalSettingName);
        }
        #endregion

        #region Proprietà
        /// <summary>
        /// The database connection string
        /// </summary>
        private DbConnectionsColl dbConnStringsColl = new DbConnectionsColl();
        /// <summary>
        /// Gets or sets the database connection string.
        /// </summary>
        /// <value>The database connection string.</value>
        public DbConnectionsColl DbConns { get { return this.dbConnStringsColl; } set { this.dbConnStringsColl = value; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Imposta la cartella per i settaggi in roaming.
        /// </summary>
        /// <param name="serverFolderSetting">Cartella con i file dei settaggi</param>
        public void SetRoamingFolder(string serverFolderSetting)
        {
            this.FsoSettings.SetRoamingFolder(string.Concat(serverFolderSetting));
        }

        /// <summary>Files contenenti i settaggi dell'applicazione</summary>
        private Folders fsoSettings = null;
        /// <summary>
        /// Restituisce i path dei files contenenti i settaggi dell'applicazione
        /// </summary>
        [SubSettings, Description("Posizione dei file di settaggi dell'applicazione")]
        public Folders FsoSettings { get { return this.fsoSettings; } }

        /// <summary>
        /// Deserializza i dati dell'istanza corrente
        /// </summary>
        /// <param name="settingFiles">Enumerato dei files da considerare</param>
        /// <returns>True se l'operazione riesce, altrimenti false</returns>
        public bool Deserialize(SettingFiles settingFiles)
        {
            return new IOSettingsXml(this).Deserialize(settingFiles);
        }

        /// <summary>
        /// Serializza i dati dell'istanza corrente
        /// </summary>
        /// <param name="settingFiles">Enumerato dei files da considerare</param>
        /// <returns>True se l'operazione riesce, altrimenti false</returns>
        public bool Serialize(SettingFiles settingFiles)
        {
            return new IOSettingsXml(this).Serialize(settingFiles);
        }


        /// <summary>
        /// Salva le dimensioni di un form
        /// </summary>
        /// <param name="size">Dimensione da salvare</param>
        /// <param name="key">Chiave della dimensione da ottenere</param>
        public void SaveSize(FormMap formMap, string key)
        {
            XmlDocument XDoc = this.getXmlSizeForms();
            XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", key));
            bool save = true;
            if (xNode != null)
            {
                save = xNode.Attributes["Size"].Value != formMap.Size.ToString();
                xNode.Attributes["Size"].Value = formMap.Size.ToString();

                save = save || xNode.Attributes["Location"].Value != formMap.Location.ToString();
                xNode.Attributes["Location"].Value = formMap.Location.ToString();

                save = save || xNode.Attributes["ScreenName"].Value != formMap.ScreenName;
                xNode.Attributes["ScreenName"].Value = formMap.ScreenName;
            }
            else
            {
                save = true;
                XmlNode n = XDoc.CreateNode(XmlNodeType.Element, "Size", "");

                XmlAttribute a = XDoc.CreateAttribute("Key");
                a.Value = key;
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("Size");
                a.Value = formMap.Size.ToString();
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("Location");
                a.Value = formMap.Location.ToString();
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("ScreenName");
                a.Value = formMap.ScreenName;
                n.Attributes.Append(a);

                XDoc.DocumentElement.AppendChild(n);
            }
            if (save)
                XDoc.Save(getFileXmlFormSize());
        }

        /// <summary>
        /// Carica le dimensioni di un form (le imposta subito sul form)
        /// </summary>
        /// <param name="key">Chiave della dimensione da ottenere</param>
        /// <returns>Dimensioni ottenute</returns>
        public FormMap LoadSize(string key)
        {
            FormMap retFormMap = new FormMap();
            XmlDocument XDoc = this.getXmlSizeForms();
            XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", key));
            if (xNode != null)
            {
                retFormMap.Size = TypeConvert<Size>.GetValue(xNode.Attributes["Size"].Value, default(Size));
                retFormMap.Location = TypeConvert<Point>.GetValue(xNode.Attributes["Location"].Value, default(Point));
                retFormMap.ScreenName = xNode.Attributes["ScreenName"].Value;
            }
            return retFormMap;
        }

        /// <summary>
        /// Sets the default values.
        /// </summary>
        public virtual void SetDefaultValues()
        {
            this.DefaultSettings = new SubDefaultSettings()
            {
                RoamingFolder = this.FsoSettings.FolderServer.Path
            };
        }

        private XmlDocument getXmlSizeForms()
        {
            string fileSize = getFileXmlFormSize();
            XmlDocument XDoc = new XmlDocument();
            if (!File.Exists(fileSize))
                using (XmlFileWriter writer = new XmlFileWriter(fileSize))
                    writer.OpenXML("SizeForms");
            XDoc.Load(fileSize);
            return XDoc;
        }

        /// <summary>
        /// Gets the size of the file XML form.
        /// </summary>
        /// <returns>System.String.</returns>
        private string getFileXmlFormSize()
        {
            string fileSize = string.Concat(this.FsoSettings.FileSettingUserRoaming.FsoParentFolder.Path, "\\LayoutControl\\SizeForms.xml");
            string folder = Path.GetDirectoryName(fileSize);
            FileSystemMgr fsm = new FileSystemMgr(folder);
            if (!fsm.Exists)
                fsm.Create(FileSystemTypes.Directory);
            return fileSize;
        }
        #endregion

        #region Evento SettingsChanged                
        /// <summary>
        /// Delegato per la gestione dell'evento.
        /// </summary>
        public event EventHandler SettingsChanged;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected internal virtual void OnSettingsChanged(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (SettingsChanged != null)
                // viene lanciato l'evento
                SettingsChanged(this, e);
        }
        #endregion

        #region Tipi nidificati


        /// <summary>
        /// Restituisce o imposta l'istanza contenente i membri dei settaggi per i DB
        /// </summary>
        [SubSettings, Description("Settaggi per i DB")]
        public SubDefaultSettings DefaultSettings { get; set; }

        /// <summary>
        /// Opzioni per i DB
        /// </summary>
        public class SubDefaultSettings
        {
            /// <summary>
            /// Restituisce o imposta il nome del file con le impostazioni di connessione da utilizzare
            /// </summary>
            [ClientSetting, Description("Cartella settaggi remoti")]
            public string RoamingFolder { get; set; }
        }

        /// <summary>
        /// The database connection string
        /// </summary>
        public class DbConnectionsColl : Dictionary<string, DbConnections>
        {

            public void Add(DbConnections dbConns)
            {
                base.Add(dbConns.SetConnectionName, dbConns);
            }

        }

        /// <summary>
        /// The database connection string
        /// </summary>
        public class DbConnections : Dictionary<string, DbConnectionStringBuilder>
        {
            /// <summary>
            /// The set connection name
            /// </summary>
            private string setConnectionName = string.Empty;
            /// <summary>
            /// Gets or sets the name of the set connection.
            /// </summary>
            /// <value>The name of the set connection.</value>
            public string SetConnectionName { get { return this.setConnectionName; } set { this.setConnectionName = value; } }
        }

        /// <summary>
        /// Memorizza i path dei files contenenti i settaggi dell'applicazione
        /// </summary>
        public class Folders
        {
            #region Costruttori
            /// <summary>
            /// Inizializza una nuova istanza con la cartella predefinita locale
            /// </summary>
            /// <param name="folderClient">Istanza <see cref="Mx.Core.IO.FileSystemMgr"/> del path della cartella predefinita locale</param>
            public Folders(FileSystemMgr folderClient, string folderSettingsName)
            {
                this.folderClient = string.IsNullOrEmpty(folderSettingsName) ? folderClient : new FileSystemMgr(string.Concat(folderClient, "\\", folderSettingsName));

                StringBuilder folder = new StringBuilder();

                folder.Append(folderClient.Path);
                folder.Append("\\roaming");
                if (!string.IsNullOrEmpty(folderSettingsName))
                    folder.Append(string.Concat("\\", folderSettingsName));

                this.folderServer = new FileSystemMgr(folder.ToString());


                if (!this.FolderClient.Create(FileSystemTypes.Directory).Exists)
                    throw new Exception(string.Format("Impossibile raggiungere o creare la cartella locale {0}", this.folderClient));

                this.setPaths();
            }

            #endregion

            #region Proprietà
            /// <summary>File settaggi client formato: Dominio-ComputerName.settings (Es: CedTecnosweetLocal-CLT001.settings)</summary>
            private FileSystemMgr fileSettingClient = null;
            /// <summary>
            /// Restituisce il file settaggi client formato: Dominio-ComputerName.settings (Es: CedTecnosweetLocal-CLT001.settings)
            /// </summary>
            public FileSystemMgr FileSettingClient { get { return this.fileSettingClient; } }

            /// <summary>File settaggi roaming utente formato: Dominio.UserName.settings (Es: CedTecnosweetLocal-CorteseMauro.settings)</summary>
            private FileSystemMgr fileSettingUserRoaming = null;
            /// <summary>
            /// Restituisce il file settaggi roaming utente formato: Dominio.UserName.settings (Es: CedTecnosweetLocal-CorteseMauro.settings)
            /// </summary>
            public FileSystemMgr FileSettingUserRoaming { get { return this.fileSettingUserRoaming; } }

            /// <summary>File settaggi server formato: Dominio.settings (Es: CedTecnosweetLocal.settings)</summary>
            private FileSystemMgr fileSettingServer = null;
            /// <summary>
            /// Restituisce il file settaggi server formato: Dominio.settings (Es: CedTecnosweetLocal.settings)
            /// </summary>
            public FileSystemMgr FileSettingServer { get { return this.fileSettingServer; } }

            private FileSystemMgr folderServer = null;
            /// <summary>
            /// Restituisce l'istanza <see cref="Mx.Core.IO.FileSystemMgr"/> della cartella dei settaggi remota
            /// </summary>
            [ClientSetting, Description("Full path file settaggi server")]
            public FileSystemMgr FolderServer { get { return this.folderServer; } }

            private FileSystemMgr folderClient = null;
            /// <summary>
            /// Restituisce l'istanza <see cref="Mx.Core.IO.FileSystemMgr"/> della cartella dei settaggi locale
            /// </summary>
            public FileSystemMgr FolderClient { get { return this.folderClient; } }

            /// <summary>
            /// Gets the folder setting user roaming.
            /// </summary>
            /// <value>The folder setting user roaming.</value>
            public string FolderSettingUserRoaming { get { return Path.GetDirectoryName(this.fileSettingUserRoaming.Path); } }

            /// <summary>
            /// Gets the folder setting server.
            /// </summary>
            /// <value>The folder setting server.</value>
            public string FolderSettingServer { get { return Path.GetDirectoryName(this.fileSettingServer.Path); } }
            #endregion

            #region Metodi pubblici
            /// <summary>
            /// Costruttore privato non permette l'istanziamento diretto di questa classe.
            /// </summary>
            /// <param name="serverFolderSetting">Cartella con i file dei settaggi</param>
            /// <exception cref="System.Exception"></exception>
            internal void SetRoamingFolder(string serverFolderSetting)
            {
                if (string.IsNullOrWhiteSpace(serverFolderSetting)) return;
                this.folderServer = new FileSystemMgr(serverFolderSetting);
                if (!this.FolderServer.Create(FileSystemTypes.Directory).Exists)
                    throw new Exception(string.Format("Impossibile raggiungere o creare la cartella remota {0}", serverFolderSetting));

                this.setPaths();
            }
            #endregion

            #region Metodi privati
            /// <summary>
            /// Inizializza i paths
            /// </summary>
            private void setPaths()
            {
                SysInfo syf = new SysInfo();
                string app = Path.GetFileNameWithoutExtension(syf.ExecutableFile);
                string domain = syf.DomainName.Replace(".", " ").GetUpperCamelCase().Replace("Local", "");
                this.fileSettingClient = new FileSystemMgr(string.Format("{0}\\{1}-{2}.settings", this.FolderClient, domain, syf.ComputerName.ToUpper()));
                this.fileSettingUserRoaming = new FileSystemMgr(string.Format("{0}\\{1}\\users\\{2}\\{3}\\Application.settings", this.FolderServer, domain, syf.UserName, app));
                this.fileSettingServer = new FileSystemMgr(string.Format("{0}\\{1}\\{1}.settings", this.FolderServer, domain));
            }
            #endregion
        }
        #endregion

    }


    /// <summary>
    /// Ereditare da questa classe per tipi che contengono i settaggi di un'applicazione
    /// </summary>
    public abstract class SubSettingBase
    {
        #region Campi
        #endregion

        #region Campi interni
        /// <summary>Identificativo del set di settaggi</summary>
        private SettingBase settings = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Initializes a new instance of the <see cref="SubSettingBase"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        protected SubSettingBase(SettingBase settings)
        {
            this.settings = settings;
        }
        #endregion

        /// <summary>
        /// Raises the event settings changed.
        /// </summary>
        public void RaiseEventSettingsChanged()
        {
            this.settings.OnSettingsChanged(new EventArgs());

        }
    }
}
