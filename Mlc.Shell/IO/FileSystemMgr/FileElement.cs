using System;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Mlc.Shell;
using Mlc.Shell.Crypto;
using Mlc.Shell.IO;

//using Mx.Core.Constants;
//using Mx.Core.Enumerations;
//using Mx.Core.Crypto;
//using Mx.Core;

using SevenZip;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Implemetazione per le operazioni sui files
    /// </summary>
    internal class FileElement : IObj
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="path">Fullpath del file origine</param>
        public FileElement(string path)
            : base(path)
        {
        }
        #endregion

        #region Public
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Internal
        #region Properties
        /// <summary>
        /// Restituisce la dimensione dell'elemento in byte
        /// </summary>
        /// <returns>Dimensione dell'elemento in byte</returns>
        internal override long Size
        {
            get { return new FileInfo(path).Length; ; }
        }
        #endregion

        #region Method Static
        #endregion

        #region Method

        /// <summary>
        /// Copying the file system object at the specified location
        /// </summary>
        /// <param name="pathDest">Path to the copy destination</param>
        /// <param name="actionsCopy">Determines the action to be taken on new files generated, the possible actions are in the enumeration <see cref="Mlc.Shell.IO.ActionsCopy"/></param>
        /// <param name="resulCopyCompress">Determines the possible results of the copy of a file system object for <see cref="Mlc.Shell.IO.ActionsCopy.Compress"/> </param>
        /// <param name="password">Password that wil be use for data encryption, if value is null will be used the password of the constant <see cref="Mlc.Shell.Constants.DefaultPwd" /></param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Copy(ref string pathDest, ActionsCopy actionsCopy, ResultCopyCompress resulCopyCompress = ResultCopyCompress.SingleObject, string password = Constants.DefaultPwd)
        {
            bool retVal = false;
            byte[] fileBytes = null;
            try
            {
                #region Preliminary steps
                // check if file exists
                if (!File.Exists(base.path))
                    return false;

                // creates the destination folder if it doesn't exist yet
                string folderDest = System.IO.Path.GetDirectoryName(pathDest);
                if (!Directory.Exists(folderDest))
                    Directory.CreateDirectory(folderDest);
                #endregion

                /*
                 Combinazioni per actionsCopy
                 None            = simple copy of the file
                 Compress        = copy the file into a new compressed file in .7z format
                 Extract         = copy the compressed file in .7z format into a new uncompressed file
                 Encrypt         = copy the file into a new encrypted file 
                 Decript         = copy the encrypted file into a new decripted file 
                 CompressEncrypt = copy the file into a new compressed and encrypted file in .7z format
                 ExtractDecript  = copy the files compressed and encrypted in .7z format into a new uncompressed e decripted file
                 */

                SevenZipCompressor szc = null;
                SevenZipExtractor sze = null;
                switch (actionsCopy)
                {
                    case ActionsCopy.None:
                        // simple copy of the file
                        this.deleteExistingFile(pathDest);
                        File.Copy(base.path, pathDest);
                        break;
                    case ActionsCopy.Compress:
                        // copy the file into a new compressed file in .7z format
                        this.deleteExistingFile(pathDest);
                        szc = new SevenZipCompressor();
                        szc.ArchiveFormat = OutArchiveFormat.SevenZip;
                        szc.CompressFiles(pathDest, new string[] { base.path });
                        break;
                    case ActionsCopy.Extract:
                        // copy the compressed file in .7z format into a new uncompressed file
                        if (!Directory.Exists(pathDest))
                            Directory.CreateDirectory(pathDest);
                        sze = new SevenZipExtractor(base.path);
                        sze.ExtractArchive(pathDest);
                        break;
                    case ActionsCopy.Encrypt:
                        // copy the file into a new encrypted file 
                        fileBytes = this.encriptBytes(password, File.ReadAllBytes(base.path));
                        File.WriteAllBytes(pathDest, fileBytes);
                        break;
                    case ActionsCopy.Decrypt:
                        // copy the encrypted file into a new decripted file 
                        fileBytes = this.decriptBytes(password, File.ReadAllBytes(base.path));
                        File.WriteAllBytes(pathDest, fileBytes);
                        break;
                    case ActionsCopy.CompressEncrypt:
                        // copy the file into a new compressed and encrypted file in .7z format
                        this.deleteExistingFile(pathDest);
                        szc = new SevenZipCompressor();
                        szc.ArchiveFormat = OutArchiveFormat.SevenZip;
                        szc.ZipEncryptionMethod = ZipEncryptionMethod.Aes256;
                        szc.CompressFilesEncrypted(pathDest, password, new string[] { base.path });
                        break;
                    case ActionsCopy.ExtractDecrypt:
                        // copy the files compressed and encrypted in .7z format into a new uncompressed e decripted file
                        if (!Directory.Exists(pathDest))
                            Directory.CreateDirectory(pathDest);
                        sze = new SevenZipExtractor(base.path, password);
                        sze.ExtractArchive(pathDest);
                        break;
                    default:

                        break;
                }
                retVal = true;
            }
            catch (Exception ex)
            {
                if (ex.Source == "SevenZipSharp")
                {
                    try
                    {
                        SevenZipExtractor extractor = new SevenZipExtractor(base.path);
                        extractor.ExtractArchive(pathDest);
                        retVal = true;
                    }
                    catch (Exception ex7z)
                    {
                        ExceptionsRegistry.GetInstance().Add(ex);
                        ExceptionsRegistry.GetInstance().Add(ex7z);
                        retVal = false;
                    }
                }
                else
                {
                    ExceptionsRegistry.GetInstance().Add(ex);
                    retVal = false;
                }
            }
            return retVal;
        }

        private void deleteExistingFile(string pathDest)
        {
            // eliminates the destination file if it already exists
            if (File.Exists(pathDest))
            {
                File.SetAttributes(pathDest, FileAttributes.Normal);
                File.Delete(pathDest);
            }
        }

        private byte[] decriptBytes(string password, byte[] fileBytes)
        {
            try
            {
                RijndaelCryptoByte cb = null;
                cb = new RijndaelCryptoByte(fileBytes);
                cb.Password = password;
                cb.Decrypt();
                fileBytes = cb.Data;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return fileBytes;
        }

        private byte[] encriptBytes(string password, byte[] fileBytes)
        {
            try
            {
                RijndaelCryptoByte cb = null;
                cb = new RijndaelCryptoByte(fileBytes);
                cb.Password = password;
                cb.Encrypt();
                fileBytes = cb.Data;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return fileBytes;
        }

        private void setOriginalAttribute(string pathDest, FileAttributes originAttr)
        {
            // imposto le date di accesso
            FileInfo f1 = new FileInfo(base.path);
            FileInfo f2 = new FileInfo(pathDest);

            f2.CreationTime = f1.CreationTime;
            f2.CreationTimeUtc = f1.CreationTimeUtc;
            f2.LastAccessTime = f1.LastAccessTime;
            f2.LastAccessTimeUtc = f1.LastAccessTimeUtc;
            f2.LastWriteTime = f1.LastWriteTime;
            f2.LastWriteTimeUtc = f1.LastWriteTimeUtc;

            // imposto gli attributi di origine al file copiato
            File.SetAttributes(pathDest, originAttr);
        }

        /// <summary>
        /// Esegue la cifratura del file
        /// </summary>
        /// <param name="password">Password per la cifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Encrypt(string password)
        {
            string newPath = string.Concat(base.path, ".mxcr");
            return this.Copy(ref newPath, ActionsCopy.Encrypt, password: password);
        }

        /// <summary>
        /// Esegue la decifratura del file
        /// </summary>
        /// <param name="password">Password per la decifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Decrypt(string password)
        {
            if (!base.path.EndsWith(".mxcr"))
                return false;

            string newPath = base.path.Replace(".mxcr", string.Empty);
            return this.Copy(ref newPath, ActionsCopy.Decrypt, password: password);
        }

        /// <summary>
        /// Elimina l'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Delete()
        {
            try
            {
                File.SetAttributes(base.path, FileAttributes.Normal);
                File.Delete(base.path);
                return !File.Exists(base.path);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Comprime i dati dell'elemento del file system nel file specificato
        /// </summary>
        /// <param name="fileDest">Fullpath del file di destinazione dei dati compressi (formato .7z)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Compress(string fileDest)
        {
            try
            {
                SevenZipCompressor ss = new SevenZipCompressor();
                ss.CompressFiles(fileDest, new string[] { this.path });
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Estrae i dati compressi dal file puntato dall'istanza nella directory specificata
        /// </summary>
        /// <param name="folderDest">Fullpath della cartella in cui estrarre i dati compressi (formato .7z)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Extract(string folderDest)
        {
            try
            {
                SevenZipExtractor sze = new SevenZipExtractor(this.path);
                sze.ExtractArchive(folderDest);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <param name="toDoListFilePath">To do list file path.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool WriteTreeInToDoList(string toDoListFilePath)
        {
            throw new Exception();
        }

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool WriteTreeInToDoList()
        {
            throw new Exception();
        }


        /// <summary>
        /// Lock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Lock()
        {
            return setAccessRule(true);
        }

        /// <summary>
        /// Unlock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool UnLock()
        {
            return setAccessRule(false);
        }
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Sets the access rule.
        /// </summary>
        /// <param name="lockAccess">if set to <c>true</c> lock access, otherwise if set to <c>false</c> unlock access.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        private bool setAccessRule(bool lockAccess)
        {
            try
            {
                string adminUserName = Environment.UserName;
                FileSecurity ds = File.GetAccessControl(this.path);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.Read | FileSystemRights.Delete, AccessControlType.Deny);
                if (lockAccess)
                    ds.AddAccessRule(fsa);
                else
                    ds.RemoveAccessRule(fsa);
                File.SetAccessControl(this.path, ds);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
