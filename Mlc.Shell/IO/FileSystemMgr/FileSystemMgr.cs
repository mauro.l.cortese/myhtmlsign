using SevenZip;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Gets information about an object of the file system and provides several methods to perform operations on files and folders
    /// </summary>
    /// <seealso cref="Mlc.Shell.IO.ShellLink"/>
    /// <seealso cref="Mlc.Shell.IO.ShellLinkInfo"/>
    public class FileSystemMgr
    {
        #region Constants
        /// <summary>
        /// Dll name 7z 32 bit
        /// </summary>
        private const string Dll7z32 = "7z(x32).dll";
        /// <summary>
        /// Dll name 7z 64 bit
        /// </summary>
        private const string Dll7z64 = "7z(x64).dll";
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// Instance for the operations on the file system
        /// </summary>
        private IObj iObj = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemMgr"/> class.
        /// </summary>
        public FileSystemMgr()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemMgr"/> class.
        /// </summary>
        /// <param name="path">The path of file system object.</param>
        public FileSystemMgr(string path)
        {
            this.Path = path;
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// The path of file system object.
        /// </summary>
        private string path;
        /// <summary>
        /// Gets or sets the path of file system object.
        /// </summary>
        /// <value>The path of file system object.</value>
        public string Path
        {
            get { return this.path; }
            set
            {

                if (value != null)
                    this.path = value.Trim();
                else
                    this.path = value;

                this.testPath();
            }
        }

        /// <summary>
        /// Value that determines whether it is a file
        /// </summary>
        private bool isFile = false;
        /// <summary>
        /// Gets a value that determines whether it is a file
        /// </summary>    
        public bool IsFile { get { return this.isFile; } }

        /// <summary>
        /// Value that determines whether it is a folder
        /// </summary>
        private bool isDirectory = false;
        /// <summary>
        /// Gets a value that determines whether it is a folder
        /// </summary>     
        public bool IsDirectory { get { return this.isDirectory; } }

        /// <summary>
        /// Value that determines if the file system object exists
        /// </summary>
        private bool exist = false;
        /// <summary>
        /// Gets a value that determines if the file system object exists
        /// </summary>    
        public bool Exists { get { return this.exist; } }

        /// <summary>Type of file system object</summary>
        private FileSystemTypes fsoType = FileSystemTypes.UnDefine;
        /// <summary>
        /// Gets the type of file system object
        /// </summary>    
        /// <example>
        /// After assigning a path to the instance, you can discriminate the type of the object of the file system (file or directory)
        /// <code>
        /// FileSystemMgr fso = new FileSystemMgr();
        /// fso.Path = "C:\\Temp";
        /// 
        /// switch (fso.FsoType)
        /// {
        ///     case Mx.Core.Enumerations.FileSystemTypes.File:
        ///             // Do Anything ...
        ///         break;
        ///     case Mx.Core.Enumerations.FileSystemTypes.Directory:
        ///             // Do Anything ...
        ///         break;
        ///     case Mx.Core.Enumerations.FileSystemTypes.UnDefine:
        ///             // Do Anything ...
        ///     default:
        ///         break;
        /// }
        /// </code>
        /// </example>
        public FileSystemTypes FsoType { get { return this.fsoType; } /*set { this.fsoType = value; }*/ }

        /// <summary>
        /// Gets the size of file system object in bytes
        /// </summary>
        /// <returns>The size of file system object in bytes</returns>
        public long Size { get { return this.iObj.Size; } }

        /// <summary>Folder that contains the current file system object</summary>
        private FileSystemMgr fsoParentFolder = null;
        /// <summary>
        /// Gets the fullpath of the folder that contains the current file system object
        /// </summary>
        /// <returns>The fullpath of the folder that contains the current file system object</returns>
        public FileSystemMgr FsoParentFolder { get { return this.fsoParentFolder; } }

        /// <summary>
        /// Gets the icon that is associated to the current file system object
        /// </summary>
        /// <remarks>If the current file system object is a <see cref="Mlc.Shell.IO.FileSystemTypes.Directory"/> returns null.</remarks>
        /// <returns>The icon that is associated to the current file system object</returns>
        public Icon AssociateIcon
        {
            get
            {
                if (this.fsoType == FileSystemTypes.File)
                    return GetFileIcons.GetInstance().GetIconByFile(this.Path);
                else
                    return null;
            }
        }
        #endregion

        #region Method Static
        /// <summary>
        /// Sets the path to the dll 7z based on the execution of the process to 32 or 64 bits.
        /// </summary>
        public static void Set7zLibraryPath()
        {
            try
            {
                SysInfo sysInfo = new SysInfo();
                if (sysInfo.Is64BitProcess)
                    SevenZipCompressor.SetLibraryPath(sysInfo.GetFileOnExecutableFolder(FileSystemMgr.Dll7z64));
                else
                    SevenZipCompressor.SetLibraryPath(sysInfo.GetFileOnExecutableFolder(FileSystemMgr.Dll7z32));
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        /// <summary>
        /// Gets the absolute path starting from the relative path, in reference to the absolute path of reference provided
        /// </summary>
        /// <param name="startPath">absolute path starting</param>
        /// <param name="relativePath">relative path to the arrival</param>
        /// <returns>Absolute path determined</returns>
        public static string GetAbsolutePath(string startPath, string relativePath)
        {
            //                 da: C:\RootFolder\BFolder01\BFolder02
            //                  a: C:\RootFolder\AFolder01\AFolder02\Relative
            // il path relativo è: ..\..\..\AFolder01\AFolder02\Relative
            StringBuilder absolutePath = new StringBuilder();

            if (string.IsNullOrEmpty(startPath) || string.IsNullOrEmpty(relativePath)) return null;
            string[] startPathVect = startPath.ToLower().Split(System.IO.Path.DirectorySeparatorChar);
            string[] relativePathVect = relativePath.ToLower().Split(System.IO.Path.DirectorySeparatorChar);

            int jump = 0;
            // ottengo il numero di salti dal path relativo
            for (int i = 0; i < relativePathVect.Length; i++)
                if (relativePathVect[i] == "..")
                    jump++;
                else
                    break;

            if (jump == 0)
                absolutePath.Append(string.Concat(startPath, relativePath));
            else
            {
                if (jump < startPathVect.Length)
                {
                    int start = startPathVect.Length - jump + 1;

                    string[] newStartPathVect = startPath.Split(System.IO.Path.DirectorySeparatorChar);
                    for (int i = 0; i < start; i++)
                    {
                        absolutePath.Append(newStartPathVect[i]);
                        absolutePath.Append("\\");
                    }


                    string[] newRelativePathVect = relativePath.Split(System.IO.Path.DirectorySeparatorChar);
                    for (int i = jump; i < newRelativePathVect.Length; i++)
                    {
                        absolutePath.Append(newRelativePathVect[i]);
                        if (i < newRelativePathVect.Length - 1)
                            absolutePath.Append("\\");
                    }
                }
            }
            return absolutePath.ToString();
        }

        /// <summary>
        /// Gets the relative path starting from the absolute path, in reference to the absolute path of reference provided
        /// </summary>
        /// <param name="startPathFolder">absolute folder path starting</param>
        /// <param name="endPath">path to the arrival (folder or file)</param>
        /// <returns>Relative path determined</returns>

        public static string GetRelativePath(string startPathFolder, string endPath)
        {
            //                 da: C:\RootFolder\BFolder01\BFolder02
            //                  a: C:\RootFolder\AFolder01\AFolder02\Relative
            // il path relativo è: ..\..\..\AFolder01\AFolder02\Relative
            StringBuilder relativePath = new StringBuilder();

            if (string.IsNullOrEmpty(startPathFolder) || string.IsNullOrEmpty(endPath)) return null;
            string[] startPathVect = startPathFolder.ToLower().Split(System.IO.Path.DirectorySeparatorChar);
            string[] endPathVect = endPath.ToLower().Split(System.IO.Path.DirectorySeparatorChar);

            // ottengo il numero di elementi del path più corto
            int length = startPathVect.Length > endPathVect.Length ? endPathVect.Length : startPathVect.Length;

            // ciclo per il numero di elementi e ottengo la porzione comune tra i due path
            int pos = -1;
            for (int i = 0; i < length; i++)
                if (startPathVect[i] == endPathVect[i])
                    pos++;

            if (pos - 1 < 0) return relativePath.ToString();


            if (pos == startPathVect.Length - 1) // si tratta di una sotto cartella dello start path
            {
                relativePath.Append(endPath.Substring(startPathFolder.Length));
            }
            else
            {
                pos++;
                int numRelFolder = startPathVect.Length - pos;
                if (numRelFolder > 0)
                {
                    for (int i = 0; i < numRelFolder; i++)
                        relativePath.Append("..\\");

                    string[] newEndPathVect = endPath.Split(System.IO.Path.DirectorySeparatorChar);

                    for (int i = pos; i < newEndPathVect.Length; i++)
                    {
                        relativePath.Append(newEndPathVect[i]);
                        if (endPathVect.Length - 1 != i)
                            relativePath.Append("\\");
                    }
                }
            }
            return relativePath.ToString();
        }

        /// <summary>
        /// Delete all files in the specified folder
        /// </summary>
        /// <param name="folder">Fullpath folder to empty</param>
        public static void DeleteAllFiles(string folder)
        {
            FileSystemMgr fsoFolder = new FileSystemMgr(folder);
            if (!fsoFolder.Exists)
                fsoFolder.Create(FileSystemTypes.Directory);

            string[] files = Directory.GetFiles(fsoFolder.Path);
            FileSystemMgr fso = new FileSystemMgr();
            foreach (string file in files)
            {
                fso.Path = file;
                fso.Delete();
            }
        }

        /// <summary>
        /// Returns the string passed correcting illegal characters for the file system
        /// </summary>
        /// <param name="fsoName">string to be evaluated</param>
        /// <returns>Correct string</returns>
        public static string GetValidFsoName(string fsoName)
        {
            string retVal = fsoName;
            string[] invalidChars = new string[] { "\\", "/", ":", "*", "?", "<", ">", "|", "\r", "\n" };
            string[] validChars = new string[] { "-", "-", " ", "x", "-", "(", ")", "-", "", "" };
            retVal = fsoName.Sieve(invalidChars, validChars, null);
            return retVal;
        }

        /// <summary>
        /// Gets the icon that is associated to the file provided
        /// </summary>
        /// <param name="file">full path or file name with the extension</param>
        /// <returns>The icon that is associated to the current file system object</returns>
        public static Icon GetIcon(string file)
        {
            return GetFileIcons.GetInstance().GetIconByFile(file);
        }


        /// <summary>
        /// Delete the files from the specified folder
        /// </summary>
        /// <param name="folder">folder to delete files</param>
        /// <param name="filter">Filter for finding files</param>
        /// <param name="searchOptions">file search options</param>
        /// <returns>Difference between the found files, and deleted 0 = Success</returns>
        public static int DeleteFiles(string folder, string filter, SearchOption searchOptions)
        {
            int retVal = 0;
            try
            {
                if (Directory.Exists(folder))
                {
                    string[] files = Directory.GetFiles(folder, filter, searchOptions);
                    retVal = files.Length;
                    // ciclo per tutti i files e li cancello
                    foreach (string file in files)
                        if (new FileSystemMgr(file).Delete())
                            retVal--;
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }

        /// <summary>
        /// Gets the full path of the folder under executable path
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="create">if set to <c>true</c> creates the folder if it is not exists yet.</param>
        /// <returns>Full path of the folder under executable path.</returns>
        public static FileSystemMgr AppSubFolder(string folderName, bool create)
        {
            FileSystemMgr retval = null;
            try
            {
                SysInfo syf = new SysInfo();
                string root = syf.ExecutableFolder;
                retval = new FileSystemMgr(string.Concat(root, Chars.BackSlash, folderName));
                if (!retval.Exists && create)
                    retval.Create(FileSystemTypes.Directory);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retval;
        }


        #endregion

        #region Method
        /// <summary>
        /// Copying the file system object at the specified location
        /// </summary>
        /// <param name="pathDest">Path to the copy destination</param>
        /// <returns>If the copy succeeds it returns a new instance of the <see cref="Mlc.Shell.IO.FileSystemMgr" /> refers to the copy of the item
        /// Otherwise null is returned</returns>
        /// <example>
        /// Make a copy of a file system object with this method is a very fast operation.
        /// In the following example the directory "C: \ Temp1" is copied to "C: \ Temp2" all sub-directories in "C: \ Temp1" will be included in the copy.
        /// <code>
        /// new FileSystemMgr(@"C:\Temp1").Copy(@"C:\Temp2");
        /// </code>
        /// The next example copies only the file "C: \ Temp1 \ Data.txt"..
        /// <code>
        /// new FileSystemMgr(@"C:\Temp1\Data.txt").Copy(@"C:\Temp2\Data.txt");
        /// </code>        
        /// In both examples above, if the destination folder does not exist it will be created, 
        /// if the files are already present in the destination folder they will be overwritten.
        /// </example>
        public FileSystemMgr Copy(string pathDest)
        {
            return this.Copy(pathDest, ActionsCopy.None);
        }

        ///// <summary>
        ///// Copying the file system object at the specified location
        ///// </summary>
        ///// <param name="pathDest">Path to the copy destination</param>
        ///// <param name="actionsCopy">Determines the action to be taken on new files generated, the possible actions are in the enumeration <see cref="Mlc.Shell.IO.ActionsCopy"/></param>
        ///// <returns>If the copy succeeds it returns a new instance of the <see cref="Mlc.Shell.IO.FileSystemMgr" /> refers to the copy of the item
        ///// Otherwise null is returned</returns>
        //public FileSystemMgr Copy(string pathDest, ActionsCopy actionsCopy )
        //{
        //    return this.Copy(pathDest, actionsCopy, Constants.DefaultPwd);
        //}

        /// <summary>
        /// Copying the file system object at the specified location
        /// </summary>
        /// <param name="pathDest">Path to the copy destination</param>
        /// <param name="actionsCopy">Determines the action to be taken on new files generated, the possible actions are in the enumeration <see cref="Mlc.Shell.IO.ActionsCopy"/></param>
        /// <param name="resulCopyCompress">Determines the possible results of the copy of a file system object for <see cref="Mlc.Shell.IO.ActionsCopy.Compress"/> </param>
        /// <param name="password">Password that wil be use for data encryption, if value is null will be used the password of the constant <see cref="Mlc.Shell.Constants.DefaultPwd" /></param>
        /// <returns>If the copy succeeds it returns a new instance of the <see cref="Mlc.Shell.IO.FileSystemMgr" /> refers to the copy of the item
        /// Otherwise null is returned</returns>
        public FileSystemMgr Copy(string pathDest, ActionsCopy actionsCopy = ActionsCopy.None, ResultCopyCompress resulCopyCompress = ResultCopyCompress.SingleObject, string password = Constants.DefaultPwd)
        {
            if (this.iObj.Copy(ref pathDest, actionsCopy, resulCopyCompress, password))
                return new FileSystemMgr(pathDest);
            else
                return null;
        }


        /// <summary>
        /// Compresses all the data in the file system object into the specified file
        /// </summary>
        /// <param name="fileDest">Fullpath destination file compressed (.7z format)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Compress(string fileDest)
        {
            return this.iObj.Compress(fileDest);
        }

        /// <summary>
        /// Decompresses a file .7z
        /// </summary>
        /// <param name="folderDest">Fullpath of the folder to extract the compressed data</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Extract(string folderDest)
        {
            return this.iObj.Extract(folderDest);
        }

        /// <summary>
        /// Esegue la cifratura dell'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Encrypt()
        {
            return this.Encrypt(Constants.DefaultPwd);
        }

        /// <summary>
        /// Esegue la cifratura dell'elemento del file system
        /// </summary>
        /// <param name="password">Password per la cifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Encrypt(string password)
        {
            try
            {
                return this.iObj.Encrypt(password);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Esegue la decifratura dell'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Decrypt()
        {
            return this.Decrypt(Constants.DefaultPwd);
        }

        /// <summary>
        /// Esegue la decifratura dell'elemento del file system
        /// </summary>
        /// <param name="password">Password per la decifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Decrypt(string password)
        {
            try
            {
                return this.iObj.Decrypt(password);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion

        #region Metodi pubblici



        /// <summary>
        /// Elimina l'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Delete()
        {
            try
            {
                return this.iObj.Delete();
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
            finally
            {
                this.Refresh();
            }
        }

        /// <summary>
        /// Aggiorna le informazioni dell'istanza
        /// </summary>
        /// <example>
        /// Se si presume che il file system sia stato modificato dopo l'inizializzazione dell'istanza si 
        /// può utilizzare questo metodo per rieseguire rapidamente l'inizializzazione
        /// <code>
        /// FileSystemMgr fso = new FileSystemMgr(@"C:\Temp");
        /// 
        /// // un'azione esterna elimina la cartella 
        /// // ..
        /// // ...
        /// 
        /// fso.Refresh(); // aggiorna le informazioni sul path
        /// </code>
        /// </example>
        public void Refresh()
        {
            // Verifica il path assegnato all'istanza
            this.testPath();
        }

        /// <summary>
        /// Restituisce una rappresentazione a stringa dell'istanza
        /// </summary>
        /// <returns>Rappresentazione a stringa dell'istanza</returns>
        public override string ToString()
        {
            return this.Path;
        }

        /// <summary>
        /// Crea l'elemento del file sistem se non esiste
        /// </summary>
        /// <returns>Se la creazione riesce viene restituita una nuova istanza di <see cref="Mx.Core.IO.IFso"/> riferita alla copia dell'elemento
        /// altrimenti viene restituito null</returns>
        public FileSystemMgr Create(FileSystemTypes fsoElement)
        {
            try
            {
                if (!this.exist)
                    switch (fsoElement)
                    {
                        case FileSystemTypes.File:
                            new FileSystemMgr(System.IO.Path.GetDirectoryName(this.path)).Create(FileSystemTypes.Directory);
                            File.Create(this.path).Close();
                            break;
                        case FileSystemTypes.Directory:
                            Directory.CreateDirectory(this.path);
                            break;
                    }
                return this;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return this;
            }
            finally
            {
                this.Refresh();
            }
        }

        /// <summary>
        /// Crea, in una cartella di sistema, un link della shell che punta all'elemento del file system
        /// </summary>
        /// <param name="specialFolder">Cartella di sistema in cui inserire il link della shell</param>
        /// <returns>Istanza del link ottenuto</returns>
        /// <example>
        /// <para>L'esempio mostra come creare una cartella e posizionarne un link sul desktop.
        /// </para>
        /// <code>
        /// // inizializza l'istanza con il path della cartella 
        /// FileSystemMgr fso = new FileSystemMgr(@"C:\Temp\MyFolder");
        /// // crea la cartella se non esiste
        /// fso.Create(FileSystemTypes.Directory);
        /// // crea il relativo link sul desktop
        /// fso.CreateLink(Environment.SpecialFolder.Desktop);
        /// </code>
        /// </example>
        public ShellLink CreateLink(Environment.SpecialFolder specialFolder)
        {
            return CreateLink(Environment.GetFolderPath(specialFolder));
        }

        /// <summary>
        /// Crea, in una cartella, un link della shell che punta all'elemento del file system
        /// </summary>
        /// <param name="genericFolder">Cartella in cui inserire il link della shell</param>
        /// <returns>Istanza del link ottenuto</returns>
        /// <example>
        /// <para>L'esempio mostra come creare due cartelle e posizionare il link della prima nella seconda.
        /// </para>
        /// <code>
        /// // crea la prima cartella
        /// IFso fso1 = new FileSystemMgr(@"C:\Temp\MyFolder\Fold01").Create(FileSystemTypes.Directory);
        /// // crea la seconda cartella
        /// IFso fso2 = new FileSystemMgr(@"C:\Temp\MyFolder\Fold02").Create(FileSystemTypes.Directory);
        /// // crea il link della prima cartella nella seconda
        /// fso1.CreateLink(fso2.Path);
        /// </code>
        /// </example>
        public ShellLink CreateLink(string genericFolder)
        {
            return CreateLink(genericFolder, System.IO.Path.GetFileNameWithoutExtension(this.path));
        }

        /// <summary>
        /// Crea, in una cartella di sistema, un link della shell che punta all'elemento del file system
        /// </summary>
        /// <param name="specialFolder">Cartella di sistema in cui inserire il link della shell</param>
        /// <param name="linkName">Nome del link</param>
        /// <returns>Istanza del link</returns>
        /// <example>
        /// <para>L'esempio mostra come creare una cartella e posizionarne un link sul desktop.
        /// </para>
        /// <code>
        /// // inizializza l'istanza con il path della cartella 
        /// FileSystemMgr fso = new FileSystemMgr(@"C:\Temp\MyFolder");
        /// // crea la cartella se non esiste
        /// fso.Create(FileSystemTypes.Directory);
        /// // crea il relativo link sul desktop
        /// fso.CreateLink(Environment.SpecialFolder.Desktop, "NewLink");
        /// </code>
        /// </example>
        public ShellLink CreateLink(Environment.SpecialFolder specialFolder, string linkName)
        {
            return CreateLink(Environment.GetFolderPath(specialFolder), linkName);
        }

        /// <summary>
        /// Crea, in una cartella, un link della shell che punta all'elemento del file system
        /// </summary>
        /// <param name="linkFolder">Cartella in cui inserire il link della shell</param>
        /// <param name="linkName">Nome del link</param>
        /// <returns>Istanza del link</returns>
        /// <example>
        /// <para>Come per <see cref="Mx.Core.IO.FileSystemMgr.CreateLink(System.Environment.SpecialFolder, string)"/> specificando il path di una cartella qualsiasi.
        /// </para>
        /// </example>        
        public ShellLink CreateLink(string linkFolder, string linkName)
        {
            ShellLink shellLink = new ShellLink(linkFolder, this.Path, linkName);
            shellLink.SetLink();
            return shellLink;
        }

        /// <summary>
        /// Apre l'elemento con l'applicazione associata nel sistema
        /// </summary>
        /// <returns>True se l'operazione riesce, altrimetni false</returns>
        public bool Run()
        {
            bool retVal = false;
            if (!this.Exists) return retVal;
            try
            {
                Process.Start(this.Path);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }

        /// <summary>
        /// Restituisce il path relativo alla cartella di root indicata
        /// </summary>
        /// <param name="rootFolder">Cartella di root per cui calcolare il path</param>
        /// <returns>Path relativo</returns>
        public string GetPathRelative(string rootFolder) { return FileSystemMgr.GetRelativePath(rootFolder, this.Path); }

        /// <summary>
        /// Restituisce il path finale applicando il path relativo al path corrente
        /// </summary>
        /// <param name="relativePath">Path relativo</param>
        /// <returns>Path assoluto</returns>
        public string GetPathAbsolute(string relativePath) { return FileSystemMgr.GetAbsolutePath(this.Path, relativePath); }

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <param name="toDoListFilePath">To do list file path.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool WriteTreeInToDoList(string toDoListFilePath)
        {
            return this.iObj.WriteTreeInToDoList(toDoListFilePath);
        }

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool WriteTreeInToDoList()
        {
            return this.iObj.WriteTreeInToDoList();
        }
        #endregion

        #region Metodi privati
        /// <summary>
        /// Verifica il path assegnato all'istanza
        /// </summary>
        private void testPath()
        {
            if (string.IsNullOrEmpty(this.path)) return;

            // verifico se si tratta di un file o di una cartella
            this.isFile = File.Exists(this.path);
            if (!this.isFile)
                this.isDirectory = Directory.Exists(this.path);

            this.exist = this.isFile || this.isDirectory;

            // determino il tipo dell'elemento
            if (this.exist)
                this.fsoType = this.isFile ? FileSystemTypes.File : FileSystemTypes.Directory;
            else
                this.fsoType = FileSystemTypes.UnDefine;

            switch (this.fsoType)
            {
                case FileSystemTypes.UnDefine:
                    this.iObj = null;
                    break;
                case FileSystemTypes.File:
                    if (this.exist)
                        this.iObj = new FileElement(this.path);
                    break;
                case FileSystemTypes.Directory:
                    if (this.exist)
                        this.iObj = new DirectoryElement(this.path);
                    break;
            }

            //switch (this.fsoType)
            //{
            //    case FileSystemTypes.Directory:
            //    case FileSystemTypes.File:
            //        break;
            //    case FileSystemTypes.UnDefine:
            //    default:
            //        break;
            //}
            this.fsoParentFolder = new FileSystemMgr(System.IO.Path.GetDirectoryName(this.path));

        }
        #endregion
    }
}

