using System;
using System.Windows.Forms;
using System.IO;
using IWshRuntimeLibrary;

using Mlc.Shell;
using Mlc.Shell.IO;


namespace Mlc.Shell.IO
{
    /// <summary>
    /// Permette la getione di un link della shell
    /// </summary>
    /// <example>
    /// <para>Utilizzando un'istanza di questa classe si pouò gestire un link della shell (collegamento ad un file o ad una cartella)</para>
    /// <para>Il seguente esempio crea un link sul Descktop di nome "TmpFolder" che punta alla cartella "C:\Temp\Personale"
    /// </para>
    /// <code>
    /// ShellLink linkToTmpFolder = new ShellLink(Environment.SpecialFolder.Desktop, @"C:\Temp\Personale", "TmpFolder");
    /// IShellLinkInfo linkInfo = linkToTmpFolder.SetLink();
    /// </code>
    /// <para>
    /// Il metodo <see cref="Mlc.Shell.IO.ShellLink.SetLink()"/> crea il link (se esiste già lo aggiorna) e restituisce un'istanza di <see cref="Mx.Core.IO.IShellLinkInfo"/> che rappresenta i dati del link ottenuto.
    /// </para>
    /// </example>
    /// <seealso cref="Mlc.Shell.IO.FileSystemMgr"/>
    /// <seealso cref="Mlc.Shell.IO.ShellLinkInfo"/>
    public class ShellLink
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        #endregion

        #region Campi
        /// <summary>Elemento a cui punta il link</summary>
        private FileSystemMgr fsoTargetElement = null;
        /// <summary>Elemto fso del Link</summary>
        private FileSystemMgr fsoFileLink = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="linkDirectory">Cartella speciale di sistema in cui localizzare il link</param>
        /// <param name="pathTarget">Full path dell'elemento a cui punta il link</param>
        /// <param name="linkName">Nome del link</param>
        /// <example>
        /// <para>L'esempio seguente inizializza l'istanza per creare un link di un cartella sul desktop
        /// </para>
        /// <code>
        /// ShellLink linkToTmpFolder = new ShellLink(Environment.SpecialFolder.Desktop, @"C:\Temp\Personale", "TmpFolder");
        /// // Crea il link
        /// IShellLinkInfo linkInfo = linkToTmpFolder.SetLink();
        /// </code>
        /// </example>
        public ShellLink(Environment.SpecialFolder linkDirectory, string pathTarget, string linkName)
            : this(Environment.GetFolderPath(linkDirectory), new FileSystemMgr(pathTarget), linkName)
        { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="linkDirectory">Fullpath della cartella in cui localizzare il link</param>
        /// <param name="pathTarget">Full path dell'elemento a cui punta il link</param>
        /// <param name="linkName">Nome del link</param>
        /// <example>
        /// <para>L'esempio seguente inizializza l'istanza per creare un link che lancia wordpad.exe in una cartella specificata
        /// </para>
        /// <code>
        /// ShellLink linkToWordPad = new ShellLink("C:\Temp", @"%ProgramFiles%\Windows NT\Accessories\wordpad.exe", "WordPad");
        /// // Crea il link
        /// IShellLinkInfo linkInfo = linkToWordPad.SetLink();
        /// </code>
        /// </example>
        public ShellLink(string linkDirectory, string pathTarget, string linkName)
            : this(linkDirectory, new FileSystemMgr(pathTarget), linkName)
        { }

        /// <summary>
        /// Inizializza una nuova istanza per 
        /// </summary>
        /// <param name="linkDirectory">Fullpath della cartella in cui localizzare il link</param>
        /// <param name="fsoTargetElement">Elemento <see cref="Mx.Core.IO.FileSystemMgr"/> a cui punta il link</param>
        /// <param name="linkName">Nome del link (senza estensione ".lnk")</param>
        /// <example>
        /// <para>L'esempio seguente inizializza l'istanza per creare un link che lancia wordpad.exe in una cartella specificata
        /// </para>
        /// <code>
        /// // Inizializza l'istanza Mx.Core.IO.FileSystemMgr da associare al link
        /// FileSystemMgr fsoLink = new FileSystemMgr(@"%ProgramFiles%\Windows NT\Accessories\wordpad.exe");
        /// ShellLink linkToWordPad = new ShellLink("C:\Temp", fsoLink, "WordPad");
        /// // Crea il link
        /// IShellLinkInfo linkInfo = linkToWordPad.SetLink();
        /// </code>
        /// </example>
        public ShellLink(string linkDirectory, FileSystemMgr fsoTargetElement, string linkName)
            : this(string.Concat(linkDirectory, Chars.BackSlash, linkName, FsoExt.Lnk))
        {


            this.fsoTargetElement = fsoTargetElement;
            this.fsoFileLink = new FileSystemMgr(this.linkInfo.PathFileLink);
        }

        /// <summary>
        /// Inizializza una nuova istanza con le informazioni del link della shell
        /// </summary>
        /// <param name="linkInfo">informazioni del link della shell</param>
        /// <example>
        /// <para>L'esempio seguente inizializza l'istanza per creare un link che lancia wordpad.exe in una cartella specificata
        /// </para>
        /// <code>
        /// // inizializza l'istanza con le informazioni del link
        /// IShellLinkInfo lnkInfo = new ShellLinkInfo();
        /// lnkInfo.Description = "Commento del link";                                                          // descrizione del link
        /// lnkInfo.PathTarget = @"%ProgramFiles%\Windows NT\Accessories\wordpad.exe";                          // file puntato dal link
        /// lnkInfo.PathFileLink = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\WordPad";  // file del link
        /// lnkInfo.Arguments = @"C:\Temp\Readme.txt";                                                          // argomenti della linea di comando
        /// ShellLink linkToWordPad = new ShellLink(lnkInfo);
        /// // Crea il link
        /// IShellLinkInfo linkInfo = linkToWordPad.SetLink();
        /// </code>
        /// </example>
        public ShellLink(ShellLinkInfo linkInfo)
        {
            this.setInstance(linkInfo);
        }

        /// <summary>
        /// Inizializza una nuova istanza con il path del file link
        /// </summary>
        /// <param name="fileLink">Fullpath del file link (con o senza estensione ".lnk")</param>
        /// <example>
        /// <para>Questo sovraccarico permette di inizializzare l'istanza puntando un file .lnk esistente
        /// </para>
        /// <code>
        /// // inizializzo l'istanza utilizzando il fullpath del file .lnk
        /// ShellLink linkToWordPad = new ShellLink(Environment.SpecialFolder.Desktop + @"\WordPad");
        /// </code>
        /// <para>E utilile quando si voglia modificare un file .lnk esistente o semplicemente ottenerne le informazioni</para>
        /// <code>
        /// Console.WriteLine("Argomenti:   {0}", linkToWordPad.LinkInfo.Arguments);
        /// Console.WriteLine("Descrizione: {0}", linkToWordPad.LinkInfo.PathTarget);
        /// Console.WriteLine("Target:      {0}", linkToWordPad.LinkInfo.Description);
        /// </code>
        /// </example>
        public ShellLink(string fileLink)
        {
            if (!fileLink.EndsWith(FsoExt.Lnk))
                fileLink += FsoExt.Lnk;

            this.fsoFileLink = new FileSystemMgr(fileLink);
            this.setInstance(new ShellLinkInfo(fileLink));
        }

        #endregion

        #region Proprietà
        /// <summary>
        /// Restituisce un valore che determina l'esistenza del link
        /// </summary>
        public bool Exist { get { return this.fsoFileLink.Exists; } }

        private ShellLinkInfo linkInfo = null;
        /// <summary>
        /// Restituisce le informazioni del link
        /// </summary>
        public ShellLinkInfo LinkInfo { get { return this.linkInfo; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Imposta e crea il link
        /// </summary>
        /// <returns>L'istanza che espone <see cref="Mx.Core.IO.IShellLinkInfo"/> che rappresenta il link della shell creato</returns>
        public ShellLinkInfo SetLink()
        {
            // Se l'elemento puntato dal link esiste
            if (this.fsoTargetElement.Exists)
                this.DeleteLink(DeleteLinkMode.OnlyLink);

            WshShell shell = new WshShell();
            IWshShortcut link = (IWshShortcut)(object)shell.CreateShortcut(this.fsoFileLink.Path);

            if (!string.IsNullOrEmpty(this.linkInfo.Arguments))
                link.Arguments = this.linkInfo.Arguments;

            if (!string.IsNullOrEmpty(this.linkInfo.Description))
                link.Description = this.linkInfo.Description;

            link.TargetPath = string.Concat(this.fsoTargetElement.Path);
            link.IconLocation = this.fsoTargetElement.Path;
            link.WorkingDirectory = Path.GetDirectoryName(this.fsoTargetElement.Path);
            link.Save();
            this.linkInfo = new ShellLinkInfo(this.fsoFileLink.Path);
            return this.linkInfo;
        }

        /// <summary>
        /// Elimina il link rappresentato dall'istanza
        /// </summary> 
        /// <returns><c>true</c> se l'liminazione riesce altrimenti <c>false</c></returns>
        /// <example>
        /// <para>L'esempio seguente elimina dal desktop il file .lnk che punta all'applicazione WordPad.exe.
        /// </para>
        /// <code>
        /// // inizializzo l'istanza utilizzando il fullpath del file .lnk
        /// ShellLink linkToWordPad = new ShellLink(Environment.SpecialFolder.Desktop + @"\WordPad");
        /// // elimina il file .lnk
        /// linkToWordPad.DeleteLink();
        /// </code>
        /// </example>        
        public bool DeleteLink()
        {
            return this.DeleteLink(DeleteLinkMode.OnlyLink);
        }

        /// <summary>
        /// Elimina il link rappresentato dall'istanza utilizzando una delle modalità previste da <see cref="Mx.Core.DeleteLinkMode"/>
        /// </summary>
        ///<param name="deleteMode">Modo di eliminazione del link della shell</param>
        /// <returns><c>true</c> se l'liminazione riesce altrimenti <c>false</c></returns>
        /// <example>
        /// <para>L'esempio seguente elimina dal desktop il file .lnk che punta all'applicazione WordPad.exe. E` equivalente al
        /// metodo <see cref="Mx.Core.IO.ShellLink.DeleteLink()"/>
        /// </para>
        /// <code>
        /// // inizializzo l'istanza utilizzando il fullpath del file .lnk
        /// ShellLink linkToWordPad = new ShellLink(Environment.SpecialFolder.Desktop + @"\WordPad");
        /// // elimina il file .lnk
        /// linkToWordPad.DeleteLink(DeleteLinkMode.OnlyLink);
        /// </code>
        /// <para>Il secondo esempio elimina dal desktop il file .lnk che punta all'applicazione WordPad.exe, ed elimina anche il file WordPad.exe stesso dalla cartella in cui si trova
        /// </para>
        /// <code>
        /// // inizializzo l'istanza utilizzando il fullpath del file .lnk
        /// ShellLink linkToWordPad = new ShellLink(Environment.SpecialFolder.Desktop + @"\WordPad");
        /// // elimina il file .lnk e il file puntato
        /// linkToWordPad.DeleteLink(DeleteLinkMode.LinkAndTarget);
        /// </code>
        /// <para>L'ultimo esempio elimina dal desktop il file .lnk che punta all'applicazione WordPad.exe, ed elimina anche il file WordPad.exe con la cartella in cui si trova
        /// </para>
        /// <para>Utilizzare questo sovraccarico con estrema cautela verificando che l'eliminazione della cartella del file puntato dal link non crei danni al sistema o ai dati</para>
        /// <code>
        /// // inizializzo l'istanza utilizzando il fullpath del file .lnk
        /// ShellLink linkToWordPad = new ShellLink(Environment.SpecialFolder.Desktop + @"\WordPad");
        /// // elimina il file .lnk e la cartella che contiene il file puntato
        /// linkToWordPad.DeleteLink(DeleteLinkMode.LinkTargetFileAndFolderParent);
        /// </code>
        /// </example>      
        public bool DeleteLink(DeleteLinkMode deleteMode)
        {
            bool retVal = false;
            this.fsoFileLink.Refresh();
            if (!this.fsoFileLink.Exists) return retVal;
            switch (deleteMode)
            {
                case DeleteLinkMode.OnlyLink:
                    this.fsoFileLink.Delete();
                    break;
                case DeleteLinkMode.LinkAndTarget:
                    this.fsoFileLink.Delete();
                    new FileSystemMgr(this.linkInfo.PathTarget).Delete();
                    break;
                case DeleteLinkMode.LinkTargetFileAndFolderParent:
                    this.fsoFileLink.Delete();
                    FileSystemMgr fso = new FileSystemMgr(this.linkInfo.PathTarget);
                    if (fso.IsFile)
                        fso.FsoParentFolder.Delete();
                    else
                        fso.Delete();
                    break;
                default:
                    break;
            }
            return retVal;
        }
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        /// <summary>
        /// Inizializza una nuova istanza con le informazioni del link della shell
        /// </summary>
        /// <param name="linkInfo">informazioni del link della shell</param>
        private void setInstance(ShellLinkInfo linkInfo)
        {
            this.linkInfo = linkInfo;
            this.fsoTargetElement = new FileSystemMgr(this.linkInfo.PathTarget);
            this.fsoFileLink = new FileSystemMgr(this.linkInfo.PathFileLink);
        }
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion
    }
}
