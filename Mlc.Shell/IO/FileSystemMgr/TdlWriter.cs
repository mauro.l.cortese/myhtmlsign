﻿// ***********************************************************************
// Assembly         : Mlc.Tools
// Author           : Cortese Mauro Luigi
// Created          : 09-22-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 09-22-2015
// ***********************************************************************
// <copyright file="TdlWriter.cs" company="Microsoft">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.IO;
using System.Xml;

using Mlc.Shell;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class TdlWriter.
	/// </summary>
	public class TdlWriter
	{

		#region Fields
		/// <summary>
		/// The root path
		/// </summary>
		private string rootPath;
		/// <summary>
		/// The identifier count
		/// </summary>
		private int idCount = 0;
		/// <summary>
		/// The XML document
		/// </summary>
		private XmlDocument xmlDoc = new XmlDocument();
        /// <summary>
        /// The use relative path
        /// </summary>
        private bool useRelativePath = true;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="TdlWriter"/> class.
		/// </summary>
		/// <param name="fso">The fso.</param>
		public TdlWriter(FileSystemMgr fso)
			: this(fso.Path, string.Concat(fso.Path, '\\', Path.GetFileName(fso.Path)))
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="TdlWriter"/> class.
		/// </summary>
		/// <param name="fso">The fso.</param>
		/// <param name="fileTdl">The file TDL.</param>
		public TdlWriter(FileSystemMgr fso, string fileTdl)
			: this(fso.Path, fileTdl)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="TdlWriter"/> class.
		/// </summary>
		/// <param name="path">The path.</param>
		public TdlWriter(string path)
			: this(path, string.Concat(path, '\\', Path.GetFileName(path)))
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="TdlWriter"/> class.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="fileTdl">The file TDL.</param>
		public TdlWriter(string path, string fileTdl)
		{
			this.rootPath = path;
			this.toDoListFilePath = fileTdl;
			//this.fileTdl = string.Concat(path, '\\', fileTdl);
			if (!this.toDoListFilePath.ToLower().EndsWith(".tdl"))
				this.toDoListFilePath += ".tdl";
		}        
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// To do list file path
		/// </summary>
		private string toDoListFilePath = null;
		/// <summary>
		/// Gets to do list file path.
		/// </summary>
		/// <value>To do list file path.</value>
		public string ToDoListFilePath { get { return this.toDoListFilePath; } }
		#endregion

		#region Method

        /// <summary>
        /// Writes the ToDoList File
        /// </summary>
        public void Write()
        {
            this.Write(true);
        }

        /// <summary>
        /// Writes the ToDoList File
        /// </summary>
        /// <param name="useRelativePath">if set to <c>true</c> use relative path.</param>
		public void Write(bool useRelativePath)
		{
            this.useRelativePath = useRelativePath;

			/* Formato .xml del file .tdl
				<?xml version="1.0" encoding="utf-16" ?>
				<TODOLIST PROJECTNAME="PROVA">
				<TASK TITLE="RootFolder 01" ID="1" FILEREFPATH="D:\tmp" ICONINDEX="0">
					<TASK TITLE="Child 1" ID="2" FILEREFPATH="D:\tmp\tmp1.txt" ICONINDEX="36"/>
					<TASK TITLE="SubFolder 01" ID="3" FILEREFPATH="D:\tmp" ICONINDEX="0">
						<TASK TITLE="Child 2" ID="4" FILEREFPATH="D:\tmp\tmp1.txt" ICONINDEX="36"/>
					</TASK>
				</TASK>
				</TODOLIST>
			 * 
			 * 
				<TODOLIST PROJECTNAME="D:\temp">
					<TASK TITLE="temp" ID="1" FILEREFPATH="D:\temp" ICONINDEX="0" />
				</TODOLIST>* 
			 */

			XmlNode rootNode = this.createNode("TODOLIST");
			this.appendAttr(rootNode, "PROJECTNAME", rootPath);
			xmlDoc.AppendChild(rootNode);

			// creo il nodo della cartella root
			XmlNode folderNode = this.appendFolderNode(rootNode, rootPath, "0");
			// ricorre la struttura dei cartelle
			this.fsoRecursor(folderNode, rootPath);

			xmlDoc.Save(this.toDoListFilePath);
		}
		#endregion
		#endregion

		#region Private
		#region Method
		/// <summary>
		/// Fsoes the recursor.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="folderPath">The folder path.</param>
		private void fsoRecursor(XmlNode parentNode, string folderPath)
		{
			// aggiungo tutti i nodi delle sottocartelle
			string[] folders = Directory.GetDirectories(folderPath, "*", SearchOption.TopDirectoryOnly);
			EnumOperator<FileAttributes> eo = new EnumOperator<FileAttributes>();
			foreach (string folder in folders)
			{
				if (!eo.Contains(File.GetAttributes(folder), FileAttributes.Hidden))
				{
					XmlNode folderNode = this.appendFolderNode(parentNode, folder, "0");
					this.fsoRecursor(folderNode, folder);
				}
			}

			// aggiungo i files della cartella corrente
			string[] files = Directory.GetFiles(folderPath, "*", SearchOption.TopDirectoryOnly);
			foreach (string file in files)
				this.appendFolderNode(parentNode, file, "36");
		}

		/// <summary>
		/// Appends the folder node.
		/// </summary>
		/// <param name="rootNode">The root node.</param>
		/// <param name="path">The path.</param>
		/// <param name="iconIndex">Index of the icon.</param>
		/// <returns>XmlNode.</returns>
		private XmlNode appendFolderNode(XmlNode rootNode, string path, string iconIndex)
		{
            string pathForLink = null;
            if (useRelativePath)
            {
                string relPath = FileSystemMgr.GetRelativePath(this.rootPath, path);
                if (relPath.StartsWith("\\"))
                    relPath = relPath.Substring(1);
                pathForLink = relPath;
            }
            else
                pathForLink = path;

			idCount++;

			XmlNode rootFolder = this.createNode("TASK");
			this.appendAttr(rootFolder, "TITLE", Path.GetFileName(path));
			this.appendAttr(rootFolder, "ID", idCount.ToString());
            this.appendAttr(rootFolder, "FILEREFPATH", pathForLink);
			this.appendAttr(rootFolder, "ICONINDEX", iconIndex);
			rootNode.AppendChild(rootFolder);
			return rootFolder;
		}

		/// <summary>
		/// Appends the attribute.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		private void appendAttr(XmlNode xmlNode, string name, string value)
		{
			XmlAttribute attribute = xmlDoc.CreateAttribute(name);
			attribute.Value = value;
			xmlNode.Attributes.Append(attribute);
		}

		/// <summary>
		/// Creates the node.
		/// </summary>
		/// <param name="nodeName">Name of the node.</param>
		/// <returns>XmlNode.</returns>
		private XmlNode createNode(string nodeName)
		{
			return xmlDoc.CreateElement(nodeName);
		}
		#endregion
		#endregion
	}
}
