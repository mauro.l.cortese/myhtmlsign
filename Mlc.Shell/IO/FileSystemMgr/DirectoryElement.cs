using System;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mlc.Shell;
using Mlc.Shell.Crypto;
using Mlc.Shell.IO;

//using Mx.Core.Constants;
//using Mx.Core.Enumerations;
//using Mx.Core.Crypto;
//using Mx.Core;

using SevenZip;


namespace Mlc.Shell.IO
{
    /// <summary>
    /// Implemetazione per le operazioni sulle directories
    /// </summary>
    internal class DirectoryElement : IObj
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Inizializza una nuova istanza 
        /// </summary>
        /// <param name="path">Fullpath della directory origine</param>
        public DirectoryElement(string path)
            : base(path)
        {
        }
        #endregion

        #region Public
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Internal
        #region Properties
        /// <summary>
        /// Restituisce la dimensione dell'elemento in byte
        /// </summary>
        /// <returns>Dimensione dell'elemento in byte</returns>
        internal override long Size
        {
            get { return this.getDirectorySize(); }
        }
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Copying the file system object at the specified location
        /// </summary>
        /// <param name="pathDest">Path to the copy destination</param>
        /// <param name="actionsCopy">Determines the action to be taken on new files generated, the possible actions are in the enumeration <see cref="Mlc.Shell.IO.ActionsCopy"/></param>
        /// <param name="resulCopyCompress">Determines the possible results of the copy of a file system object for <see cref="Mlc.Shell.IO.ActionsCopy.Compress"/> </param>
        /// <param name="password">Password that wil be use for data encryption, if value is null will be used the password of the constant <see cref="Mlc.Shell.Constants.DefaultPwd" /></param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Copy(ref string pathDest, ActionsCopy actionsCopy, ResultCopyCompress resulCopyCompress = ResultCopyCompress.SingleObject, string password = Constants.DefaultPwd)
        {
            bool retVal = true;
            try
            {
                // se la directory esiste
                if (Directory.Exists(base.path))
                {
                    // ottengo l'elenco delle sotto directories da copiare
                    string[] dirs = Directory.GetDirectories(base.path, "*", SearchOption.AllDirectories);
                    // ciclo per creare le sottodirectory
                    foreach (string dir in dirs)
                        // creo il path di destinazione
                        Directory.CreateDirectory(getDestinationPath(pathDest, dir, ActionsCopy.None));
                    if (dirs.Length == 0)
                        Directory.CreateDirectory(pathDest);

                    // ottengo l'elenco dei files da copiare
                    string[] files = Directory.GetFiles(base.path, "*", SearchOption.AllDirectories);
                    // ciclo per copiare i files

                    FileSystemMgr fsoCopyFile = new FileSystemMgr();

                    foreach (string file in files)
                    {

                        string fileDest = getDestinationPath(pathDest, file, actionsCopy);

                        try
                        {
                            // copio il file
                            fsoCopyFile.Path = file;
                            if (!fsoCopyFile.Copy(fileDest, actionsCopy, password: password).Exists)
                                throw new Exception(string.Format("Copia di {0} fallita!", file));

                            Console.WriteLine("Copia di {0}", file);
                            //File.Copy(file, fileDest, true);
                        }
                        catch (Exception ex)
                        {
                            ExceptionsRegistry.GetInstance().Add(ex);
                            retVal = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                retVal = false;
            }
            return retVal;
        }

        /// <summary>
        /// Esegue la cifratura del file
        /// </summary>
        /// <param name="password">Password per la cifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Encrypt(string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Esegue la decifratura del file
        /// </summary>
        /// <param name="password">Password per la decifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Decrypt(string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Elimina l'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Delete()
        {
            try
            {
                FolderRecursive folderRecursive = new FolderRecursive();
                folderRecursive.Delete(base.path, true);
                return folderRecursive.DeleteSuccess;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Comprime i dati dell'elemento del file system nel file specificato
        /// </summary>
        /// <param name="fileDest">Fullpath del file di destinazione dei dati compressi (formato .7z)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Compress(string fileDest)
        {
            try
            {
                SevenZipCompressor szc = new SevenZipCompressor();
                szc.CompressDirectory(this.path, fileDest);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Estrae i dati compressi dal file puntato dall'istanza nella directory specificata
        /// </summary>
        /// <param name="folderDest">Fullpath della cartella in cui estrarre i dati compressi (formato .7z)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Extract(string folderDest)
        {
            return false;
        }


        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <param name="toDoListFilePath">To do list file path.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool WriteTreeInToDoList(string toDoListFilePath)
        {
            TdlWriter tdlWriter = new TdlWriter(this.path, toDoListFilePath);
            tdlWriter.Write(false);
            return new FileSystemMgr(tdlWriter.ToDoListFilePath).Exists;
        }


        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool WriteTreeInToDoList()
        {
            TdlWriter tdlWriter = new TdlWriter(this.path);
            tdlWriter.Write();
            return new FileSystemMgr(tdlWriter.ToDoListFilePath).Exists;
        }


        /// <summary>
        /// Lock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool Lock()
        {
            return setAccessRule(true);
        }

        /// <summary>
        /// Unlock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal override bool UnLock()
        {
            return setAccessRule(false);
        }
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="destPath">path di destinazione per la cartella</param>
        /// <param name="sourcePath">path corrente dell'elemento da copiare</param>
        /// <param name="actionsCopy">Determina l'azione da compiere sui nuovi files generati, le azioni previste sono quelle dell'enumerazione <see cref="Mx.Core.AdvancedCopyActions"/></param>
        /// <returns>Nuovo path di destinazione dell'elemento</returns>
        private string getDestinationPath(string destPath, string sourcePath, ActionsCopy actionsCopy)
        {

            string retVal = destPath + sourcePath.Substring(base.path.Length);


            switch (actionsCopy)
            {
                case ActionsCopy.None:
                    break;
                case ActionsCopy.Compress:
                    retVal = this.addExtension(retVal, ".7z");
                    break;
                //case ActionsCopy.CompressSingleFile:
                //    break;
                case ActionsCopy.Extract:
                    retVal = retVal.Replace(".7zcr", ".cr");
                    retVal = this.removeExtension(retVal, ".7z");
                    break;
                case ActionsCopy.Encrypt:
                    retVal = this.addExtension(retVal, ".cr");
                    break;
                case ActionsCopy.Decrypt:
                    retVal = retVal.Replace(".7zcr", ".7z");
                    retVal = this.removeExtension(retVal, ".cr");
                    break;
                //case ActionsCopy.CompressEncrypt:
                //    retVal = this.addExtension(retVal, ".7zcr");
                //    break;
                //case ActionsCopy.CompressEncryptSingleFile:
                //    break;
                //case ActionsCopy.DecryptExtract:
                //    retVal = this.removeExtension(retVal, ".7zcr");
                //    break;
            }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="removeExt"></param>
        /// <returns></returns>
        private string removeExtension(string path, string removeExt)
        {
            string ext = Path.GetExtension(path).ToLower();
            if (ext.Equals(removeExt))
                return path.Substring(0, path.Length - removeExt.Length);

            return path;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="newExt"></param>
        /// <returns></returns>
        private string addExtension(string path, string newExt)
        {
            string ext = Path.GetExtension(path).ToLower();

            if (!string.IsNullOrEmpty(ext))
                return string.Concat(path, newExt);

            return path;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="newExt"></param>
        /// <returns></returns>
        private string replaceExtension(string path, string newExt)
        {
            string ext = Path.GetExtension(path).ToLower();

            if (!string.IsNullOrEmpty(ext))
                return path.Substring(0, path.Length - ext.Length) + newExt;

            return path;
        }

        /// <summary>
        /// Ottiene la dimensione in byte della directory 
        /// </summary>
        private long getDirectorySize()
        {
            long tsize = 0;

            FileInfo[] fi = new DirectoryInfo(base.path).GetFiles("*", SearchOption.AllDirectories);

            foreach (FileInfo f in fi)
                tsize += f.Length;
            return tsize;
        }

        /// <summary>
        /// Sets the access rule.
        /// </summary>
        /// <param name="lockAccess">if set to <c>true</c> lock access, otherwise if set to <c>false</c> unlock access.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        private bool setAccessRule(bool lockAccess)
        {
            try
            {
                string adminUserName = Environment.UserName;
                DirectorySecurity ds = Directory.GetAccessControl(this.path);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.Read | FileSystemRights.Delete, AccessControlType.Deny);
                if (lockAccess)
                    ds.AddAccessRule(fsa);
                else
                    ds.RemoveAccessRule(fsa);
                Directory.SetAccessControl(this.path, ds);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        /// <summary>
        /// Elimina ricorsivamente una struttura di cartelle e files
        /// </summary>
        private class FolderRecursive
        {
            #region Campi
            /// <summary>Cartella di partenza</summary>
            private string rootFolder;
            /// <summary>Determina la rimozione della cartella di partenza</summary>
            private bool removeRootFolder;
            /// <summary>Lista dei files non eliminati</summary>
            private List<string> noDeleteFiles = new List<string>();
            #endregion

            #region Proprietà
            /// <summary>Lista delle cartelle che sono escluse dalla ricorsione</summary>
            private List<string> excludedFolders = new List<string>();
            /// <summary>
            /// Restituisce o imposta la lista delle cartelle che sono escluse dalla ricorsione
            /// </summary>    
            public List<string> ExcludedFolders { get { return this.excludedFolders; } set { this.excludedFolders = value; } }

            /// <summary>
            /// Restituisce true se la cartella è stata svuota, altrimenti false
            /// </summary>
            public bool DeleteSuccess { get { return this.noDeleteFiles.Count == 0; } }

            #endregion

            #region Metodi pubblici
            /// <summary>
            /// Ricorre la struttura di cartelle e ne elimina il contenuto
            /// </summary>
            /// <param name="startFolder">Fullpath della cartella da eliminare o svuotare</param>
            /// <param name="removeRootFolder">True rimuove anche la cartella passata, altrimenti ne svuota solo il contenuto</param>
            public void Delete(string startFolder, bool removeRootFolder)
            {
                if (this.excludedFolders == null)
                    this.excludedFolders = new List<string>();
                this.rootFolder = startFolder;
                this.removeRootFolder = removeRootFolder;
                this.delete(startFolder);
            }
            #endregion

            #region Metodi privati
            /// <summary>
            /// Ricorre la struttura di cartelle e ne elimina il contenuto
            /// </summary>
            /// <param name="startFolder">Fullpath della cartella da eliminare o svuotare</param>
            private void delete(string startFolder)
            {
                string[] subFolders = Directory.GetDirectories(startFolder);
                string[] files = Directory.GetFiles(startFolder);

                foreach (string subFolder in subFolders)
                    if (!this.excludedFolders.Contains(System.IO.Path.GetFileName(subFolder)))
                        this.delete(subFolder);

                foreach (string file in files)
                    try { File.Delete(file); }
                    catch (Exception ex)
                    {
                        ExceptionsRegistry.GetInstance().Add(ex);
                        noDeleteFiles.Add(file);
                    }

                // se sono sulla root e non la devo eliminare abbandono
                if (startFolder == this.rootFolder && !removeRootFolder) return;

                try { Directory.Delete(startFolder); }
                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            }
            #endregion
        }        
        #endregion
    }
}
