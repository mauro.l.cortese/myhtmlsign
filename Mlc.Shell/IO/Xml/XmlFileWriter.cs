using System.Xml;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe per scrittura semplificata di un file XML.
    /// </summary>
    public class XmlFileWriter : XmlWriter
    {
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        /// <param name="fileXml">Fullpath del file xml</param>
        public XmlFileWriter(string fileXml)
        {
            this.fileXml = fileXml;
        }

        /// <summary>Fullpath del file xml</summary>
        private string fileXml = null;
        /// <summary>
        /// Imposta o restituisce il fullpath del file xml
        /// </summary>
        public string FileXml { get { return fileXml; } set { fileXml = value; } }

        /// <summary>
        /// Apre e inizializza il file xml
        /// </summary>
        /// <param name="rootName">Nome del nodo root</param>
        public override void OpenXML(string rootName)
        {
            if (fileXml == null) return;
            base.XmlTextWriter = new XmlTextWriter(fileXml, System.Text.Encoding.Unicode);
            base.OpenXML(rootName);
        }
    }
}
