﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-23-2017
// ***********************************************************************
// <copyright file="IViewLogger.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Interface IViewLogger
    /// </summary>
    public interface IViewLogger
    {
        /// <summary>
        /// Gets or sets the logger list.
        /// </summary>
        /// <value>The logger list.</value>
        LogList LogList { get; set; }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        void DataRefresh();
    }
}