﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-23-2017
// ***********************************************************************
// <copyright file="Logger.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class Logger.
    /// </summary>
    public class LogList : List<Log>
    {
        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        #endregion

        #region Public Properties        
        private IViewLogger iViewer = null;
        /// <summary>
        /// Gets or sets the i viewer.
        /// </summary>
        /// <value>The i viewer.</value>
        public IViewLogger IViewer
        {
            get { return this.iViewer; }
            set
            {
                this.iViewer = value;
                if (this.iViewer != null)
                    this.iViewer.LogList = this;
            }
        }
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method        
        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Add(string message)
        {
            this.Add(message, null, null);
        }

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="name">The name.</param>
        public void Add(string message, string name)
        {
            this.Add(message, name, null);
        }

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cmdName">The name.</param>
        /// <param name="pars">The pars.</param>
        public void Add(string message, string cmdName, params string[] pars)
        {
            string msg;
            if (pars != null)
                msg = string.Format(message, pars);
            else
                msg = message;

            base.Add(new Log(base.Count + 1, cmdName, msg));

            if (this.IViewer != null)
                this.IViewer.DataRefresh();
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }



    /// <summary>
    /// Class Log.
    /// </summary>
    public class Log
    {
        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Log"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="message">The message.</param>
        public Log(int index, string message)
            : this(index, string.Empty, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="cmdName">The name.</param>
        /// <param name="message">The message.</param>
        public Log(int index, string cmdName, string message)
        {
            this.name = cmdName;
            this.index = index;
            this.message = message;
            this.time = DateTime.Now;
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// The index
        /// </summary>
        private int index;
        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>The index.</value>
        public int Index { get { return this.index; } }

        /// <summary>
        /// The message
        /// </summary>
        private string name;
        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string CmdName { get { return this.name; } }

        /// <summary>
        /// The message
        /// </summary>
        private string message;
        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get { return this.message; } }

        /// <summary>
        /// The message
        /// </summary>
        private DateTime time;
        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public DateTime Time { get { return this.time; } }
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.time != null ? this.time.ToShortTimeString() : string.Empty;
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }

}
