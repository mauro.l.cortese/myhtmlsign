﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell
{
    /// <summary>
    /// Utilità per la gestione delle risorse
    /// </summary>
    public class EmbeddedResource
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        #endregion

        #region Campi
        private Assembly assembly = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        public EmbeddedResource(Assembly assembly)
        {
            this.assembly = assembly;
        }
        #endregion

        #region Proprietà
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce il vettore di byte della risorsa richiesta
        /// </summary>
        /// <param name="resourceName">Nome della risorsa</param>
        /// <returns>Vettore di byte della risorsa richiesta</returns>
        public byte[] ToByteArray(string resourceName)
        {
            Stream stream = this.getStreamResources(resourceName);
            if (stream == null) return null;
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        /// <summary>
        /// Restituisce il valore della risorsa come stringa
        /// </summary>
        /// <param name="resourceName">Nome della risorsa</param>
        /// <returns>Valore della risorsa come stringa</returns>
        public string ToString(string resourceName)
        {
            string retVal = null;
            Stream stream = this.getStreamResources(resourceName);
            if (stream == null) return null;
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            retVal = ASCIIEncoding.UTF8.GetString(buffer);
            return retVal;
        }

        /// <summary>
        /// Restituisce il name space della risorsa incorporata richiesta
        /// </summary>
        /// <param name="resourceName">Nome della risorsa</param>
        /// <param name="args">Argomenti di formattazione della stringa</param>
        /// <returns>Valore della risorsa come stringa</returns>
        public string ToString(string resourceName, params object[] args)
        {
            string res = this.ToString(resourceName);
            if (!string.IsNullOrEmpty(res))
                res = string.Format(res, args);
            return res;
        }
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati

        /// <summary>
        /// Restituisce uno stream sulla risorsa richiesta
        /// </summary>
        /// <param name="resourceName">Nome della risorsa</param>
        /// <returns>Stream sulla risorsa</returns>
        private Stream getStreamResources(string resourceName)
        {
            Stream stream = null;
            stream = this.assembly.GetManifestResourceStream(resourceName);
            return stream;
        }
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion

    }
}
