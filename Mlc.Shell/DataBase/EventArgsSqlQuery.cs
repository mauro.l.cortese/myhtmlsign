using System;
using System.Data.Common;

namespace Mlc.Shell.DataBase
{
    /// <summary>
    /// Delegato per la gestione dell'evento che occorre prima dell'esecuzione di un istruzione SQL.
    /// </summary>
    public delegate void EventHandlerBeforeSqlExecute(object sender, EventArgsSqlQuery e);

    /// <summary>
    /// Delegato per la gestione dell'evento che occorre prima dell'esecuzione di un istruzione SQL.
    /// </summary>
    public delegate void EventHandlerAfterSqlExecute(object sender, EventArgsSqlQuery e);

    /// <summary>
    /// Classe per gli argomenti della generazione dell'evento che occorre prima dell'esecuzione di un istruzione SQL.
    /// </summary>
    public class EventArgsSqlQuery : EventArgs
    {
        #region Costruttori
        /// <summary>
        /// Inizilizza una nuova istanza
        /// </summary>
        /// <param name="sql">Istruzione sql da eseguire</param>
        public EventArgsSqlQuery(string sql)
            : this(null, sql)
        {
        }

        /// <summary>
        /// Inizilizza una nuova istanza
        /// </summary>
        /// <param name="sender">Istanza che sta per eseguire l'istruzione Sql</param>
        /// <param name="sql">Istruzione sql da eseguire</param>
        public EventArgsSqlQuery(object sqlSender, string sql)
            : this(sqlSender, sql, null)
        {
        }

        /// <summary>
        /// Inizilizza una nuova istanza
        /// </summary>
        /// <param name="sender">Istanza che sta per eseguire l'istruzione Sql</param>
        /// <param name="sql">Istruzione sql da eseguire</param>
        /// <param name="rowUpdatingEventArgs">Dati dell'evento RowUpdating del provider di dati</param>
        public EventArgsSqlQuery(object sqlSender, string sql, RowUpdatingEventArgs rowUpdatingEventArgs)
        {
            this.sqlSender = sqlSender;
            this.sql = sql;
            this.rowUpdatingEventArgs = rowUpdatingEventArgs;
        }
        #endregion

        #region Proprietà
        /// <summary>Istanza che sta per eseguire l'istruzione Sql</summary>
        private object sqlSender = null;
        /// <summary>
        /// Restituisce o imposta l'istanza che sta per eseguire l'istruzione Sql
        /// </summary>
        public object SqlSender { get { return sqlSender; } set { sqlSender = value; } }

        /// <summary>Istruzione sql da eseguire</summary>
        private string sql = null;
        /// <summary>
        /// Restituisce o imposta l'istruzione sql da eseguire
        /// </summary>
        public string Sql { get { return sql; } set { sql = value; } }


        /// <summary>Dati dell'evento RowUpdating del provider di dati</summary>
        private RowUpdatingEventArgs rowUpdatingEventArgs = null;
        /// <summary>
        /// Restituisce o imposta i dati dell'evento RowUpdating del provider di dati
        /// </summary>
        public RowUpdatingEventArgs RowUpdatingEventArgs { get { return rowUpdatingEventArgs; } set { rowUpdatingEventArgs = value; } }

        #endregion
    }



}
