using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Mlc.Shell.DataBase
{
    /// <summary>
    /// Dati di una connessione DB
    /// </summary>
    public class DbConnInfo : Dictionary<string, string>
    {

        public DbConnInfo(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        /// <summary>
        /// Restituisce o imposta la password dell'account di accesso
        /// </summary>
        public string Password { get; set; }

        /// <summary>stringa di connessione</summary>
        private string connectionString = null;
        /// <summary>
        /// Restituisce o imposta la stringa di connessione
        /// </summary>    
        public string ConnectionString
        {
            get { return this.connectionString; }
            set
            {
                this.connectionString = value;
                if (!string.IsNullOrEmpty(this.connectionString))
                {
                    this.Clear();
                    string[] args = this.connectionString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in args)
                    {
                        string[] subItem = item.Split('=');
                        if (subItem.Length == 2)
                            this.Add(subItem[0].Trim().ToUpper(), subItem[1].Trim());
                    }
                    if (this.ContainsKey("PASSWORD"))
                    {
                        this.Password = this["PASSWORD"];
                        this.Remove("PASSWORD");

                        DbConnectionStringBuilder bb = new DbConnectionStringBuilder();
                        foreach (KeyValuePair<string, string> item in this)
                            bb.Add(item.Key.ToLower(), item.Value.ToLower());
                        this.connectionString = bb.ConnectionString;  
                    }
                }
            }
        }
    }
}
