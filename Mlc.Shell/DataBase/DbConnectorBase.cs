using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Reflection;

namespace Mlc.Shell.DataBase
{
    /// <summary>
    /// Ereditare da questa classe per la gestione delle connessioni verso i DB
    /// </summary>
    public abstract class DbConnectorBase
    {
        #region Campi
        ///// <summary>Istanza per la generazione della stringa di connessione</summary>
        protected DbConnectionStringBuilder dbConnStringBuilder = null;
        /// <summary>Connessione al Db</summary>
        protected IDbConnection dbConn = null;
        /// <summary></summary>
        private IDataReader reader = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dbConnInfo">Dati della connessione</param>
        public DbConnectorBase(DbConnInfo dbConnInfo)
        {
            this.dbConnInfo = dbConnInfo;
            this.setDbConnStringBuilder();

            this.dbConnStringBuilder.ConnectionString = dbConnInfo.ConnectionString;
            if (string.IsNullOrEmpty(dbConnInfo.Password))
                this.dbConnStringBuilder.Add("password", dbConnInfo.Password);

            this.setDbConn();

            /*
            Assembly connectorAssembly = null;

            try { connectorAssembly = Assembly.Load(dbConnInfo.ConnectorAssembly); }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            if (connectorAssembly != null)
            {
                Type typeConnectionStringBuilder = connectorAssembly.GetType(dbConnInfo.ConnectionStringBuilder);

                object objectDbConnStringBuilder = Activator.CreateInstance(typeConnectionStringBuilder);
                if (objectDbConnStringBuilder is DbConnectionStringBuilder)
                    this.dbConnStringBuilder = (DbConnectionStringBuilder)objectDbConnStringBuilder;

                dbConnStringBuilder.ConnectionString = dbConnInfo.ConnectionString;
                dbConnStringBuilder.Add("password", dbConnInfo.Password);

                Type typeDbConnection = connectorAssembly.GetType(dbConnInfo.DbConnection);

                object objectDbConn = Activator.CreateInstance(typeDbConnection);
                if (objectDbConn is DbConnection)
                    this.dbConn = (DbConnection)objectDbConn;

                this.dbConn.ConnectionString = this.dbConnStringBuilder.ConnectionString;
            }
            */
        }

        /// <summary>
        /// 
        /// </summary>
        protected abstract void setDbConnStringBuilder();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract void setDbConn();


        #endregion

        #region Proprietà
        /// <summary>
        /// Restituisce la connessione
        /// </summary>
        public IDbConnection IDbConnection { get { return this.dbConn; } }

        /// <summary>Dati della connessione</summary>
        private DbConnInfo dbConnInfo = null;
        /// <summary>
        /// Restituisce i dati della connessione
        /// </summary>
        public DbConnInfo DbConnInfo { get { return this.dbConnInfo; } }

        ///// <summary>
        ///// Restituisce l'istanza per la generazione della stringa di connessione
        ///// </summary>
        //public DbConnectionStringBuilder DbConnectionStringBuilder { get { return this.dbConnStringBuilder; } }

        /// <summary>
        /// Restituisce la stringa di connessione con la password nascosta
        /// </summary>
        public abstract string ConnectionStringPwdHidden { get; }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Esegue il test della connessione
        /// </summary>
        /// <returns>True se la connessione riesce, altrimenti false</returns>
        public bool ConnectionTest()
        {
            bool retVal = false;
            try
            {
                if (this.dbConn.State == ConnectionState.Open)
                    retVal = true;
                else
                {
                    this.OpenConnection();
                    if (this.dbConn.State == ConnectionState.Open)
                    {
                        retVal = true;
                        this.CloseConnection();
                    }
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }

        /// <summary>
        /// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.IDataReader"/> ottenuta
        /// </summary>
        /// <param name="sql">Istruzione SQL da eseguire</param>
        /// <returns>Istanza <see cref="System.Data.IDataReader"/> ottenuta</returns>
        public virtual IDataReader ExecuteReader(string sql)
        {
            try
            {
                this.OnEventBeforeSqlExecute(new EventArgsSqlQuery(this, sql));
                IDbCommand iDbCommand = getDbCommand();
                iDbCommand.CommandText = sql;
                this.closeReader();
                this.reader = iDbCommand.ExecuteReader();
                return this.reader;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return null;
        }


        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.IDataReader"/> ottenuta
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <returns>Istanza <see cref="System.Data.IDataReader"/> ottenuta</returns>
        //public IDataReader ExecuteReader(Enum sqlKey)
        //{
        //    return this.ExecuteReader(SqlStore.GetInstance(this)[sqlKey]);
        //}

        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.IDataReader"/> ottenuta
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <param name="parameter">Parametri da sostituire nell'istruzione SQL</param>
        ///// <returns>Istanza <see cref="System.Data.IDataReader"/> ottenuta</returns>
        //public IDataReader ExecuteReader(Enum sqlKey, object[] parameter)
        //{
        //    return this.ExecuteReader(SqlStore.GetInstance(this)[sqlKey, parameter]);
        //}

        /// <summary>
        /// Esegue l'istruzione sql passata e restituisce il valore scalare ottenuto
        /// </summary>
        /// <param name="sql">Istruzione SQL da eseguire</param>
        /// <returns>Valore scalare ottenuto</returns>
        public object ExecuteScalar(string sql)
        {
            try
            {
                this.OnEventBeforeSqlExecute(new EventArgsSqlQuery(this, sql));
                IDbCommand iDbCommand = getDbCommand();
                iDbCommand.CommandText = sql;
                return iDbCommand.ExecuteScalar();
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return null;
        }

        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce il valore scalare ottenuto
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <returns>Valore scalare ottenuto</returns>
        //public object ExecuteScalar(Enum sqlKey)
        //{
        //    return this.ExecuteScalar(SqlStore.GetInstance(this)[sqlKey]);
        //}

        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce il valore scalare ottenuto
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <param name="parameter">Parametri da sostituire nell'istruzione SQL</param>
        ///// <returns>Valore scalare ottenuto</returns>
        //public object ExecuteScalar(Enum sqlKey, object[] parameter)
        //{
        //    return this.ExecuteScalar(SqlStore.GetInstance(this)[sqlKey, parameter]);
        //}

        /// <summary>
        /// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.DataTable"/> ottenuta
        /// </summary>
        /// <param name="sql">Istruzione SQL da eseguire</param>
        /// <returns>Istanza <see cref="System.Data.DataTable"/> ottenuta</returns>
        public DataTable GetDataTable(string sql)
        {
            IDbCommand cmd = this.getDbCommand();
            return this.GetDataTable(cmd);

        }

        /// <summary>
        /// Gets the data table.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <returns>DataTable.</returns>
        public DataTable GetDataTable(IDbCommand cmd)
        {
            DataTable retTable = new DataTable();
            cmd.Connection = this.dbConn;
            try
            {
                if (this.OpenConnection())
                {
                    IDataReader reader = cmd.ExecuteReader();
                    retTable.Load(reader);
                    reader.Close();
                    this.CloseConnection();
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retTable;
        }


        /// <summary>
        /// Gets the data set.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <param name="tables">The tables.</param>
        /// <returns>DataSet.</returns>
        public DataSet GetDataSet(IDbCommand cmd, string[] tables)
        {
            DataSet retDataSet = new DataSet();
            cmd.Connection = this.dbConn;

            try
            {
                if (this.OpenConnection())
                {
                    IDataReader reader = cmd.ExecuteReader();
                    retDataSet.Load(reader, LoadOption.OverwriteChanges, tables);
                    reader.Close();
                    this.CloseConnection();
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retDataSet;
        }



        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.DataTable"/> ottenuta
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <returns>Istanza <see cref="System.Data.DataTable"/> ottenuta</returns>
        //public DataTable GetDataTable(Enum sqlKey)
        //{
        //    return this.GetDataTable(SqlStore.GetInstance(this)[sqlKey]);
        //}

        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce l'istanza <see cref="System.Data.DataTable"/> ottenuta
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <param name="parameter">Parametri da sostituire nell'istruzione SQL</param>
        ///// <returns>Istanza <see cref="System.Data.DataTable"/> ottenuta</returns>
        //public DataTable GetDataTable(Enum sqlKey, object[] parameter)
        //{
        //    return this.GetDataTable(SqlStore.GetInstance(this)[sqlKey, parameter]);
        //}

        /// <summary>
        /// Esegue l'istruzione sql passata e restituisce il numero di righe interessate
        /// </summary>
        /// <param name="sql">Istruzione SQL da eseguire</param>
        /// <returns>numero di righe interessate</returns>
        public virtual int ExecuteNonQuery(string sql)
        {
            int retVal = 0;
            try
            {
                this.OnEventBeforeSqlExecute(new EventArgsSqlQuery(this, sql));
                IDbCommand iDbCommand = getDbCommand();
                iDbCommand.CommandText = sql;
                retVal = iDbCommand.ExecuteNonQuery();
                this.CloseConnection();
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }


        public virtual int ExecuteNonQuery(IDbCommand cmd)
        {
            int retVal = 0;
            try
            {
                if (this.OpenConnection())
                {
                    //cmd.ExecuteReader();
                    retVal = cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }



        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce il numero di righe interessate
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <returns>numero di righe interessate</returns>
        //public int ExecuteNonQuery(Enum sqlKey)
        //{
        //    return this.ExecuteNonQuery(SqlStore.GetInstance(this)[sqlKey]);
        //}

        ///// <summary>
        ///// Esegue l'istruzione sql passata e restituisce il numero di righe interessate
        ///// </summary>
        ///// <param name="sqlKey">Enumerato dell'istruzione SQL da eseguire</param>
        ///// <param name="parameter">Parametri da sostituire nell'istruzione SQL</param>
        ///// <returns>numero di righe interessate</returns>
        //public int ExecuteNonQuery(Enum sqlKey, object[] parameter)
        //{
        //    return this.ExecuteNonQuery(SqlStore.GetInstance(this)[sqlKey, parameter]);
        //}

        /// <summary>
        /// Apre la connessione se è chiusa
        /// </summary>
        public bool OpenConnection()
        {
            try
            {
                if (this.dbConn.State != ConnectionState.Open)
                    this.dbConn.Open();

                while (this.dbConn.State != ConnectionState.Open)
                    continue;

                this.OnConnectionOpened(new EventArgs());

                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Chiude la connessione se è aperta
        /// </summary>
        public bool CloseConnection()
        {
            try
            {
                if (this.dbConn.State == ConnectionState.Open)
                    this.dbConn.Close();

                while (this.dbConn.State != ConnectionState.Closed)
                    continue;

                this.OnConnectionClosed(new EventArgs());
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }

        }

        /// <summary>
        /// Restituisce l'istanza <see cref="System.Data.Common.DbDataAdapter"/> per popolare un <see cref="System.Data.DataSet"/>
        /// </summary>
        /// <returns>Istanza <see cref="System.Data.Common.DbDataAdapter"/> per popolare un <see cref="System.Data.DataSet"/></returns>
        public abstract DbDataAdapter GetDataAdpter();

        ///// <summary>
        ///// Popola l'istanza DataTable associata all'istanza passata che espone <see cref="Mx.Core.Data.ITableProxy"/>
        ///// </summary>
        ///// <param name="dataSet">Istanza del DataSet a cui dovrà appartenere il DataTable contenuto nel <see cref="Mx.Core.Data.ITableProxy"/> specificato</param>
        ///// <param name="tableProxy">Istanza che espone <see cref="Mx.Core.Data.ITableProxy"/></param>
        //public abstract void TableProxyFill(DataSet dataSet, ITableProxy tableProxy);

        ///// <summary>
        ///// Popola l'istanza DataTable associata all'istanza passata che espone <see cref="Mx.Core.Data.ITableProxy"/>
        ///// </summary>
        ///// <param name="tableProxy">Istanza che espone <see cref="Mx.Core.Data.ITableProxy"/></param>
        //public abstract void TableProxyFill(ITableProxy tableProxy);
        #endregion

        #region Handlers eventi
        /// <summary>
        /// Occorre all'esecuzione delle istruzioni per la propagazione delle modifiche alla sorgente
        /// </summary>
        protected void dataAdapter_RowUpdating(object sender, RowUpdatingEventArgs e)
        {
            try
            {
                if (e.Command != null)
                    this.OnEventBeforeSqlExecute(new EventArgsSqlQuery(this, e.Command.CommandText, e));
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion

        #region Metodi privati
        ///// <summary>
        ///// Verifica la connessione e restituisce una nuova istanza <see cref="System.Data.IDbCommand"/>
        ///// </summary>
        ///// <returns>Istanza <see cref="System.Data.IDbCommand"/></returns>
        //private IDbCommand getDbCommand()
        //{
        //    if (this.dbConn.State != ConnectionState.Open)
        //        this.OpenConnection();
        //    IDbCommand iDbCommand = this.getDbCommand();
        //    return iDbCommand;
        //}

        /// <summary>
        /// Chiude il reader se è rimasto aperto
        /// </summary>
        private void closeReader()
        {
            try
            {
                if (this.reader != null)
                {
                    this.reader.Close();
                    this.reader = null;
                }
            }
            catch { }
        }

        #endregion

        #region Metodi protetti astratti
        /// <summary>
        /// Restituisce un istanza <see cref="System.Data.IDbCommand"/> sulla connessione corrente
        /// </summary>
        /// <returns>Istanza <see cref="System.Data.IDbCommand"/> sulla connessione corrente</returns>
        protected abstract IDbCommand getDbCommand();
        #endregion

        #region Definizione eventi


        #region Evento ConnectionOpened
        /// <summary>
        /// Delegato per la gestione dell'evento di connessione al Db Aperta.
        /// </summary>///
        public event EventHandler ConnectionOpened;

        /// <summary>
        /// Metodo per il lancio dell'evento di connessione al Db Aperta.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>///
        protected virtual void OnConnectionOpened(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (ConnectionOpened != null)
                // viene lanciato l'evento
                ConnectionOpened(this, e);
        }
        #endregion

        #region Evento ConnectionClosed
        /// <summary>
        /// Delegato per la gestione dell'evento di connessione al Db Chiusa.
        /// </summary>
        public event EventHandler ConnectionClosed;

        /// <summary>
        /// Metodo per il lancio dell'evento di connessione al Db Chiusa.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnConnectionClosed(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (ConnectionClosed != null)
                // viene lanciato l'evento
                ConnectionClosed(this, e);
        }
        #endregion

        #region Evento EventBeforeSqlExecute
        /// <summary>
        /// Definizione dell'evento che occorre prima dell'esecuzione di un istruzione SQL.
        /// </summary>
        public event EventHandlerBeforeSqlExecute EventBeforeSqlExecute;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnEventBeforeSqlExecute(EventArgsSqlQuery e)
        {
            // Se ci sono ricettori in ascolto ...
            if (EventBeforeSqlExecute != null)
                // viene lanciato l'evento
                EventBeforeSqlExecute(this, e);
        }
        #endregion
        #endregion

        #region Tipi nidificati
        #endregion
    }
}
