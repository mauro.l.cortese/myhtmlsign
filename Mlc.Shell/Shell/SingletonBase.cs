﻿using System;

namespace Mlc.Shell
{
    /// <summary>
    /// A base class for the singleton design pattern.
    /// </summary>
    /// <typeparam name="T">Class type of the singleton</typeparam>
    public abstract class SingletonBase<T> where T : class
    {
        #region Members
        /// <summary>
        /// Static instance. Needs to use lambda expression
        /// to construct an instance (since constructor is private).
        /// </summary>
        protected static readonly Lazy<T> instance = new Lazy<T>(() => getInstance());
        #endregion

        #region Properties

        /// <summary>
        /// Gets the instance of this singleton.
        /// </summary>
        public static T GetInstance() {
            return instance.Value; }

        #endregion

        #region Methods

       /// <summary>
        /// Creates an instance of T via reflection since T's constructor is expected to be private.
        /// </summary>
        /// <returns></returns>
        private static T getInstance()
        {
            return Activator.CreateInstance(typeof(T), true) as T;
        }

        #endregion
    }

}
