﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-21-2015
// ***********************************************************************
// <copyright file="Enumerations.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Shell
{
    /// <summary>
    /// Enumerating all the main full names of the base types of Microsoft Framework 
    /// </summary>
    public enum SysTypeName
    {
        /// <summary>
        /// Enumerated system type: <see cref="System.String"/>
        /// </summary>
        String,
        /// <summary>
        /// Enumerated system type: <see cref="System.Char"/>
        /// </summary>
        Char,
        /// <summary>
        /// Enumerated system type: <see cref="System.Byte"/>
        /// </summary>
        Byte,
        /// <summary>
        /// Enumerated system type: <see cref="System.Int16"/>
        /// </summary>
        Int16,
        /// <summary>
        /// Enumerated system type: <see cref="System.Int32"/>
        /// </summary>
        Int32,
        /// <summary>
        /// Enumerated system type: <see cref="System.Int64"/>
        /// </summary>
        Int64,
        /// <summary>
        /// Enumerated system type: <see cref="System.UInt16"/>
        /// </summary>
        UInt16,
        /// <summary>
        /// Enumerated system type: <see cref="System.UInt32"/>
        /// </summary>
        UInt32,
        /// <summary>
        /// Enumerated system type: <see cref="System.UInt64"/>
        /// </summary>
        UInt64,
        /// <summary>
        /// Enumerated system type: <see cref="System.Single"/>
        /// </summary>
        Single,
        /// <summary>
        /// Enumerated system type: <see cref="System.Double"/>
        /// </summary>
        Double,
        /// <summary>
        /// Enumerated system type: <see cref="System.Decimal"/>
        /// </summary>
        Decimal,
        /// <summary>
        /// Enumerated system type: <see cref="System.Boolean"/>
        /// </summary>
        Boolean,
        /// <summary>
        /// Enumerated system type: <see cref="System.DateTime"/>
        /// </summary>
        DateTime,
        /// <summary>
        /// Enumerated system type: <see cref="System.Enum"/>
        /// </summary>
        Enum,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Point"/>
        /// </summary>
        Point,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Rectangle"/>
        /// </summary>
        Rectangle,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Size"/>
        /// </summary>
        Size,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.PointF"/>
        /// </summary>
        PointF,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.RectangleF"/>
        /// </summary>
        RectangleF,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.SizeF"/>
        /// </summary>
        SizeF,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Color"/>
        /// </summary>
        Color,
        /// <summary>
        /// Enumerated system type: <see cref="System.TimeSpan"/>
        /// </summary>
        TimeSpan,
        /// <summary>
        /// Enumerated system type: <see cref="System.Nullable"/>
        /// </summary>
        Nullable,
        /// <summary>
        /// Enumerated system type: <see cref="System.Exception"/>
        /// </summary>
        Exception,
        /// <summary>
        /// Enumerated system type: <see cref="System.Guid"/>
        /// </summary>
        Guid,
        /// <summary>
        /// Enumerated system type: <see cref="System.SByte"/>
        /// </summary>
        SByte,
        /// <summary>
        /// Enumerated system type: <see cref="Mx.Core.IO.FileSystemMgr"/>
        /// </summary>
        Fso,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Font"/>
        /// </summary>
        Font,
        /// <summary>
        /// Enumerated system type: <see cref="System.Drawing.Icon"/>
        /// </summary>
        Icon,
    }
}
