﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-29-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-29-2015
// ***********************************************************************
// <copyright file="TypeIsNotEnumException.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;


namespace Mlc.Shell
{
    /// <summary>
    /// The TypeIsNotEnumException class can be use when is need to check if a generic type inherits from the <see cref="System.Enum"/> type.
    /// In case that checked type isn't inherits from the <see cref="System.Enum"/> type, raise this exception.
    /// </summary>
    /// <remarks>
    /// The text of <see cref="System.Exception.Message"/> property is localized in italian and english
    /// This type is also used in <see cref="EnumOperator{T}"/>
    /// </remarks>
    public class TypeIsNotEnumException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypeIsNotEnumException"/> class.
        /// </summary>
        public TypeIsNotEnumException()
            : base(Resources.LocalResource.TypeIsNotEnumMessage)
        { }
    }
}
