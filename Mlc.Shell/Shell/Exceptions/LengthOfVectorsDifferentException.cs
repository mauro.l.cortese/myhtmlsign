﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-29-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-29-2015
// ***********************************************************************
// <copyright file="LengthOfVectorsDifferentException.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

/// <summary>
/// The Shell namespace.
/// </summary>
namespace Mlc.Shell
{
    /// <summary>
    /// Class LengthOfVectorsDifferentException for two arrays which don't have the same number of elements
    /// </summary>
    public class LengthOfArraysDifferentException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LengthOfArraysDifferentException" /> class.
        /// </summary>
        public LengthOfArraysDifferentException()
            : base(Resources.LocalResource.ArraysDiffLenght)
        { }
    }
}
