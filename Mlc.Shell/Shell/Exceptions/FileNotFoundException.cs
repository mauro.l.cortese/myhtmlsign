﻿// ***********************************************************************
// Assembly         : Mlc.Common
// Author           : Cortese Mauro Luigi
// Created          : 07-13-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-12-2015
// ***********************************************************************
// <copyright file="FileNotFoundException.cs" company="MLC">
//     Copyright ©  2014
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;


namespace Mlc.Shell
{
    /// <summary>
    /// Class FileNotFoundException for missing file.
    /// </summary>
    public class FileNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileNotFoundException" /> class.
        /// </summary>
        /// <param name="path">The full path of the missing file.</param>
        public FileNotFoundException(string path)
            : base(string.Format(Resources.LocalResource.MissingFile, path))
        {
            this.path = path;
        }

        /// <summary>
        /// The path of the missing file.
        /// </summary>
        private string path;
        /// <summary>
        /// Gets the full path of the missing file.
        /// </summary>
        /// <value>The full path of the missing file.</value>
        public string Path { get { return this.path; } }
    }
}
