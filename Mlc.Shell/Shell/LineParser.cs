using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace Mlc.Shell
{
    /// <summary>
    /// Provides a wrapper for the interpretation of the command line used to launch the application
    /// </summary>
    public abstract class LineParser
    {
        #region Public
        /// <summary>The char which marks the begin of each parameter.</summary>
        public static char StartParameterChar = '-';


        #region Properties
        /// <summary>Parameters collection</summary>
        protected CmdParams parameters = new CmdParams();
        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public CmdParams Parameters { get { return parameters; } set { parameters = value; } }

        /// <summary>The command line arguments</summary>
        protected string[] args = null;
        /// <summary>
        /// Gets the the command line arguments.
        /// </summary>
        /// <value>The command line arguments.</value>
        public string[] Args { get { return args; } }

        /// <summary>String to split the command line arguments</summary>
        private string splitArgs = " ";
        /// <summary>
        /// Gets or sets the string which will be used to split the command line arguments.
        /// </summary>
        public string SplitArgs { get { return this.splitArgs; } set { this.splitArgs = value; } }

        /// <summary>The string which divides the key from the value.</summary>
        private string splitKey = "=";
        /// <summary>
        /// Gets or sets the string which divides the key from the value.
        /// </summary>
        public string SplitKey { get { return this.splitKey; } set { this.splitKey = value; } }

        /// <summary>
        /// Gets the fullpath of first command line argumet
        /// </summary>
        public string FirstArgument { get { return this.args[0]; } }
        #endregion

        #region Method
        /// <summary>
        /// Gets the string which represents the current instance.
        /// </summary>
        /// <returns>String which represents the current instance</returns>
        public override string ToString()
        {
            // ciclo per tutti gli argomenti
            string[] appoggio = new string[this.args.Length];

            for (int i = 0; i < this.args.Length; i++)
            {
                string arg = this.args[i];
                if (arg.Contains(this.splitArgs))
                    arg = arg.AddApex();
                appoggio[i] = arg;
            }
            return string.Join(this.splitArgs, appoggio);
        }

        /// <summary>
        /// Parses the command line.
        /// </summary>
        /// <param name="cmdString">Command line to parsing</param>
        public virtual LineParser ParseString(string cmdString)
        {
            this.args = cmdString.Split(new string[] { this.splitArgs }, StringSplitOptions.RemoveEmptyEntries);
            this.parseParameters();
            return this;
        }
        #endregion
        #endregion

        #region Protected
        #region Method
        /// <summary>
        /// Interpreta gli argomenti della linea di comando e popola la collezione dei parameters.
        /// </summary>
        protected void parseParameters()
        {
            this.parameters = new CmdParams();
            string startParameterDouble = new string(LineParser.StartParameterChar, 2);
            string startParameterSingle = LineParser.StartParameterChar.ToString();

            // ciclo per tutti gli argomenti e popolo la collezione di parameters
            for (int i = 1; i < this.args.Length; i++)
            {
                string arg;
                if (i < this.args.Length - 2 && this.args[i + 1] == this.splitKey)
                {
                    arg = this.args[i] + this.args[i + 1] + this.args[i + 2];
                    i += 2;
                }
                else
                    arg = this.args[i];

                if (arg.Contains(this.splitKey))
                {
                    string[] splitArgs = arg.Split(new string[] { this.splitKey }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitArgs != null && splitArgs.Length == 2 && !this.parameters.ContainsKey(splitArgs[0]))
                    {
                        string key = splitArgs[0];
                        if (key.StartsWith(startParameterDouble))
                            key = key.Substring(2);
                        this.parameters.Add(key, splitArgs[1]);
                    }
                }
                else if (arg.StartsWith(startParameterDouble))
                {
                    string key = arg.Substring(2);
                    if (!this.parameters.ContainsKey(key))
                        this.parameters.Add(key, string.Empty);
                }
                else if (arg.StartsWith(startParameterSingle))
                {
                    string key = arg.Substring(1);
                    foreach (char c in key)
                    {
                        string par = c.ToString();
                        if (!this.parameters.ContainsKey(par))
                            this.parameters.Add(par, string.Empty);
                    }
                }
                else if (arg.EndsWith(startParameterSingle))
                {
                    string key = arg.Substring(0, arg.Length - 1);
                    if (!this.parameters.ContainsKey(key))
                        this.parameters.Add(key, string.Empty);
                }
            }
        }
        #endregion
        #endregion
    }

    /// <summary>
    /// Command line parameters collection
    /// </summary>
    public sealed class CmdParams : Dictionary<string, string>
    {
        /// <summary>
        /// Checks if the enumeration key exists in this collection
        /// </summary>
        /// <param name="key">Enumeration key to check</param>
        /// <returns><c>true</c> if the key exists, otherwise <c>false</c></returns>
        public bool ContainsKey(Enum key) { return this.ContainsKey(key.ToString()); }

        /// <summary>
        /// Index for direct access at one parameter with enumeration key   
        /// </summary>
        /// <param name="key">Enumeration key to find the parameter</param>
        /// <returns>The parameter value</returns>
        public string this[Enum key] { get { return this[key.ToString()]; } }

        /// <summary>
        /// Gets the string which represents the current instance.
        /// </summary>
        /// <returns>String which represents the current instance</returns>
        public override string ToString()
        {
            string retVal = string.Empty;
            string[] args = CmdLineParser.GetInstance().Args;
            if (args.Length > 1)
            {
                string[] newArgs = new string[args.Length - 1];
                for (int i = 0; i < newArgs.Length; i++)
                    newArgs[i] = args[i + 1];
                retVal = string.Join(" ", newArgs);
            }
            return retVal;
        }
    }
}
