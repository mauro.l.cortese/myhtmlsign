using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace Mlc.Shell
{
    /// <summary>
    /// Fornisce un wrapper per l'interpretazione della riga di comando utlizzata per il lancio dell'applicazione
    /// </summary>
    /// <remarks>Questa classe è implementata seguendo il pattern singleton quindi viene istanziata mediante il metodo 
    /// statico <see cref="Mx.Core.CmdLineParser.GetInstance()"/> che restituisce sempre la medesima istanza.</remarks>
    public class CmdLineParser : LineParser
    {
        #region Campi
        /// <summary>Istanza della classe</summary>
        private static volatile CmdLineParser instance = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Costruttore privato che non permette l'istanziamento diretto di questa classe.
        /// </summary>
        private CmdLineParser()
        {
            base.args = System.Environment.GetCommandLineArgs();
            base.parseParameters();
        }
        #endregion

        #region Proprietà
        /// <summary>
        /// Restituisce il fullpath del file eseguibile a cui appartiene la linea di comando
        /// </summary>
        public string ExecuteblePath { get { return base.FirstArgument; } }

        /// <summary>
        /// Restituisce il fullpath del file eseguibile a cui appartiene la linea di comando
        /// </summary>
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce sempre la medesima istanza della classe.
        /// </summary>
        /// <returns>Istanza della classe</returns>
        public static CmdLineParser GetInstance()
        {
            // se l'istanza non è ancora stata allocata
            if (instance == null)
            {
                // ottengo una nuova istanza
                if (instance == null)
                    instance = new CmdLineParser();
            }
            // restituisco sempre la stessa istanza 
            return instance;
        }

        /// <summary>
        /// Utilizza la stringa passata per inizializzare l'istanza
        /// </summary>
        /// <param name="cmdString">Stringa da interpretare</param>
        public override LineParser ParseString(string cmdString)
        {
            throw new Exception("L'utilizzo di questo metodo è proibito con questa specializzazione!");
        }
        #endregion
    }
}
