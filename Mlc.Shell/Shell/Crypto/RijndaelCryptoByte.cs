using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

using Mlc.Shell;

namespace Mlc.Shell.Crypto
{
    /// <summary>
    /// Cifra o decifra un vettore di byte con algoritmo Rijndael
    /// </summary>
    /// <example>
    /// La cifratura di un array di byte è molto semplice.
    /// <code>
    /// // viene istanziata la classe passando al cotruttore il vettore di byte da cifrare
    /// CryptoByte cb = new CryptoByte(new byte[] { 126, 111, 12, 24, 1 });
    /// </code>
    /// 
    /// Oppure:
    /// 
    /// <code>
    /// // viene istanziata la classe
    /// CryptoByte cb = new CryptoByte();
    /// // il vettore viene assegnato alla proprietà Data dell'istanza 
    /// cb.Data = new byte[] { 126, 111, 12, 24, 1 };
    /// </code>  
    /// 
    /// Quindi viene richiamato il metodo <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Encrypt()"/> per cifrare i byte del vettore
    /// 
    /// <code>
    /// // cifratura i dati del vettore
    /// cb.Encrypt();
    /// </code>
    /// 
    /// La proprietà <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Data"/> restituisce i dati cifrati, cioè un vettore 
    /// di byte ottenuto dalla cifratura del vettore passato al costruttore. 
    /// <para>
    /// Il metodo <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Decrypt()"/> esegue la decifratura dei bytes del vettore assegnato alla proprietà  <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Data"/>.
    /// </para>
    ///
    /// <code>
    /// // Decifratura dei dati
    /// cb.Decrypt();
    /// </code>
    /// 
    /// Negli esempi proposti i dati vengono cifrati utilizzando la password della costante <see cref="Mx.Core.Constants.Strings.DefaultPwd"/>.
    /// Per utilizzare una password diversa assegnarla alla proprietà <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Password"/> prima di richiamare i metodi
    /// <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Encrypt()"/> o <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Decrypt()"/>
    /// 
    /// <code>
    /// CryptoByte cb = new CryptoByte(new byte[] { 126, 111, 12, 24, 1 });
    /// // Impostazione di una password diversa da quella di default
    /// cb.Password = "12dtrhs@435";
    /// cb.Encrypt();
    /// </code>
    /// 
    /// L'esempio seguente mostra come cifrare un file
    /// 
    /// <code>
    /// CryptoByte cb = new CryptoByte();
    /// cb.Data = File.ReadAllBytes(@"C:\Prova.xml");
    /// cb.Encrypt();
    /// File.WriteAllBytes(@"C:\Prova.xml.cry", cb.Data);
    /// </code>
    /// 
    /// ed infine come decifrarlo
    /// 
    /// <code>
    /// CryptoByte cb = new CryptoByte();
    /// cb.Data = File.ReadAllBytes(@"C:\Prova.xml.cry");
    /// cb.Decrypt();
    /// File.WriteAllBytes(@"C:\Prova.xml.new", cb.Data);
    /// </code>
    /// 
    /// </example>
    /// <remarks>
    /// <para>
    /// Per cifrare e decifrare stringhe di testo si possono utilizzare i metodi estesi di <see cref="Mx.Core.Extensions.CriptoStringExtension"/>.
    /// </para>
    /// <para>
    /// Per cifrare e decifrare files o intere cartelle è più semplice l'utilizzo dei metodi <see cref="Mx.Core.IO.FileSystemMgr.Encrypt()"/> o <see cref="Mx.Core.IO.FileSystemMgr.Encrypt(string)"/> 
    /// e <see cref="Mx.Core.IO.FileSystemMgr.Decrypt()"/> o <see cref="Mx.Core.IO.FileSystemMgr.Decrypt(string)"/> della classe <see cref="Mx.Core.IO.FileSystemMgr"/>
    /// </para>
    /// </remarks>
    public class RijndaelCryptoByte : ICryptoByte
    {
        #region Campi
        /// <summary>Vettore della chiave di cifratura</summary>
        private byte[] Key;
        /// <summary>Vettore di inizializzazione</summary>
        private byte[] IV;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        public RijndaelCryptoByte()
        {
            this.Password = Constants.DefaultPwd;
        }

        /// <summary>
        /// Inizializza una nuova istanza e vi assegna il vettore di bytes passato.
        /// </summary>
        /// <param name="bytesData">Vettore di bytes da elaborare</param>
        public RijndaelCryptoByte(byte[] bytesData)
            : this()
        {
            this.bytesData = bytesData;
        }
        #endregion

        #region Proprietà
        /// <summary>Password di cifratura</summary>
        private string password = null;
        /// <summary>
        /// Restituisce o imposta la password di cifratura, per default la password è quella della costante <see cref="Mx.Core.Constants.Strings.DefaultPwd"/>
        /// </summary>    
        public string Password
        {
            get { return password; }
            set
            {
                this.password = value;
                this.Key = getBytesKey(this.password, 32, false);
                this.IV = getBytesKey(this.password, 16, true);
            }
        }

        /// <summary>Vettore di bytes cifrato o decifrato</summary>
        private byte[] bytesData;
        /// <summary>    
        /// Restituisce o imposta il vettore di bytes cifrato o decifrato
        /// </summary>    
        public byte[] Data { get { return bytesData; } set { bytesData = value; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Esegue la cifratura dei bytes contenuti in <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Data"/>
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Encrypt()
        {
            try
            {
                RijndaelManaged rijndaelMan = new RijndaelManaged();
                rijndaelMan.Key = this.Key;
                rijndaelMan.IV = this.IV;
                dataProcess(rijndaelMan.CreateEncryptor(Key, IV));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Esegue la decifratura dei bytes contenuti in <see cref="Mx.Core.Crypto.RijndaelCryptoByte.Data"/>
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Decrypt()
        {
            try
            {
                RijndaelManaged rijndaelMan = new RijndaelManaged();
                rijndaelMan.Key = this.Key;
                rijndaelMan.IV = this.IV;
                dataProcess(rijndaelMan.CreateDecryptor(Key, IV));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }
        #endregion

        #region Metodi privati
        /// <summary>
        /// Processa i dati con l'istanza di trasformazione
        /// </summary>
        /// <param name="iCryptoTransform">Istanza di trasfomazione dei dati</param>
        private void dataProcess(ICryptoTransform iCryptoTransform)
        {
            byte[] bytesDecrypt;
            MemoryStream ms = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(ms, iCryptoTransform, CryptoStreamMode.Write); ;
            cryptoStream.Write(this.bytesData, 0, this.bytesData.Length);
            cryptoStream.FlushFinalBlock();
            bytesDecrypt = ms.ToArray();
            cryptoStream.Close();
            ms.Close();
            this.bytesData = bytesDecrypt;
        }

        /// <summary>
        /// Restituisce un vettore di bytes della lunghezza richiesta ottenuto dalla stringa della password
        /// </summary>
        /// <param name="password">Stringa della password</param>
        /// <param name="lenght">Lunghezza del vettore richiesta</param>
        /// <param name="reverse">Determina se invertire o meno il vettore di bytes</param>
        /// <returns>Vettore di byte ottenuto</returns>
        private byte[] getBytesKey(string password, int lenght, bool reverse)
        {
            if (password.Length > lenght)
                password = password.Substring(0, lenght);

            byte[] retVal = new byte[lenght];
            byte[] pwd = ASCIIEncoding.ASCII.GetBytes(password);

            if (reverse)
            {
                byte[] buffer = new byte[pwd.Length];
                int n = 0;
                for (int i = pwd.Length - 1; i >= 0; i--)
                {
                    buffer[n] = pwd[i];
                    n++;
                }
                pwd = buffer;
            }

            pwd.CopyTo(retVal, 0);

            return retVal;
        }
        #endregion
    }
}
