﻿using System;

namespace Mlc.Shell.Crypto
{
    /// <summary>
    /// Espone i membri per la cifratura o decifratura di un vettore di byte
    /// </summary>
    public interface ICryptoByte
    {
        /// <summary>    
        /// Restituisce o imposta il vettore di bytes cifrato o decifrato
        /// </summary>   
        byte[] Data { get; set; }

        /// <summary>
        /// Esegue la decifratura dei bytes contenuti in <see cref="Mx.Core.Crypto.ICryptoByte.Data"/>
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        bool Decrypt();

        /// <summary>
        /// Esegue la cifratura dei bytes contenuti in <see cref="Mx.Core.Crypto.ICryptoByte.Data"/>
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>        
        bool Encrypt();

        /// <summary>
        /// Restituisce o imposta la password di cifratura
        /// </summary>   
        string Password { get; set; }
    }
}
