using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Reflection;
using System.ServiceProcess;

namespace Mlc.Shell
{
    /// <summary>
    /// Restituisce informazioni di sistema
    /// </summary>
    public class SysInfo
    {
        #region Costanti
        /// <summary>Nome del processo dell'ambiente di sviluppo</summary>
        private const string DevEnv = "devenv";
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        public SysInfo()
        {
        }
        #endregion

        #region Proprietà
        /// <summary>
        /// Restituisce true se il processo è in esecuzione a 64 bit, altrimenti false
        /// </summary>
        public bool Is64BitProcess { get { return Environment.Is64BitProcess; } }

        /// <summary>
        /// Restituisce true se il sistema operativo è in esecuzione a 64 bit, altrimenti false
        /// </summary>
        public bool Is64BitOperatingSystem { get { return Environment.Is64BitOperatingSystem; } }

        /// <summary>
        /// Restituisce il fullpath della cartella dove si trova l'eseguibile dell'applicazione corrente
        /// </summary>
        public string ExecutableFolder
        {
            get { return Path.GetDirectoryName(this.ExecutableFile); }
        }

        /// <summary>
        /// Retituisce il path della cartella temporanea per l'applicazione corrente
        /// </summary>
        public string ExecutableTempFolder { get { return string.Concat(this.ExecutableFolder, "\\temp"); } }

        /// <summary>
        /// Restituisce il fullpath dell'eseguibile dell'applicazione corrente
        /// </summary>
        public string ExecutableFile
        {
            get
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                if (assembly == null)
                    assembly = Assembly.GetCallingAssembly();
                if (assembly == null)
                    assembly = Assembly.GetExecutingAssembly();
                return assembly.Location;
            }
        }

        /// <summary>
        /// Restituisce il nome NetBIOS del computer locale
        /// </summary>
        public string ComputerName { get { return Environment.MachineName; } }

        /// <summary>
        /// Restituisce il nome del dominio a cui appartiene il computer o il nome del workgroup
        /// </summary>
        public string DomainName
        {
            get
            {
                string domainName = Environment.UserDomainName;
                SelectQuery query = new SelectQuery("Select * from Win32_ComputerSystem");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
                foreach (ManagementObject mo in searcher.Get())
                {
                    if (mo["workgroup"] != null)
                        domainName = mo["workgroup"].ToString();
                    else
                        if (mo["domain"] != null)
                            domainName = mo["domain"].ToString();
                }
                return domainName;
            }
        }

        /// <summary>
        /// Restituisce il nome dell'utente loggato al computer locale
        /// </summary>
        public string UserName { get { return Environment.UserName; } }

        /// <summary>
        /// Determina se l'esecuzione sta avvenendo nell'ambiente dell'editor grafico dell'IDE di VisualStudio
        /// </summary>
        public bool IsExecuteInVsIdeDesignMode { get { return (Process.GetCurrentProcess().ProcessName.ToLower() == SysInfo.DevEnv); } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce il path del primo file che viene trovato nella cartella dell'eseguibile o in una delle sue sottocartelle dato il suo nome 
        /// </summary>
        /// <param name="fileName">Nome del file da trovare</param>
        /// <returns>Fullpath del file ottenuto</returns>
        public string GetFileOnExecutableFolder(string fileName)
        {
            string[] files = Directory.GetFiles(this.ExecutableFolder, fileName, SearchOption.AllDirectories);
            if (files.Length > 0)
                return files[0];
            else
                return null;
        }

        /// <summary>
        /// Restituisce il full path di un file temporaneo con nome specificato
        /// </summary>
        /// <param name="fileName">Nome del file temporaneo</param>
        /// <returns>Full path del file temporaneo ottenuto</returns>
        public string GetFileTmp(string fileName)
        {
            string retVal = string.Concat(Path.GetTempPath(), fileName);
            if (!File.Exists(retVal))
                File.Create(retVal).Close();
            return retVal;
        }
        #endregion

        #region Tipi nidificati
        /// <summary>
        /// Fornisce metodi e proprietà per operare con i servizi di un computer nella rete
        /// </summary>
        public class ServiceInfo
        {
            #region Campi
            /// <summary>Rappresenta un servizio di Windows</summary>
            private ServiceController serviceController = null;
            /// <summary>Nome del servizio</summary>
            private string serviceName = null;
            #endregion

            #region Costruttori
            /// <summary>
            /// Inizializza una nuova istanza per un servizio sull'localhost
            /// </summary>
            /// <param name="serviceName">Nome del servizio</param>
            public ServiceInfo(string serviceName)
                : this(serviceName, "localhost")
            { }

            /// <summary>
            /// Inizializza una nuova istanza per un servizio sul computer specificato
            /// </summary>
            /// <param name="serviceName">Nome del servizio</param>
            /// <param name="machineName">Nome del computer in rete</param>
            public ServiceInfo(string serviceName, string machineName)
            {
                this.serviceName = serviceName;
                this.serviceController = new ServiceController(serviceName, machineName);
                try
                {
                    string name = this.serviceController.ServiceName;
                    this.isInstalled = true;
                }
                catch { this.isInstalled = false; }
            }
            #endregion

            #region Proprietà
            /// <summary>
            /// Restituisce il nome del sevizio
            /// </summary>
            public string Name { get { return this.serviceName; } }

            /// <summary>Determina se il servizio è installato sul computer</summary>
            public bool isInstalled = false;
            /// <summary>
            /// Restituisce true se il servizio è installato sul computer, altrimenti false
            /// </summary>
            public bool IsInstalled { get { return this.isInstalled; } }

            /// <summary>
            /// Restituisce true se il servizio è in esecuzione, altrimenti false
            /// </summary>
            public bool IsRunning
            {
                get
                {
                    return this.IsInstalled ? this.serviceController.Status == ServiceControllerStatus.Running : false;
                }
            }
            #endregion

            #region Metodi pubblici
            /// <summary>
            /// Ferma il servizio se non è in esecuzione e attende che lo stato del servizio sia Stopped
            /// </summary>
            public void Stop()
            {
                if (this.IsInstalled && this.IsRunning)
                {
                    this.serviceController.Stop();
                    this.serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
                }
            }

            /// <summary>
            /// Avvia il servizio se non è in esecuzione e attende che lo stato del servizio sia Running
            /// </summary>
            public void Start()
            {
                if (this.IsInstalled && !this.IsRunning)
                {
                    this.serviceController.Start();
                    this.serviceController.WaitForStatus(ServiceControllerStatus.Running);
                }
            }
            #endregion
        }
        #endregion

    }
}
