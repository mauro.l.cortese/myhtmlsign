// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-29-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-29-2015
// ***********************************************************************
// <copyright file="StringExtension.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Mlc.Shell
{
    /// <summary>
    /// Class StringExtension. Provides extension methods for fast operations on <see cref="String" /> type.
    /// </summary>
    public static class StringExtension
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// The cultureInfo
        /// </summary>
        private static CultureInfo cultureInfo;
        #endregion

        #region Constructors
        #endregion

        #region Public
        #region Properties
        #endregion

        #region Method Static
        /// <summary>
        /// Adds the apex.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String.</returns>
        public static string AddApex(this string text)
        {
            return AddApex(text, Chars.DoubleApex);
        }

        /// <summary>
        /// Adds the apex.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="apex">The apex.</param>
        /// <returns>System.String.</returns>
        public static string AddApex(this string text, char apex)
        {
            return AddApex(text, apex, apex);
        }

        /// <summary>
        /// Adds the apex.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="apexOne">The apex one.</param>
        /// <param name="apexTwo">The apex two.</param>
        /// <returns>System.String.</returns>
        public static string AddApex(this string text, char apexOne, char apexTwo)
        {
            return apexOne.ToString() + text + apexTwo.ToString();
        }

        /// <summary>
        /// Returns the string representation of a font
        /// </summary>
        /// <param name="font">Font to be converted to string</param>
        /// <returns>String obtained by the font</returns>
        public static string FontToString(this Font font)
        {
            const string fontFormat = "[Font: Name={0}, Size={1}, Style={2}]";
            return string.Format(fontFormat, font.FontFamily.Name, font.Size.ToString().Replace(',', '.'), font.Style.ToString().Replace(',', ';'));
        }


        /// <summary>
        /// Gets the first maius.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String.</returns>
        public static string GetFirstMaius(this string text)
        {
            return GetFirstMaius(text, false);
        }

        /// <summary>
        /// Gets the first maius.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="onlyFirst">if set to <c>true</c> only first only first char will be changed.</param>
        /// <returns>System.String.</returns>
        public static string GetFirstMaius(this string text, bool onlyFirst)
        {
            try
            {
                if (onlyFirst)
                    return text[0].ToString().ToUpper() + text.Substring(1);
                else
                    return text[0].ToString().ToUpper() + text.Substring(1).ToLower();
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return text;
            }
        }

        /// <summary>
        /// Returns a string that contains only the digits of the input text string.
        /// </summary>
        /// <param name="testo">Input text string</param>
        /// <returns>String that contains only the digits of the input text string.</returns>
        public static string GetNumber(this string testo)
        {
            return GetNumber(testo, false, false);
        }

        /// <summary>
        /// Returns a string that contains only the digits of the input text string.
        /// </summary>
        /// <param name="testo">Input text string</param>
        /// <param name="removeZero">If true that are removed leading and trailing zeros that are not significant by the number obtained</param>
        /// <returns>String that contains only the digits of the input text string.</returns>
        public static string GetNumber(this string testo, bool removeZero)
        {
            return GetNumber(testo, removeZero, removeZero);
        }

        /// <summary>
        /// Returns a text string that contains only the digits of the input text string.
        /// </summary>
        /// <param name="testo">Input text string</param>
        /// <param name="removeStartZero">If true removes the leading zeros that are not significant by the number obtained</param>
        /// <param name="removeEndZero">If true, trailing zeros that are removed from the insignificant number obtained</param>
        /// <returns>String that contains only the digits of the input text string.</returns>
        public static string GetNumber(this string testo, bool removeStartZero, bool removeEndZero)
        {
            if (testo == string.Empty) return testo;

            StringBuilder retVal = new StringBuilder();
            bool valid = false;
            bool dec = false;

            // for all chars in the text string ..
            for (int i = 0; i < testo.Length; i++)
            {
                char c = testo[i];
                if (char.IsDigit(c))
                {
                    // it removes leading zeros that are not significant
                    if (removeStartZero)
                    {
                        if (c != Chars.Zero) valid = true;
                        if (valid) retVal.Append(c);
                    }
                    else
                        retVal.Append(c);
                }

                // if the char is a dot or a comma, and it is not yet entered
                if ((c == Chars.Comma || c == Chars.Dot) && dec == false)
                {
                    dec = true;
                    if (retVal.Length == 0)
                        retVal.Append(string.Concat(Chars.Zero.ToString(), Chars.Comma.ToString()));
                    else
                        retVal.Append(Chars.Comma);
                }
            }

            // it removes trailing zeros that are not significant
            if (removeEndZero && dec)
                for (int i = retVal.Length - 1; i >= 0; i--)
                    if (retVal[i] == Chars.Zero)
                        retVal.Remove(i, 1);
                    else
                        break;

            string valore = retVal.ToString();
            if (valore.EndsWith(Chars.Comma.ToString())) valore = valore.Substring(0, valore.Length - 1);
            return valore;
        }
        
        /// <summary>
        /// Gets an array containing the substrings that are found in a text string.
        /// In the original text string, the substrings are separated by a tag text (start tag) followed by another (end tag).
        /// </summary>
        /// <param name="testo">Text string to be analyzed (eg. "Text of [test] for extractions [tag]")</param>
        /// <param name="startTag">Start tag (eg "[")</param>
        /// <param name="endTag">End tag (eg. "]")</param>
        /// <returns>Array that contains substrings that are found</returns>
        /// <remarks>This method uses Regex</remarks>
        public static string[] GetTagsString(this string testo, string startTag, string endTag)
        {
            return testo.GetTagsString(startTag, endTag, true);
        }

        /// <summary>
        /// Returns an array containing the substrings that are found in a text string.
        /// In the original text string, the substrings are separated by a tag text (start tag) followed by another (end tag).
        /// </summary>
        /// <param name="testo">Text string to be analyzed (eg. "Text of [test] for extractions [tag]")</param>
        /// <param name="startTag">Start tag (eg "[")</param>
        /// <param name="endTag">End tag (eg. "]")</param>
        /// <param name="removeTag">If true it removes the tags by result, otherwise it does nothing</param>
        /// <returns>Array that contains substrings that are found</returns>
        /// <remarks>This method uses Regex</remarks>
        public static string[] GetTagsString(this string testo, string startTag, string endTag, bool removeTag)
        {
            // converto
            startTag = startTag.Replace(Chars.SquareBracketOpen, Chars.Minor); startTag = startTag.Replace(Chars.SquareBracketClose, Chars.Major);
            endTag = endTag.Replace(Chars.SquareBracketOpen, Chars.Minor); endTag = endTag.Replace(Chars.SquareBracketClose, Chars.Major);
            testo = testo.Replace(Chars.SquareBracketOpen, Chars.Minor); testo = testo.Replace(Chars.SquareBracketClose, Chars.Major);

            // Regex rx = new Regex(startTag + "[^" + endTag + "]*" + endTag);
            /* This pattern analyzes the text as a continuous string and works best with complex tags
             * example:
             * <TagSTART>[^<TagEND>].*?<TagEND>
             * That means:
             * any text after <TagSTART> (.*?) which is different to <TagEND> ([^<TagEND>]) until text that is found is <TagEND>
            */
            //Regex rx = new Regex(startTag + "[^" + endTag + "].*?" + endTag, RegexOptions.Singleline);

            Regex rx = new Regex(startTag + "(.*?)" + endTag, RegexOptions.Multiline);
            MatchCollection m = rx.Matches(testo);

            // values found
            string[] resultArray = new string[m.Count];

            // fill the result array
            for (int i = 0; i < m.Count; i++)
                resultArray[i] = m[i].Value;

            if (removeTag)
                // it edits values in the result array (deletes the tags)
                for (int i = 0; i < resultArray.GetLength(0); i++)
                    resultArray[i] = resultArray[i].Substring(startTag.Length, resultArray[i].Length - (endTag.Length + startTag.Length));

            // returns the result array
            return resultArray;
        }

        /// <summary>
        /// Returns a text string without the specified char
        /// </summary>
        /// <param name="testo">Text string from which to remove the specified char</param>
        /// <param name="removeChar">Char to remove</param>
        /// <returns>Text string with the removed chars</returns>
        public static string GetTrimForChar(this string testo, char removeChar)
        {
            string newstring = string.Empty;
            char[] caratteri = testo.ToCharArray();
            foreach (char c in caratteri)
                if (c != removeChar)
                    newstring += c.ToString();
            return newstring;
        }

        /// <summary>
        /// Returns the upper camel case.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String.</returns>
        /// <example>
        /// The following example makes it clear what you get from the function
        /// <code>
        /// // Next istruction returns: TextToTransform
        /// string val = "text   to   TRANSFORM".GetUpperCamelCase();
        /// </code></example>
        public static string GetUpperCamelCase(this string text)
        {
            return GetUpperCamelCase(text, false);
        }

        /// <summary>
        /// Returns the upper camel case.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="withSpace">if set to <c>true</c> returns text with space.</param>
        /// <returns>System.String.</returns>
        /// <example>
        /// The following example makes it clear what you get from the function
        /// <code>
        /// // Next istruction returns: Text To Transform
        /// string val = "text   to   TRANSFORM".GetUpperCamelCase(true);
        /// </code></example>
        public static string GetUpperCamelCase(this string text, bool withSpace)
        {
            try
            {
                string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (!(words.Length > 0)) return text;
                StringBuilder retVal = new StringBuilder();
                foreach (string word in words)
                {
                    retVal.Append(word.GetFirstMaius());
                    if (withSpace) retVal.Append(' ');
                }
                return retVal.ToString().Trim();
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return text;
            }
        }



        /// <summary>
        /// Returns a text string formed by the number of required words, for example GetWords("Text string for example",3) returns "Text string for").
        /// </summary>
        /// <param name="testo">Input text string</param>
        /// <param name="numWords">Number of required words</param>
        /// <returns>Text string formed by the number of required words</returns>
        public static string GetWords(this string testo, int numWords)
        {
            return GetWords(testo, numWords, ' ');
     
        }


        /// <summary>
        /// Gets the words.
        /// </summary>
        /// <param name="testo">The testo.</param>
        /// <param name="numWords">The number words.</param>
        /// <param name="splitChar">The split character.</param>
        /// <returns>System.String.</returns>
        public static string GetWords(this string testo, int numWords, char splitChar)
        {
            try
            {
                string[] words = testo.Split(new char[] { splitChar }, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length < numWords) return testo;

                StringBuilder retVal = new StringBuilder();
                for (int i = 0; i < numWords; i++)
                    retVal.Append(words[i] + ' ');

                return retVal.ToString().Trim();
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return testo;
            }
        }

        /// <summary>
        /// Converts the text string in hexadecimal format to a normal test string
        /// </summary>
        /// <param name="value">Text string to convert</param>
        /// <returns>Normal test string</returns>

        public static string HexToString(this string value)
        {
            UnicodeEncoding unicode = new UnicodeEncoding();
            string[] arr = value.Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries);
            byte[] pwdByte = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                pwdByte[i] = (byte)Convert.ToInt16(arr[i], 16);
            return unicode.GetString(pwdByte);
        }

        /// <summary>
        /// Replaces in text string any chars that are contained in "search" parameter with the
        /// chars at the same position that are contained in "replace" parameter
        /// </summary>
        /// <param name="testo">Text string to process</param>
        /// <param name="search">Text string that contains the chars to be searched</param>
        /// <param name="replace">Text string that contains the chars to be replaced</param>
        /// <param name="wrongChars">Text string that contains the wrong chars to be deleted</param>
        /// <param name="nullValue">Array of values that must cancel the string</param>
        /// <returns>A new test string with the replaced chars</returns>
        public static string Normalize(this string testo, string search, string replace, string wrongChars, string[] nullValue)
        {
            if (testo == null) testo = string.Empty;
            if (search.Length != replace.Length) throw new Exception("");//Message.DifferentStringLength);

            string retTesto = string.Empty;
            // se ci sono valori che annullano la stringa ...
            if (nullValue != null)
                foreach (string str in nullValue)
                    if (testo == str)
                        return retTesto;

            char[] searchArr = search.ToCharArray();
            char[] replaceArr = replace.ToCharArray();
            char[] testoArr = testo.ToCharArray();
            List<char> wrongCharsArr = wrongChars.ToCharArray().ToList();

            // sostituzione dei caratteri alternativi
            for (int a = 0; a < searchArr.GetLength(0); a++)
                for (int b = 0; b < testoArr.GetLength(0); b++)
                    if (testoArr[b] == searchArr[a])
                        testoArr[b] = replaceArr[a];

            // ricompongo la stringa senza i caratteri illeciti
            for (int a = 0; a < testoArr.GetLength(0); a++)
                if (!wrongCharsArr.Contains(testoArr[a]))
                    retTesto += testoArr[a].ToString();

            return retTesto;
        }

        /// <summary>
        /// Returns a color from its string representation
        /// </summary>
        /// <param name="color">Color in text string format</param>
        /// <returns>Color obtained from the text string</returns>
        public static Color ParseColor(this string color)
        {
            Color retVal = default(Color);
            try
            {
                if (color.StartsWith("Color ["))
                {
                    // "Color [A=255, R=255, G=124, B=233]"
                    if (color.Contains("A=") || color.Contains("B="))
                    {
                        string[] nc = color.Normalize("", "", "ARGB =", null).GetTrimForChar(' ').GetTrimForChar('=').GetTagsString("[", "]")[0].Split(',');
                        if (nc.Length == 4)
                            retVal = Color.FromArgb(int.Parse(nc[0]), int.Parse(nc[1]), int.Parse(nc[2]), int.Parse(nc[3]));
                        else if (nc.Length == 3)
                            retVal = Color.FromArgb(int.Parse(nc[0]), int.Parse(nc[1]), int.Parse(nc[2]));
                    }
                    // "Color [AliceBlue]" 
                    else
                        retVal = Color.FromName(color.GetTagsString("[", "]")[0]);
                }
                else
                {
                    string[] nc = color.GetTrimForChar(' ').Split(',');
                    if (nc.Length == 4)
                        retVal = Color.FromArgb(int.Parse(nc[0]), int.Parse(nc[1]), int.Parse(nc[2]), int.Parse(nc[3]));
                    else if (nc.Length == 3)
                        retVal = Color.FromArgb(int.Parse(nc[0]), int.Parse(nc[1]), int.Parse(nc[2]));
                }
            }
            catch { }
            return retVal;
        }

        /// <summary>
        /// Replaces in "text" all the words which are contained in "wordsSearch" with those of "wordsReplace"..
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="wordsSearch">The words search.</param>
        /// <param name="wordsReplace">The words replace.</param>
        /// <param name="nullValues">The null values, They are the values that cause depletion of the string (null if not used)).</param>
        /// <returns>System.String.</returns>
        /// <exception cref="LengthOfArraysDifferentException"></exception>
        /// <example>
        /// Some examples of use:
        /// <code>
        /// // The following statement returns: #ex# #o #if# #nd #r#n#form
        /// string val = "Text to sift and transform".Sieve(new string[] { "T", "t", "a", "s" }, new string[] { "%", "#", "#", "#" }, null)
        /// </code><code>
        /// // The following statement returns: Text to evaluate and change
        /// string val = "Text to sift and transform".Sieve(new string[] { "sift", "transform" }, new string[] { "evaluate", "change" }, null);
        /// </code><code>
        /// // The following statement returns a empty string
        /// string val = "Text".Sieve(null, null, new string[] { "Text" });
        /// </code></example>
        /// <remarks>It's essential that both arrays "wordSearch" and "wordsReplace" contain the same number of elements,
        /// otherwise an exception <see cref="Mx.Core.LengthOfVectorsDifferentException" /> will be raised</remarks>
        public static string Sieve(this string text, string[] wordsSearch, string[] wordsReplace, string[] nullValues)
        {
            if (text == null) text = string.Empty;
            StringBuilder retTesto = new StringBuilder(text);
            try
            {
                if (null != wordsSearch && null != wordsReplace)
                {
                    // TODO: Implementare l'eccezione  
                    //if (wordsSearch.Length != wordsReplace.Length) throw new LengthOfArraysDifferentException();

                    for (int i = 0; i < wordsSearch.Length; i++)
                        retTesto.Replace(wordsSearch[i], wordsReplace[i]);
                }

                // if there are values that cancel the string ...
                if (nullValues != null)
                {
                    foreach (string str in nullValues)
                    {
                        if (retTesto.ToString() == str)
                        {
                            retTesto = new StringBuilder(string.Empty);
                            break;
                        }
                    }
                }
                return retTesto.ToString();
            }

            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return text;
            }
        }

        /// <summary>
        /// Splits the upper camel case text into array.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String[].</returns>
        /// <example>
        /// The following example makes it clear what you get from the function
        /// <code>
        /// // Next istruction
        /// string val = "TextToTransform".SplitUpperCamelCase();
        /// // Returns a new strings array with following elements:
        /// // "Text"
        /// // "To"
        /// // "Transform"
        /// </code></example>
        public static string[] SplitUpperCamelCase(this string text)
        {
            MatchCollection m = new Regex(@"[A-Z][a-z]+").Matches(text);
            string[] retVal = new string[m.Count];
            for (int i = 0; i < m.Count; i++)
                retVal[i] = m[i].Value;
            return retVal;
        }

        /// <summary>
        /// Returns a font from its string representation
        /// </summary>
        /// <param name="font">Font in string representation</param>
        /// <returns>Font obtained by the string</returns>
        public static Font StringToFont(this string font)
        {
            font = font.Substring(7, font.Length - 8).GetTrimForChar(' ');
            Font retVal = default(Font);
            try
            {
                setCulture();
                List<string> els = font.Split(',').ToList();
                Dictionary<string, string> elems = new Dictionary<string, string>();
                foreach (string item in els)
                {
                    if (item.Contains('='))
                    {
                        string[] ar = item.Split('=');
                        elems.Add(ar[0].Trim(), ar[1].Trim());
                    }
                }
                FontStyle style = TypeConvert<FontStyle>.GetValue(elems["Style"].Replace(';', ','), FontStyle.Regular);
                float size = TypeConvert<float>.GetValue(elems["Size"], 10f);
                retVal = new Font(elems["Name"], size, style);
            }
            catch { }
            finally { resetCulture(); }
            return retVal;
        }


        /// <summary>
        /// Converts the text string to a string in hexadecimal format
        /// </summary>
        /// <param name="value">Text string to convert</param>
        /// <returns>Text string to convert in hexadecimal format</returns>
        public static string StringToHex(this string value)
        {
            UnicodeEncoding unicode = new UnicodeEncoding();
            byte[] pwdByte = unicode.GetBytes(value);

            StringBuilder sb = new StringBuilder();
            foreach (byte b in pwdByte)
                sb.Append("#" + ((int)b).ToString("X"));

            return sb.ToString();
        }
        
        /// <summary>
        /// Converts a string containing a date value and time in format ISO 8601 in an <see cref="System.DateTime" /> instance
        /// </summary>
        /// <param name="dateTime">String containing a date value and time in format ISO 8601</param>
        /// <returns><see cref="System.DateTime" /> instance obtained</returns>
        public static DateTime ToDateTime(this string dateTime)
        {
            try
            {
                return DateTime.Parse(dateTime, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return default(DateTime);
            }
        }
        #endregion

        #region Method
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        /// <summary>
        /// Sets the culture.
        /// </summary>
        private static void setCulture()
        {
            cultureInfo = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
        }

        /// <summary>
        /// Resets the culture.
        /// </summary>
        private static void resetCulture()
        {
            Thread.CurrentThread.CurrentCulture = cultureInfo;
        }
        #endregion

        #region Method
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
