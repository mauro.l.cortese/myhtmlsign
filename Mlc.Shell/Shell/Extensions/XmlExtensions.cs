using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

namespace Mlc.Shell
{
    /// <summary>
    /// Estende la classe XmlNode
    /// </summary>
    public static class XmlEstensions
    {
        /// <summary>
        /// Ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <typeparam name="TypeValue">Tipo del valore richiesto</typeparam>
        /// <param name="node">Nodo da interrogare</param>
        /// <param name="attributeName">Nome attributo</param>
        /// <returns>Valore ottenuto</returns>
        public static TypeValue AttributeValue<TypeValue>(this XmlNode node, string attributeName)
        {
            return TypeConvert<TypeValue>.GetValue(node, attributeName, default(TypeValue));
        }

        /// <summary>
        /// Ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <typeparam name="TypeValue">Tipo del valore richiesto</typeparam>
        /// <param name="node">Nodo da interrogare</param>
        /// <param name="attributeName">Nome attributo</param>
        /// <param name="defaultValue">Valore di default</param> 
        /// <returns>Valore ottenuto</returns>
        public static TypeValue AttributeValue<TypeValue>(this XmlNode node, string attributeName, TypeValue defaultValue)
        {
            return TypeConvert<TypeValue>.GetValue(node, attributeName, defaultValue);
        }

        /// <summary>
        /// Determina se il nodo xml contiene un dato attributo
        /// </summary>
        /// <param name="node">Nodo da interrogare</param>
        /// <param name="attributeName">Nome attributo</param>
        /// <returns>True se il nodo contiene l'attributo richiesto, altrimenti false</returns>
        public static bool ContainsAttribute(this XmlNode node, string attributeName)
        {
            bool retVal = false;
            if (node.Attributes.Count == 0)
                return retVal;

            foreach (XmlAttribute xmlAttr in node.Attributes)
            {
                if (xmlAttr.Name == attributeName)
                {
                    retVal = true;
                    break;
                }
            }
            return retVal;
        }
    }
}
