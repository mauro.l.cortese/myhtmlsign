using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mlc.Shell.Crypto;

namespace Mlc.Shell
{
    /// <summary>
    /// Estensioni per la cifratura e decifratura delle stringhe
    /// </summary>
    /// <example>
    /// Tramite i metodi statici di questa classe è possibile compiere operazioni di cifratura e decifratura di stringhe 
    /// in modo molto veloce. Per utilizzare i metodi estesi aggiungere la seguente direttiva using
    /// <code>
    /// using Mx.Core.Extensions;
    /// </code>
    /// 
    /// <para>Questo esempio mostra come cifrare e decifrare una stringa servendosi della password di default della costante <see cref="Mx.Core.Constants.Strings.DefaultPwd"/></para>
    /// <code>
    /// // Cifratura veloce di una stringa
    /// string cryptoValue = "Testo da occultare".Encrypt();
    /// 
    /// // Restituzione del valore in chiaro
    /// string value = cryptoValue.Decrypt();
    /// </code>
    /// Ecco invece come cifrare e decifrare una stringa utilizzando una data password
    /// <code>
    /// // Cifratura veloce di una stringa con una password 
    /// string cryptoValue = "Testo da occultare".Encrypt("a1bd4@32");
    /// 
    /// // Restituzione del valore in chiaro con una password
    /// string value = cryptoValue.Decrypt("a1bd4@32");
    /// </code>
    /// </example>
    public static class CriptoStringExtension
    {
        /// <summary>
        /// Cifra il contenuto della stringa passata utilizzando la password della costante <see cref="Mx.Core.Constants.Strings.DefaultPwd"/>
        /// </summary>
        /// <param name="value">Stringa da cifrare</param>
        /// <returns>Stringa cifrata</returns>
        public static string Encrypt(this string value)
        {
            return value.Encrypt(string.Empty);
        }

        /// <summary>
        /// Cifra il contenuto della stringa passata
        /// </summary>
        /// <param name="value">Stringa da cifrare</param>
        /// <param name="password">Password per la cifratura della stringa</param>
        /// <returns>Stringa cifrata</returns>
        public static string Encrypt(this string value, string password)
        {
            RijndaelCryptoByte cb = new RijndaelCryptoByte(value.GetBytes());
            if (!string.IsNullOrEmpty(password))
                cb.Password = password;
            cb.Encrypt();
            return cb.Data.GetString();
        }

        /// <summary>
        /// Decifra il contenuto della stringa passata utilizzando la password della costante <see cref="Mx.Core.Constants.Strings.DefaultPwd"/>
        /// </summary>
        /// <param name="value">Stringa da cifrare</param>
        /// <returns>Stringa decifrata</returns>
        public static string Decrypt(this string value)
        {
            return value.Decrypt(string.Empty);
        }

        /// <summary>
        /// Decifra il contenuto della stringa passata
        /// </summary>
        /// <param name="value">Stringa da cifrare</param>
        /// <param name="password">Password per la decifratura della stringa</param>
        /// <returns>Stringa decifrata</returns>
        public static string Decrypt(this string value, string password)
        {
            RijndaelCryptoByte cb = new RijndaelCryptoByte(value.GetBytes());
            if (!string.IsNullOrEmpty(password))
                cb.Password = password;
            cb.Decrypt();
            return cb.Data.GetString();
        }
    }
}
