using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Mlc.Shell
{
    /// <summary>
    /// Estensioni per i tipi object.
    /// </summary>
    public static class ObjectExtension
    {
        /// <summary>
        /// Restituisce un dizionario con le proprietà dell'istanza passata
        /// </summary>
        /// <param name="obj">Istanza di cui ottenere le proprietà</param>
        /// <returns>Dizionario delle proprietà</returns>
        public static Dictionary<string, PropertyInfo> GetProperties(this object obj)
        {
            return GetProperties(obj, null);
        }

        /// <summary>
        /// Restituisce un dizionario con le proprietà dell'istanza passata
        /// </summary>
        /// <param name="obj">Istanza di cui ottenere le proprietà</param>
        /// <param name="attributeType">Tipo dell'attributo che le proprietà devono contenere</param>
        /// <returns>Dizionario delle proprietà</returns>
        public static Dictionary<string, PropertyInfo> GetProperties(this object obj, Type attributeType)
        {
            //return GetProperties(obj.GetType(), attributeType);
            PropertyInfo[] properties = obj.GetType().GetProperties();
            Dictionary<string, PropertyInfo> dicProperties = new Dictionary<string, PropertyInfo>();
            foreach (PropertyInfo pInfo in properties)
            {
                if (attributeType == null)
                    dicProperties.Add(pInfo.Name, pInfo);
                else
                {
                    if (pInfo.GetCustomAttributes(attributeType, true).Length == 1)
                        dicProperties.Add(pInfo.Name, pInfo);
                }
            }
            return dicProperties;
        }

        ///// <summary>
        ///// Restituisce un dizionario con le proprietà dell'istanza passata
        ///// </summary>
        ///// <param name="obj">Istanza di cui ottenere le proprietà</param>
        ///// <returns>Dizionario delle proprietà</returns>
        //[InfoCode("Mauro Cortese", "12/12/2011 16:53:25")]
        //public static Dictionary<string, PropertyInfo> GetProperties(this Type type)
        //{
        //    return GetProperties(type, null);
        //}

        ///// <summary>
        ///// Restituisce un dizionario con le proprietà dell'istanza passata
        ///// </summary>
        ///// <param name="obj">Istanza di cui ottenere le proprietà</param>
        ///// <param name="attributeType">Tipo dell'attributo che le proprietà devono contenere</param>
        ///// <returns>Dizionario delle proprietà</returns>
        //[InfoCode("Mauro Cortese", "12/12/2011 16:53:25")]
        //public static Dictionary<string, PropertyInfo> GetProperties(this Type type, Type attributeType)
        //{
        //    PropertyInfo[] properties = type.GetProperties();
        //    Dictionary<string, PropertyInfo> dicProperties = new Dictionary<string, PropertyInfo>();
        //    foreach (PropertyInfo pInfo in properties)
        //    {
        //        if (attributeType == null)
        //            dicProperties.Add(pInfo.Name, pInfo);
        //        else
        //        {
        //            if (pInfo.GetCustomAttributes(attributeType, true).Length == 1)
        //                dicProperties.Add(pInfo.Name, pInfo);
        //        }
        //    }
        //    return dicProperties;
        //}

        /// <summary>
        /// Valorizza le proprietà dell'istanza con il valore delle proprietà di una altra istanza
        /// </summary>
        /// <param name="obj">Istanza di cui valorizzare le proprietà</param>
        /// <param name="sourceObject">Istanza da ottenere i valori delle proprietà</param>
        public static void SetProperties(this object obj, object sourceObject)
        {
            Dictionary<string, PropertyInfo> thisProperties = obj.GetProperties();
            Dictionary<string, PropertyInfo> sourceProperties = sourceObject.GetProperties();

            // ciclo per le proprietà dell'oggetto corrente
            foreach (string key in thisProperties.Keys)
            {
                if (sourceProperties.ContainsKey(key))
                {
                    // se le proprietà sono dello stesso tipo
                    if (thisProperties[key].PropertyType == sourceProperties[key].PropertyType)
                        if (thisProperties[key].CanWrite)
                            thisProperties[key].SetValue(obj, sourceProperties[key].GetValue(sourceObject, null), null);
                }
            }
        }
    }
}
