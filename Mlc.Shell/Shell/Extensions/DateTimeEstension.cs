using System;
using System.Globalization;

namespace Mlc.Shell
{
    /// <summary>
    /// Estensioni per la classe DateTime.
    /// </summary>
    /// <example>
    /// Tramite i metodi statici di questa classe è possibile compiere operazioni rapide sulle istanze di DateTime
    /// Per utilizzare i metodi estesi aggiungere la seguente direttiva using
    /// <code>
    /// using Mx.Core.Extensions;
    /// </code>
    /// </example>
    public static class DateTimeExtension
    {
        /// <summary>
        /// Restituisce il numero della settimana per l'istanza DateTime passata
        /// </summary>
        /// <param name="dateTime">Istanza DateTime</param>
        /// <returns>Numero della settimana</returns>
        public static int GetNumberOfWeek(this DateTime dateTime)
        {
            // ottengo il numero del giorno della settimana del primo giorno dell'anno
            int dayOfWeekFirstDayOfYear = (int)new DateTime(dateTime.Year, 1, 1).DayOfWeek;
            // calcolo il numero della settimana
            int numberWeek = dateTime.DayOfYear / 7;
            if (dayOfWeekFirstDayOfYear <= 4 && dayOfWeekFirstDayOfYear > 0)
                numberWeek += 1;

            // se il numero della settimana vale 0 
            if (numberWeek == 0)
            {
                //ricavo il numero della settimana del giorno prima del giorno prima
                numberWeek = GetNumberOfWeek(dateTime - new TimeSpan(1, 0, 0, 0));
            }

            return numberWeek;
        }

        /// <summary>
        /// Restituisce una stringa che rappresenta il valore dateTime in notazione ISO 8601
        /// </summary>
        /// <param name="dateTime">Istanza DateTime</param>
        /// <returns>Stringa della data e ora in notazione ISO 8601</returns>
        public static string Serialize(this DateTime dateTime)
        {
            try
            {
                string retVal = dateTime.ToString(Constants.FormatISO8601, CultureInfo.InvariantCulture);
                return retVal;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return default(DateTime).Serialize();
            }
        }
    }
}
