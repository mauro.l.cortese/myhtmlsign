using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Mlc.Shell
{
    /// <summary>
    /// Estensioni per la classe Image.
    /// </summary>
    public static class ImageExtension
    {
        /// <summary>
        /// Converte l'immagine in un array di byte
        /// </summary>
        /// <param name="image">Immagine da convertire in array di byte</param>
        /// <returns>Array di byte dell'immagine</returns>
        public static byte[] ImageToByteArray(this Image image)
        {
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);
            return memoryStream.ToArray();
        }

        /// <summary>
        /// Converte un array di byte in un immagine
        /// </summary>
        /// <param name="byteImage">Array di byte da convertire in immagine</param>
        /// <returns>Immagine ottenuta</returns>
        public static Image ByteArrayToImage(this byte[] byteImage)
        {
            ImageConverter imageConverter = new ImageConverter();
            return (Image)imageConverter.ConvertFrom(byteImage);
        }
    }
}
