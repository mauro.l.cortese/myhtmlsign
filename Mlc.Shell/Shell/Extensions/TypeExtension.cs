using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mlc.Shell
{
    /// <summary>
    /// Fornisce metodi estesi per la classe  <see cref="System.Type"/>
    /// </summary>
    public static class TypeExtension
    {
        /// <summary>
        /// Restituisce un enumerato in base all'istanza <see cref="System.Type"/> passata
        /// </summary>
        /// <param name="type">Tipo da valutare</param>
        /// <returns>Enumerato del tipo di sistema</returns>
        public static SysTypeName GetTypeName(this Type type)
        {
            SysTypeName sysTypeName;
            if (type.BaseType == typeof(Enum))
                sysTypeName = SysTypeName.Enum;
            else
                sysTypeName = TypeConvert<SysTypeName>.GetValue(type.Name);
            return sysTypeName;
        }
    }
}
