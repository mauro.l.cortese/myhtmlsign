﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Drawing;

using Mlc.Shell.IO;

namespace Mlc.Shell
{
    /// <summary>
    /// Metodi estesi per la classe object 
    /// </summary>
    public class ReflectionTools
    {
        /// <summary>
        /// Imposta il valore per la proprietà passata convertendolo dal tipo stringa nel tipo corretto
        /// </summary>
        /// <param name="obj">Istanza per cui valorizzare la proprietà</param>
        /// <param name="property">Proprietà da valorizzare</param>
        /// <param name="value">Valore in forma di stringa da convertire e assegnare alla proprietà</param>
        /// <returns>True se l'assegnazione riesce, altrimenti false</returns>
        public bool SetProperty(object obj, PropertyInfo property, object value)
        {
            bool retVal = false;

            // se è possibile scrivere la proprietà ...
            if (property.CanWrite && property.PropertyType.GetGenericArguments().Length == 0)
            {
                object objValue = null;

                SysTypeName SysTypeName;
                if (property.PropertyType.BaseType == typeof(Enum))
                    SysTypeName = SysTypeName.Enum;
                else
                    SysTypeName = (SysTypeName)Enum.Parse(typeof(SysTypeName), property.PropertyType.Name);

                switch (SysTypeName)
                {
                    case SysTypeName.String:
                        objValue = TypeConvert<String>.GetValue(value);
                        break;
                    case SysTypeName.Char:
                        objValue = TypeConvert<Char>.GetValue(value);
                        break;
                    case SysTypeName.Byte:
                        objValue = TypeConvert<Byte>.GetValue(value);
                        break;
                    case SysTypeName.Int16:
                        objValue = TypeConvert<Int16>.GetValue(value);
                        break;
                    case SysTypeName.Int32:
                        objValue = TypeConvert<Int32>.GetValue(value);
                        break;
                    case SysTypeName.Int64:
                        objValue = TypeConvert<Int64>.GetValue(value);
                        break;
                    case SysTypeName.UInt16:
                        objValue = TypeConvert<UInt16>.GetValue(value);
                        break;
                    case SysTypeName.UInt32:
                        objValue = TypeConvert<UInt32>.GetValue(value);
                        break;
                    case SysTypeName.UInt64:
                        objValue = TypeConvert<UInt64>.GetValue(value);
                        break;
                    case SysTypeName.Single:
                        objValue = TypeConvert<float>.GetValue(value);
                        break;
                    case SysTypeName.Double:
                        objValue = TypeConvert<double>.GetValue(value);
                        break;
                    case SysTypeName.Decimal:
                        objValue = TypeConvert<decimal>.GetValue(value);
                        break;
                    case SysTypeName.Boolean:
                        objValue = TypeConvert<bool>.GetValue(value);
                        break;
                    case SysTypeName.DateTime:
                        objValue = TypeConvert<DateTime>.GetValue(value);
                        break;
                    case SysTypeName.Enum:
                        objValue = TypeConvert<Enum>.GetValue(value.ToString(), property.PropertyType.AssemblyQualifiedName);
                        break;
                    case SysTypeName.Point:
                        objValue = TypeConvert<Point>.GetValue(value.ToString());
                        break;
                    case SysTypeName.Rectangle:
                        objValue = TypeConvert<Rectangle>.GetValue(value.ToString());
                        break;
                    case SysTypeName.Size:
                        objValue = TypeConvert<Size>.GetValue(value.ToString());
                        break;
                    case SysTypeName.PointF:
                        objValue = TypeConvert<PointF>.GetValue(value);
                        break;
                    case SysTypeName.RectangleF:
                        objValue = TypeConvert<RectangleF>.GetValue(value);
                        break;
                    case SysTypeName.SizeF:
                        objValue = TypeConvert<SizeF>.GetValue(value);
                        break;
                    case SysTypeName.Color:
                        objValue = TypeConvert<Color>.GetValue(value.ToString());
                        break;
                    case SysTypeName.Fso:
                        // TODO: Completare implementazione del tipo FSO
                        //objValue = TypeConvert<FileSystemMgr>.GetValue(value.ToString(), new FileSystemMgr());
                        break;
                    case SysTypeName.Font:
                        objValue = TypeConvert<Font>.GetValue(value.ToString(), new Font("Arial", 8f));
                        break;
                    default:
                        break;
                }
                try
                {
                    property.SetValue(obj, objValue, null);
                    retVal = true;
                }
                catch
                {
                    retVal = false;
                }
            }
            return retVal;
        }
    }
}
