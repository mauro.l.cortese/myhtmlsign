﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-21-2015
// ***********************************************************************
// <copyright file="NSComment.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************

/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img> 
/// <para>The Mlc.Shell namespace contains all the base types that can be used for the development of applications</para>
/// </summary>
namespace Mlc.Shell
{

}
