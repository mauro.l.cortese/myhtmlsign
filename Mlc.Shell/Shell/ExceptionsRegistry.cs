using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mlc.Shell
{
    /// <summary>
    /// It provides a chronological record of the exceptions that are captured during run time. 
    /// This class implements the singleton pattern. This means that it is instantiable only once by the method <see cref="ExceptionsRegistry.GetInstance()"/>.
    /// <para></para>When a exception is captured and added to the <see cref="ExceptionsRegistry"/> instance, the event <see cref="ExceptionsRegistry.CapturedException"/> is generated</para>
    /// </summary>
    /// <example>
    /// In the below sample, a generic exception is captured and added to the <see cref="ExceptionsRegistry"/> instance
    /// <code>
    /// try
    /// {
    ///     // Do anything ...
    /// }
    /// catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
    /// </code>
    ///
    /// Here's how to get the notification of catching an exception
    /// <code>
    /// static void Main(string[] args)
    /// {
    ///     ExceptionsRegistry.GetInstance().CapturedException += new EventHandler(Program_CapturedException);
    ///     try
    ///     {
    ///         throw new Exception();
    ///     }
    ///     catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
    /// }
    /// 
    /// private static void Program_CapturedException(object sender, EventArgs e)
    /// {
    ///     throw new Exception();
    /// }
    /// </code>
    /// </example>
    public class ExceptionsRegistry : List<ExceptionsRegistry.RowException>
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>Variable for thread safe</summary>
        private static object syncRoot = new Object();
        /// <summary>Stores the instance of the class</summary>
        private static volatile ExceptionsRegistry instance = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="ExceptionsRegistry"/> class from being created.
        /// </summary>
        /// <remarks>The private constructor does not allow the direct instantiation of this class</remarks>
        private ExceptionsRegistry()
        {
            this.Clear();
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>The maximum row</summary>
        private int maxRow = 0;
        /// <summary>
        /// Gets or sets the maximum row.
        /// </summary>
        /// <remarks>0 to store all exceptions</remarks>
        /// <value>The maximum row.</value>
        public int MaxRow { get { return this.maxRow; } set { this.maxRow = value; } }
        #endregion

        #region Method Static
        /// <summary>
        /// Always returns the same instance of the class.
        /// </summary>
        /// <returns>The instance of the class <see cref="ExceptionsRegistry"/></returns>
        public static ExceptionsRegistry GetInstance()
        {
            // if the instance was not yet allocated
            if (instance == null)
            {
                lock (syncRoot)
                {
                    // it gets a new instance
                    if (instance == null)
                        instance = new ExceptionsRegistry();
                }
            }
            // it always returns the same instance
            return instance;
        }
        #endregion

        #region Method
        /// <summary>
        /// It adds a new instance of <see cref="Exception"/> to the registry
        /// </summary>
        /// <param name="exception">Exception to add</param>
        public void Add(Exception exception)
        {
            this.Add(new RowException(exception));

            // if you set the maximum number of exceptions to keep in the database and this number is exceeded ...
            if (this.maxRow != 0 && this.Count > this.maxRow)
                // it deletes the exception oldest
                this.RemoveAt(0);

            this.OnCapturedException(new EventArgs());
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Event Handlers
        #region CapturedException Event        
        /// <summary>
        /// Event that occurs when captured exception.
        /// </summary>
        public event EventHandler CapturedException;

        /// <summary>
        /// Handles the <see cref="E:CapturedException" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCapturedException(EventArgs e)
        {
            // If there are receptors listening ...
            if (CapturedException != null)
                // the event is raised
                CapturedException(this, e);
        }
        #endregion
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        /// <summary>
        /// This class is for Encapsulates the data of an exception, it is a row of <see cref="ExceptionsRegistry"/>
        /// </summary>
        public class RowException
        {
            #region Constants
            #endregion

            #region Enumerations
            #endregion

            #region Fields
            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="RowException"/> class.
            /// </summary>
            /// <param name="exception">The exception.</param>
            public RowException(Exception exception)

            {
                this.time = DateTime.Now;
                this.exception = exception;

                string[] stackLines = Environment.StackTrace.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                string[] stackLines2 = new string[stackLines.Length - 4];
                Array.Copy(stackLines, 4, stackLines2, 0, stackLines2.Length);

                this.stackTrace = string.Join(Environment.NewLine, stackLines2);
            }
            #endregion

            #region Public
            #region Properties
            /// <summary>The time at which the exception occurred</summary>
            private DateTime time;
            /// <summary>
            /// Gets the time at which the exception occurred.
            /// </summary>
            /// <value>The time.</value>
            public DateTime Time { get { return this.time; } }

            /// <summary>Instance of the exception</summary>
            private Exception exception;
            /// <summary>
            /// Gets the instance of the exception.
            /// </summary>
            /// <value>The instance of the exception.</value>
            public Exception Exception { get { return this.exception; } }

            /// <summary>The call stack report</summary>
            private string stackTrace;
            /// <summary>
            /// Gets the call stack report
            /// </summary>    
            public string StackTrace { get { return this.stackTrace; } }
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Internal
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Protected
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Private
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Event Handlers
            #endregion

            #region Event Definitions
            #endregion

            #region Embedded Types
            #endregion
        }
        #endregion
    }
}
