﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 07-21-2015
// ***********************************************************************
// <copyright file="Constants.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Shell
{
    /// <summary>
    /// Global common constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The default complex password
        /// </summary>
        public const string DefaultPwd = "p&S:bimrT6/Mo}UE2s;>5`%W_xNvVKPy";

        /// <summary>
        /// The default crypto data for encryption operations 
        /// </summary>
        public const string DefaultCryptoData = "<CryptoData><IV>GnoCp3X/cnPenkJ4Ug2obw==</IV><Key>R1aq7kfrlXUgMudEbHVx1kIXCrdd7tKUMK/nsQ8vg3E=</Key></CryptoData>";

        /// <summary>
        /// The string data format ISO 8601, its value is: yyyy-MM-ddTHH:mm:ss.fff
        /// </summary>
        public const string FormatISO8601 = "yyyy-MM-ddTHH:mm:ss.fff";

        /// <summary>
        /// The password that I need to use SwDocumentMgr.dll
        /// </summary>
        public const string SwDocumentMgrPwd = "B690AA55AB354A4C74AA91BA5497E6A0DD7CAEAB281F26C9";
    }

    /// <summary>
    /// Global common constants chars.
    /// </summary>
    public class Chars
    {
        /// <summary>
        /// The zero char ('0')
        /// </summary>
        public const char Zero = '0';
        /// <summary>
        /// The comma char (',')
        /// </summary>
        public const char Comma = ',';
        /// <summary>
        /// The dot char ('.')
        /// </summary>
        public const char Dot = '.';
        /// <summary>
        /// The blank char (' ')
        /// </summary>
        public const char Space = ' ';
        /// <summary>
        /// The double apex char ('"')
        /// </summary>
        public const char DoubleApex = '"';
        /// <summary>
        /// The dot comma char (';')
        /// </summary>
        public const char DotComma = ';';
        /// <summary>
        /// The double dot (':')
        /// </summary>
        public const char DoubleDot = ':';
        /// <summary>
        /// The opening bracket char ('(')
        /// </summary>
        public const char BracketOpen = '(';
        /// <summary>
        /// The closing bracket char (')')
        /// </summary>
        public const char BracketClose = ')';
        /// <summary>
        /// The opening square bracket char('[')
        /// </summary>
        public const char SquareBracketOpen = '[';
        /// <summary>
        /// The closing square bracket char (']')
        /// </summary>
        public const char SquareBracketClose = ']';
        /// <summary>
        /// The opening square brace char ('{')
        /// </summary>
        public const char BraceOpen = '{';
        /// <summary>
        /// The closing square brace char ('}')
        /// </summary>
        public const char BraceClose = '}';
        /// <summary>
        /// The underscore char ('_')
        /// </summary>
        public const char UnderScore = '_';
        /// <summary>
        /// The dash char ('-')
        /// </summary>
        public const char Dash = '-';
        /// <summary>
        /// The minor char ('&lt;')
        /// </summary>
        public const char Minor = '<';
        /// <summary>
        /// The major char ('&gt;')
        /// </summary>
        public const char Major = '>';
        /// <summary>
        /// The backslash char ('\')
        /// </summary>
        public const char BackSlash = '\\';
        /// <summary>
        /// The slash char ('/')
        /// </summary>
        public const char Slash = '/';
        /// <summary>
        /// The equal char ('=')
        /// </summary>
        public const char Equal = '=';
        /// <summary>
        /// The tab char ('\t')
        /// </summary>
        public const char Tab = '\t';
    }

    /// <summary>
    /// Constant for file system management.
    /// </summary>
    public class FsoExt
    {
        /// <summary>Estensione file link della shell ".lnk"</summary>
        public const string Lnk = ".lnk";
        /// <summary>Estensione file compressi ".zip"</summary>
        public const string Zip = ".zip";
        /// <summary>Estensione file compressi ".7z"</summary>
        public const string SevenZip = ".7z";
        /// <summary>Estensione file C# ".cs"</summary>
        public const string Cs = ".cs";
        /// <summary>Estensione file JPEG ".jpg"</summary>
        public const string Jpg = ".jpg";
        /// <summary>Estensione file BMP ".bmp"</summary>
        public const string Bmp = ".bmp";
        /// <summary>Estensione file PNG ".png"</summary>
        public const string Png = ".png";
        /// <summary>Estensione file PDF ".pdf"</summary>
        public const string Pdf = ".pdf";
        /// <summary>Estensione file XML ".xml"</summary>
        public const string Xml = ".xml";
        /// <summary>Estensione file Excel ".xls"</summary>
        public const string Xls = ".xls";
        /// <summary>Estensione file Word ".doc"</summary>
        public const string Doc = ".doc";
        /// <summary>Estensione file Excel ".xlsx"</summary>
        public const string Xlsx = ".xlsx";
        /// <summary>Estensione file Word ".docx"</summary>
        public const string Docx = ".docx";
        /// <summary>Estensione file diritti".mxrights"</summary>
        public const string MxRights = ".mxrights";
        /// <summary>Filtro ricerca tutto "*"</summary>
        public const string AllSearch = "*";
        /// <summary>Filtro ricerca tutto '*'</summary>
        public const char AllSearchChar = '*';
        /// <summary>Root path "ftp://"</summary>
        public const string FtpRoot = "ftp://";
        /// <summary>Estensione file cifrati ".encrypted"</summary>
        public const string Encrypted = ".encrypted";
        

        /// <summary>
        /// Restituisce il filtro composto con l'estensione passata nel formato (*.ext)
        /// </summary>
        /// <param name="ext">Estensione da filtrare</param>
        /// <returns>Filtro ottenuto</returns>
        public static string ToFilter(string ext)
        {
            return string.Format("{0}{1}", FsoExt.AllSearch, ext);
        }

        /// <summary>
        /// Restituisce il filtro per le finestre di dialogo apri file e salva file
        /// </summary>
        /// <param name="ext">Estensione da filtrare</param>
        /// <returns>Filtro ottenuto</returns>
        public static string ToFilterDialog(string ext)
        {
            return string.Format("{0} files (*{0})|*{0}|All files (*.*)|*.*", ext);
        }
    }
}
