﻿using System;
using System.Collections.Generic;

namespace Mlc.Shell
{
    /// <summary>
    /// Performs all the main common operations on enumerations with attribute [FlagsAttribute]
    /// </summary>
    /// <typeparam name="T">Any enumeration types</typeparam>
    public class EnumOperator<T>
    {
        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EnumOperator{T}"/> class.
        /// </summary>
        public EnumOperator()
        {
            if (typeof(T).BaseType != typeof(Enum))
                throw new TypeIsNotEnumException();
        }
        #endregion

        #region Public
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// It performs the union (OR) he specified first argument with second argument
        /// </summary>
        /// <param name="firstEnum">The first enum.</param>
        /// <param name="secondEnum">The second enum.</param>
        /// <returns>The result enum obtained.</returns>
        public T Union(T firstEnum, T secondEnum)
        {
            return (T)(object)((int)(object)firstEnum | (int)(object)secondEnum);
        }

        /// <summary>
        /// It performs the difference (XOR) by subtracting the second enum from first enum
        /// </summary>
        /// <param name="firstEnum">The first enum.</param>
        /// <param name="secondEnum">The second enum.</param>
        /// <returns>The result enum obtained.</returns>
        public T Difference(T firstEnum, T secondEnum)
        {
            return (T)(object)((int)((int)(object)firstEnum | (int)(object)secondEnum) ^ (int)(object)secondEnum);
        }

        /// <summary>
        /// It performs the intersection (AND) between the two values passed
        /// </summary>
        /// <param name="firstEnum">The first enum.</param>
        /// <param name="secondEnum">The second enum.</param>
        /// <returns>The result enum obtained.</returns>
        public T Intersection(T firstEnum, T secondEnum)
        {
            return (T)(object)((int)(object)firstEnum & (int)(object)secondEnum);
        }

        /// <summary>
        /// It determines whether the second argument is contained in the first argument.
        /// </summary>
        /// <param name="firstEnum">The first enum.</param>
        /// <param name="secondEnum">The second enum.</param>
        /// <returns><c>True</c> if second argument is contained in the first argument; otherwise, <c>false</c>.</returns>
        public bool Contains(T firstEnum, T secondEnum)
        {
            return (((int)(object)firstEnum & (int)(object)secondEnum) == (int)(object)secondEnum);
        }

        /// <summary>
        /// It returns a list of enumeration values between the first and second values
        /// </summary>
        /// <param name="firstEnum">The first enum.</param>
        /// <param name="secondEnum">The second enum.</param>
        /// <returns>List of enumerated values obtained</returns>
        public List<T> GetRange(T firstEnum, T secondEnum)
        {
            List<T> retList = new List<T>();
            Array vect = Enum.GetValues(typeof(T));

            Dictionary<T, int> dicEnum = new Dictionary<T, int>();
            for (int i = 0; i < vect.Length; i++)
                dicEnum.Add((T)vect.GetValue(i), i);

            foreach (KeyValuePair<T, int> item in dicEnum)
                if (item.Value > dicEnum[firstEnum] && item.Value < dicEnum[secondEnum])
                    retList.Add(item.Key);

            return retList;
        }

        /// <summary>
        /// It returns a list of all enumeration values
        /// </summary>
        /// <returns>List of enumerated values obtained</returns>
        public List<T> GetRange()
        {
            List<T> retList = new List<T>();
            Array vect = Enum.GetValues(typeof(T));

            Dictionary<T, int> dicEnum = new Dictionary<T, int>();
            for (int i = 0; i < vect.Length; i++)
                dicEnum.Add((T)vect.GetValue(i), i);

            foreach (KeyValuePair<T, int> item in dicEnum)
                retList.Add(item.Key);

            return retList;
        }

        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
