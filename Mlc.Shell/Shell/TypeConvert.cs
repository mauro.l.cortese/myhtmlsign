﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Xml;

using Mlc.Shell.IO;

namespace Mlc.Shell
{
    /// <summary>
    /// Tool per la conversione dei valori da formato stringa al tipo richiesto
    /// </summary>
    /// <typeparam name="ResultType">Tipo da ottenere</typeparam>
    public class TypeConvert<ResultType>
    {
        #region Metodi pubblici statici
        /// <summary>
        /// ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <param name="value">Valore da convertire</param>
        /// <returns>Valore ottenuto</returns>
        public static ResultType GetValue(object value)
        {
            if (value is DateTime && typeof(ResultType) == typeof(string))
                return (ResultType)Convert.ChangeType(((DateTime)value).Serialize(), typeof(ResultType));
            else if (value is string && typeof(ResultType) == typeof(DateTime))
                return (ResultType)Convert.ChangeType(((string)value).ToDateTime(), typeof(ResultType));
            else if (value is IConvertible)
                return (ResultType)Convert.ChangeType(value, typeof(ResultType));
            else
                if (value == null)
                return default(ResultType);
            else
                return TypeConvert<ResultType>.GetValue(value.ToString());
        }

        /// <summary>
        /// ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <param name="value">Valore da convertire</param>
        /// <returns>Valore ottenuto</returns>
        public static ResultType GetValue(string value)
        {
            return GetValue(value, default(ResultType));
        }

        /// <summary>
        /// ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <param name="value">Valore da convertire</param>
        /// <param name="defaultValue">Valore da restituire come default</param>
        /// <returns>Valore ottenuto</returns>
        public static ResultType GetValue(string value, ResultType defaultValue)
        {
            ResultType retval = defaultValue;
            if (defaultValue == null)
                defaultValue = default(ResultType);
            try
            {
                if (retval is Size) //"{Width=10, Height=20}" 
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 2)
                    {
                        Size size = new Size(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(size, typeof(ResultType));
                    }
                }
                else if (retval is Point) //"{X=15,Y=15}" 
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 2)
                    {
                        Point point = new Point(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(point, typeof(ResultType));
                    }
                }
                else if (retval is Rectangle) //"{X=10,Y=10,Width=200,Height=100}"
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 4)
                    {
                        Rectangle rectangle = new Rectangle(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1]),
                            int.Parse(vals[2].Split('=')[1]),
                            int.Parse(vals[3].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(rectangle, typeof(ResultType));
                    }
                }
                else if (retval is Enum)
                {
                    try
                    {
                        retval = (ResultType)Enum.Parse(typeof(ResultType), value, true);
                    }
                    catch (Exception ex)
                    {
                        ExceptionsRegistry.GetInstance().Add(ex);
                        retval = default(ResultType);
                    }
                }
                // TODO: Completare implementazione del tipo FSO
                //else if (retval is FileSystemMgr)
                //{
                //    try
                //    {
                //        retval = (ResultType)Convert.ChangeType(new FileSystemMgr(value), typeof(ResultType));
                //    }
                //    catch { retval = default(ResultType); }
                //}
                else if (retval is Color)
                    retval = (ResultType)Convert.ChangeType(value.ParseColor(), typeof(ResultType));
                else if (retval is Font)
                    retval = (ResultType)Convert.ChangeType(value.StringToFont(), typeof(ResultType));
                else
                    retval = (ResultType)Convert.ChangeType(value, typeof(ResultType));
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                if (!retval.Equals(defaultValue))
                    throw new Exception("Errore di conversione tipo");
            }
            return retval;
        }

        /// <summary>
        /// Ottiene il valore del tipo enumerato in base al suo assembly qualified name
        /// </summary>
        /// <param name="value">Valore da convertire</param>
        /// <param name="assemblyQualifiedName">Assembly qualified name del tipo dell'enumerato da ottenere</param>
        /// <returns>Enumerato ottenuto</returns>
        public static Enum GetValue(string value, string assemblyQualifiedName)
        {
            try
            {
                Type type = Type.GetType(assemblyQualifiedName);
                return (Enum)Enum.Parse(type, value);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return null;
            }
        }

        /// <summary>
        /// Ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <param name="xmlNode">Nodo xml da cui leggere un attributo</param>
        /// <param name="nameAttribute">Nome attributo</param>
        /// <returns>Valore ottenuto</returns>
        public static ResultType GetValue(XmlNode xmlNode, string nameAttribute)
        {
            return GetValue(xmlNode, nameAttribute, default(ResultType));
        }

        /// <summary>
        /// Ottiene il valore dell'attributo castato al tipo richiesto
        /// </summary>
        /// <param name="xmlNode">Nodo xml da cui leggere un attributo</param>
        /// <param name="nameAttribute">Nome attributo</param>
        /// <param name="defaultValue">Valore di default</param> 
        /// <returns>Valore ottenuto</returns>
        public static ResultType GetValue(XmlNode xmlNode, string nameAttribute, ResultType defaultValue)
        {
            try { return GetValue(xmlNode.Attributes[nameAttribute].Value, defaultValue); }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return defaultValue;
            }
        }

        /// <summary>
        /// (17/12/2008)
        /// <para>Imposta il valore per la proprietà passata convertendolo dal tipo stringa nel tipo corretto</para>
        /// </summary>
        /// <param name="obj">Istanza per cui valorizzare la proprietà</param>
        /// <param name="propertyName">Proprietà da valorizzare</param>
        /// <returns>Valore della proprietà</returns>
        public static ResultType GetProperty(object obj, string propertyName)
        {
            ResultType retVal = default(ResultType);

            PropertyInfo property = obj.GetType().GetProperty(propertyName);
            if (property != null)
                retVal = (ResultType)property.GetValue(obj, null);

            return retVal;
        }

        #endregion
    }
}
