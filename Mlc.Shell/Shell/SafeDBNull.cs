﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell
{
    public static class db
    {
        public static T SafeDBNull<T>(this object value, T defaultValue)
        {
            if (value == null)
                return default(T);

            if (value is string)
                return (T)Convert.ChangeType(value, typeof(T));


            if (value == DBNull.Value)
                return defaultValue;
            else if (value.GetType() == typeof(T))
                return (T)value;
            else
                return defaultValue;
        }

        public static T SafeDBNull<T>(this object value)
        {
            return value.SafeDBNull(default(T));
        }
    }

}
