﻿
namespace Mlc.Shell.JobNotify
{
    /// <summary>
    /// Enumerating all the types of log for a running process
    /// </summary>
    public enum JobLogType
    {
        /// <summary>
        /// Non impostato
        /// </summary>
        NotSet,
        /// <summary>
        /// Log su tipo Job
        /// </summary>
        Job,
        /// <summary>
        /// Log di step
        /// </summary>
        Step,
    }

    /// <summary>
    /// Enumerating all the possible results of execution of a job or a step
    /// </summary>
    public enum JobResult
    {
        /// <summary>
        /// Non impostato
        /// </summary>
        NotSet,
        /// <summary>
        /// Esecuzione del lavoro o dello step eseguita con successo
        /// </summary>
        Success,
        /// <summary>
        /// Esecuzione del lavoro o dello step fallita
        /// </summary>
        Failed,
        /// <summary>
        /// Esecuzione del lavoro o dello step interrotta dall'utente
        /// </summary>
        Stopped,
        /// <summary>
        /// Esecuzione del lavoro o dello step saltata
        /// </summary>
        Skipped,
        /// <summary>
        /// Esecuzione del lavoro o dello step ignorata
        /// </summary>
        Ignored,
        /// <summary>
        /// Esecuzione del lavoro avviata
        /// </summary>
        Started,
        /// <summary>
        /// Esecuzione del lavoro in pausa
        /// </summary>
        Paused,
    }

    /// <summary>
    /// Enumerazione dei possibili stati di una coda lavori
    /// </summary>
    public enum JobQueueState
    {
        /// <summary>
        /// Non impostato
        /// </summary>
        NotSet,
        /// <summary>
        /// Esecuzione in corso
        /// </summary>
        Running,
        /// <summary>
        /// Esecuzione finita
        /// </summary>
        Finished,
    }
}
