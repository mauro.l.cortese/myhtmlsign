﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Gui.DevExpressTools
{
    public class KeyValueCombo
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}
