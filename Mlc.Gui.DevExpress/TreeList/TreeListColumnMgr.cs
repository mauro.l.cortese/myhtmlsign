﻿using System;
using System.Drawing;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Mlc.Shell;
using Mlc.Shell.IO;
using DevExpress.XtraTreeList.Columns;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Classe Singleton pattern
    /// </summary>
    public class TreeListColumnMgr
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        #endregion

        #region Campi
        /// <summary>Variabile per safe thread</summary>
        private static object syncRoot = new Object();
        /// <summary>Istanza della classe</summary>
        private static volatile TreeListColumnMgr instance = null;
        /// <summary>
        /// The rep collection
        /// </summary>
        private Dictionary<string, RepositoryItem> repCollection = new Dictionary<string, RepositoryItem>();
        /// <summary>
        /// The list images small collection
        /// </summary>
        private Dictionary<string, ImageCollection> listImagesSmallCollection = new Dictionary<string, ImageCollection>();
        /// <summary>
        /// The list images large collection
        /// </summary>
        private Dictionary<string, ImageCollection> listImagesLargeCollection = new Dictionary<string, ImageCollection>();
        #endregion

        #region Costruttori
        /// <summary>
        /// Costruttore privato non permette l'istanziamento diretto di questa classe.
        /// </summary>
        private TreeListColumnMgr()
        { }
        #endregion

        #region Proprietà
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce sempre la stessa istanza della classe.
        /// </summary>
        public static TreeListColumnMgr GetInstance()
        {
            // se l'istanza non è ancora stata allocata
            if (instance == null)
            {
                lock (syncRoot)
                {
                    // ottengo una nuova istanza
                    if (instance == null)
                        instance = new TreeListColumnMgr();
                }
            }
            // restituisco sempre la stessa istanza 
            return instance;
        }

        /// <summary>
        /// Sets the column for icon.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="keyRepository">The key repository.</param>
        /// <param name="width">The width.</param>
        /// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
        /// <param name="allowSize">if set to <c>true</c> [allow size].</param>
        public void SetColumnForIcon(TreeListColumn column, string keyRepository, int width, bool fixedWidth, bool allowSize)
        {
            try
            {
                column.ColumnEdit = this.GetRepository(keyRepository, typeof(RepositoryItemImageComboBox), null);
                column.Width = width;
                column.OptionsColumn.FixedWidth = fixedWidth;
                column.OptionsColumn.AllowSize = allowSize;
                column.OptionsColumn.AllowFocus = false;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }


        /// <summary>
        /// Sets the column forlook up.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="table">The table.</param>
        /// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
        /// <param name="allowSize">if set to <c>true</c> [allow size].</param>
        public void SetColumnForlookUp(TreeListColumn column, DataTable table, bool fixedWidth, bool allowSize)
        {
            this.SetColumnForlookUp(column, column.FieldName, table, column.Width, fixedWidth, allowSize);
        }

        /// <summary>
        /// Sets the column for combo.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="keyRepository">The key repository.</param>
        /// <param name="table">The table.</param>
        /// <param name="width">The width.</param>
        /// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
        /// <param name="allowSize">if set to <c>true</c> [allow size].</param>
        public void SetColumnForlookUp(TreeListColumn column, string keyRepository, DataTable table, int width, bool fixedWidth, bool allowSize)
        {
            try
            {
                column.ColumnEdit = this.GetRepository(keyRepository, typeof(RepositoryItemComboBox), table);
                column.Width = width;
                column.OptionsColumn.FixedWidth = fixedWidth;
                column.OptionsColumn.AllowSize = allowSize;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

        }



        /// <summary>
        /// Gets the repository.
        /// </summary>
        /// <param name="keyRepository">The key repository.</param>
        /// <returns>RepositoryItem.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public RepositoryItem GetRepository(string keyRepository, Type typeRepository, DataTable table)
        {
            RepositoryItem retVal = null;

            if (!repCollection.ContainsKey(keyRepository))
            {
                switch (typeRepository.Name)
                {
                    case "RepositoryItemImageComboBox":
                        RepositoryItemImageComboBox riicb = new RepositoryItemImageComboBox();
                        riicb.AutoHeight = false;
                        riicb.Name = keyRepository;
                        repCollection.Add(keyRepository, riicb);
                        retVal = riicb;

                        string exeFolder = string.Format("{0}\\Resources\\Repositories\\{1}", Environment.CurrentDirectory, keyRepository);
                        ImageCollectionMgr icm = new ImageCollectionMgr();

                        this.listImagesSmallCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(20, 20), exeFolder + "\\Small"));
                        riicb.SmallImages = this.listImagesSmallCollection[keyRepository];

                        this.listImagesLargeCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(32, 32), exeFolder + "\\Large"));
                        riicb.LargeImages = this.listImagesLargeCollection[keyRepository];

                        foreach (ImageCollectionMgr.IconNameParser inp in icm.IconParserList)
                            riicb.Items.Add(new ImageComboBoxItem(inp.name, inp.value, inp.index));

                        break;
                    case "RepositoryItemComboBox":
                        RepositoryItemGridLookUpEdit riglue = new RepositoryItemGridLookUpEdit();
                        riglue.Name = keyRepository;
                        riglue.AutoHeight = false;
                        riglue.DataSource = table;
                        riglue.DisplayMember = "Value";
                        riglue.ValueMember = "Key";
                        repCollection.Add(keyRepository, riglue);
                        retVal = riglue;
                        break;
                }
            }

            retVal = repCollection[keyRepository];
            return retVal;
        }
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion
    }
}
