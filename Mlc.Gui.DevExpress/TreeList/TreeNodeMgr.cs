﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;
using Mlc.Data;
using System.Reflection;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Gestisce l'aspetto dei controlli GridView
    /// </summary>
    public static class TreeNodeMgr
    {
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="treeList">Istanza GridView da gestire</param>
        public static DsRowMapBase WriteRowMapBase(this TreeListNode node, DsRowMapBase row)
        {
            if (node == null || row == null)
                return row;
            TreeList tl = node.TreeList;
            foreach (TreeListColumn col in tl.Columns)
                row.SetValueProperty(col.FieldName, node.GetValue(col));
            return row;
        }

    }
}