﻿namespace Mlc.Gui.DevExpressTools
{
    partial class SqlEditorControl
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.richEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this.SuspendLayout();
            // 
            // richEditControl1
            // 
            this.richEditControl.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl.Appearance.Text.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richEditControl.Appearance.Text.Options.UseFont = true;
            this.richEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditControl.EnableToolTips = true;
            this.richEditControl.Location = new System.Drawing.Point(0, 0);
            this.richEditControl.Margin = new System.Windows.Forms.Padding(2);
            this.richEditControl.Name = "richEditControl1";
            this.richEditControl.Options.Export.PlainText.ExportFinalParagraphMark = DevExpress.XtraRichEdit.Export.PlainText.ExportFinalParagraphMark.Never;
            this.richEditControl.Options.Fields.UpdateFieldsInTextBoxes = false;
            this.richEditControl.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditControl.Options.HorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Visible;
            this.richEditControl.Options.Printing.PrintPreviewFormKind = DevExpress.XtraRichEdit.PrintPreviewFormKind.Bars;
            this.richEditControl.Size = new System.Drawing.Size(960, 462);
            this.richEditControl.TabIndex = 2;
            this.richEditControl.Views.DraftView.AllowDisplayLineNumbers = true;
            this.richEditControl.Views.DraftView.Padding = new System.Windows.Forms.Padding(60, 4, 0, 0);
            this.richEditControl.Views.SimpleView.AdjustColorsToSkins = true;
            this.richEditControl.DocumentLoaded += new System.EventHandler(this.richEditControl1_DocumentLoaded);
            this.richEditControl.BeforePagePaint += new DevExpress.XtraRichEdit.BeforePagePaintEventHandler(this.richEditControl1_BeforePagePaint);
            // 
            // SqlEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richEditControl);
            this.Name = "SqlEditorControl";
            this.Size = new System.Drawing.Size(960, 462);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraRichEdit.RichEditControl richEditControl;
    }
}
