﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 12-28-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-28-2017
// ***********************************************************************
// <copyright file="BarItemEditorMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars;
using System;
using System.Windows.Forms;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class BarItemEditorMgr.
    /// </summary>
    public class BarItemEditorMgr
    {
        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="BarItemEditorMgr"/> class.
        /// </summary>
        /// <param name="barEditItem">The bar edit item.</param>
        public BarItemEditorMgr(BarEditItem barEditItem)
        {
            barEditItem.EditValueChanged += barEditItem_EditValueChanged;
            barEditItem.Edit.KeyDown += edit_KeyDown;
        }
        #endregion

        #region Private Event Handlers
        /// <summary>
        /// Handles the KeyDown event of the edit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void edit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                SendKeys.Send("{TAB}");
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void barEditItem_EditValueChanged(object sender, EventArgs e)
        {
            SendKeys.Send("+{TAB}");
            this.OnEditValueChanged(new EventArgs());
        }
        #endregion

        #region Event
        #region Evento EditValueChanged                
        /// <summary>
        /// Delegato per la gestione dell'evento.
        /// </summary>
        public event EventHandler EditValueChanged;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnEditValueChanged(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (EditValueChanged != null)
                // viene lanciato l'evento
                EditValueChanged(this, e);
        }
        #endregion
        #endregion
    }
}
