﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 12-19-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-19-2017
// ***********************************************************************
// <copyright file="BarItemMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.Drawing;
using Mlc.Shell;

namespace Mlc.Gui.DevExpressTools
{

    /// <summary>
    /// Class BarItemMgr.
    /// </summary>
    public class BarItemMgr
    {

        /// <summary>
        /// The bar manager
        /// </summary>
        private BarManager barManager = null;
        /// <summary>
        /// The popup menu
        /// </summary>
        private PopupMenu popupMenu = null;
        /// <summary>
        /// The temporary items
        /// </summary>
        private List<BarButtonItem> tmpItems = new List<BarButtonItem>();


        /// <summary>
        /// Initializes a new instance of the <see cref="BarItemMgr"/> class.
        /// </summary>
        /// <param name="barManager">The bar manager.</param>
        /// <param name="popupMenu">The popup menu.</param>
        public BarItemMgr(BarManager barManager, PopupMenu popupMenu)
        {
            this.barManager = barManager;
            this.popupMenu = popupMenu;
        }


        /// <summary>
        /// Adds the item.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="glyph">The glyph.</param>
        /// <param name="largeGlyph">The large glyph.</param>
        /// <param name="beginGoup">if set to <c>true</c> [begin goup].</param>
        public BarButtonItem AddItem(string caption, Image glyph, Image largeGlyph, bool beginGoup)
        {
            BarButtonItem barButtonItemCad = new BarButtonItem(this.barManager, caption);
            this.tmpItems.Add(barButtonItemCad);
            barButtonItemCad.Glyph = glyph;
            barButtonItemCad.LargeGlyph = largeGlyph;
            this.popupMenu.AddItem(barButtonItemCad).BeginGroup = beginGoup;
            return barButtonItemCad;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            try
            {
                foreach (BarButtonItem item in this.tmpItems)
                {
                    this.popupMenu.RemoveLink(item.Links[0]);
                    this.barManager.Items.Remove(item);
                    item.Dispose();
                }
                this.tmpItems.Clear();
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
    }
}
