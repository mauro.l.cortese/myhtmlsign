﻿using System;
using System.Drawing;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Mlc.Shell;
using Mlc.Shell.IO;


namespace Mlc.Gui.DevExpressTools
{
    public class ImageCollectionMgr
    {
        private IconNameParserList inpl = new IconNameParserList();

        public IconNameParserList IconParserList { get { return this.inpl; } }

        public ImageCollectionMgr()
        { }

        public ImageCollection ImageCollectionFromFolder(Size size, string folder)
        {
            ImageCollection retVal = new ImageCollection();
            inpl = new IconNameParserList();

            retVal.ImageSize = size;
            Directory.CreateDirectory(folder);

            string[] files = Directory.GetFiles(folder);
            int cnt = 0;
            foreach (string file in files)
            {
                string[] part = Path.GetFileNameWithoutExtension(file).Split(new char[] { '_' });
                string name = part[1];
                inpl.Add(new IconNameParser(cnt, name, int.Parse(part[0])));
                retVal.AddImage(Image.FromStream(new LoadFileInStreaming(file).GetStream()), name);
                cnt++;
            }
            return retVal;
        }

        /// <summary>
        /// Class IconNameParserList.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.List{Mlc.Gui.DevExpressTools.GridViewColumnMgr.IconNameParser}" />
        public class IconNameParserList : List<IconNameParser>
        { }

        /// <summary>
        /// Class IconNameParser.
        /// </summary>
        public class IconNameParser
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="IconNameParser"/> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="name">The name.</param>
            /// <param name="value">The value.</param>
            public IconNameParser(int index, string name, int value)
            {
                this.index = index;
                this.name = name;
                this.value = value;
            }
            /// <summary>
            /// Gets the index.
            /// </summary>
            /// <value>The index.</value>
            public int index { get; }
            /// <summary>
            /// Gets the name.
            /// </summary>
            /// <value>The name.</value>
            public string name { get; }
            /// <summary>
            /// Gets the value.
            /// </summary>
            /// <value>The value.</value>
            public int value { get; }
        }
    }
}
