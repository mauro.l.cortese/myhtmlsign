﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-23-2017
// ***********************************************************************
// <copyright file="SqlPagePainter.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Layout;
using DevExpress.XtraRichEdit.API.Native;
using System.Drawing;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class SqlPagePainter.
    /// </summary>
    /// <seealso cref="DevExpressTools.XtraRichEdit.API.Layout.PagePainter" />
    public class SqlPagePainter : PagePainter
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        /// <summary>
        /// The rich edit control
        /// </summary>
        private RichEditControl richEditControl;
        /// <summary>
        /// The previous column index
        /// </summary>
        private int previousColumnIndex = -1;
        /// <summary>
        /// The line number font
        /// </summary>
        private Font lineNumberFont;
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlPagePainter"/> class.
        /// </summary>
        /// <param name="richEdit">The rich edit.</param>
        public SqlPagePainter(RichEditControl richEdit)
            : base()
        {
            richEditControl = richEdit;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlPagePainter"/> class.
        /// </summary>
        /// <param name="richEdit">The rich edit.</param>
        /// <param name="backColor">Color of the back.</param>
        /// <param name="style">The style.</param>
        public SqlPagePainter(RichEditControl richEdit, Color backColor, CharacterStyle style)
            : base()
        {
            richEditControl = richEdit;
            NumberingHighlightColor = backColor;
            NumberingFontName = style.FontName;
            NumberingFontSize = style.FontSize ?? 10F;
            NumberingFontColor = style.ForeColor ?? Color.Black;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the name of the numbering font.
        /// </summary>
        /// <value>The name of the numbering font.</value>
        public string NumberingFontName { get; set; }
        /// <summary>
        /// Gets or sets the size of the numbering font.
        /// </summary>
        /// <value>The size of the numbering font.</value>
        public float NumberingFontSize { get; set; }
        /// <summary>
        /// Gets or sets the color of the numbering font.
        /// </summary>
        /// <value>The color of the numbering font.</value>
        public Color NumberingFontColor { get; set; }
        /// <summary>
        /// Gets or sets the color of the numbering highlight.
        /// </summary>
        /// <value>The color of the numbering highlight.</value>
        public Color NumberingHighlightColor { get; set; }
        /// <summary>
        /// Gets or sets the line number padding.
        /// </summary>
        /// <value>The line number padding.</value>
        public int LineNumberPadding { get; set; }
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Draws the <see cref="T:DevExpress.XtraRichEdit.API.Layout.LayoutPage" /> element. Override it in class descendant to customize the element appearance.
        /// </summary>
        /// <param name="page">A layout element to draw.</param>
        public override void DrawPage(LayoutPage page)
        {
            lineNumberFont = new Font(NumberingFontName, NumberingFontSize, FontStyle.Regular);
            base.DrawPage(page);
            lineNumberFont.Dispose();
        }

        /// <summary>
        /// Draws the <see cref="T:DevExpress.XtraRichEdit.API.Layout.LayoutPageArea" /> element. Override it in class descendant to customize the element appearance.
        /// </summary>
        /// <param name="pageArea">A layout element to draw.</param>
        public override void DrawPageArea(LayoutPageArea pageArea)
        {
            Rectangle lineNumberBounds = new Rectangle(new Point(-LineNumberPadding, 0), new Size(LineNumberPadding, pageArea.Bounds.Height));
            Canvas.FillRectangle(new RichEditBrush(NumberingHighlightColor), lineNumberBounds);
            base.DrawPageArea(pageArea);
            previousColumnIndex = -1;
        }

        /// <summary>
        /// Draws the <see cref="T:DevExpress.XtraRichEdit.API.Layout.LayoutColumn" /> element. Override it in class descendant to customize the element appearance.
        /// </summary>
        /// <param name="column">A layout element to draw.</param>
        public override void DrawColumn(LayoutColumn column)
        {
            LayoutPageArea pageArea = column.GetParentByType<LayoutPageArea>();
            if (pageArea != null)
            {
                int leftBoundary = 0;
                if (previousColumnIndex >= 0)
                {
                    leftBoundary = pageArea.Columns[previousColumnIndex].Bounds.Right;
                }
                if (column.LineNumbers.Count > 0)
                {
                    this.highlightLineNumberingArea(column, leftBoundary);
                }
                previousColumnIndex++;
            }
            base.DrawColumn(column);
        }

        /// <summary>
        /// Draws the <see cref="T:DevExpress.XtraRichEdit.API.Layout.LineNumberBox" /> element. Override it in class descendant to customize the element appearance.
        /// </summary>
        /// <param name="lineNumberBox">A layout element to draw.</param>
        public override void DrawLineNumberBox(LineNumberBox lineNumberBox)
        {
            Canvas.DrawString(lineNumberBox.Text, lineNumberFont, new RichEditBrush(NumberingFontColor), lineNumberBox.Bounds, this.richEditControl.LayoutUnit);
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        /// <summary>
        /// Highlights the line numbering area.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="leftBoundary">The left boundary.</param>
        private void highlightLineNumberingArea(LayoutColumn column, int leftBoundary)
        {
            LayoutPage page = column.GetParentByType<LayoutPage>();
            Rectangle marginBounds = new Rectangle(new Point(leftBoundary, 0), new Size(column.Bounds.X - leftBoundary, page.Bounds.Height));
            Canvas.FillRectangle(new RichEditBrush(NumberingHighlightColor), marginBounds);
        }
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }
}
