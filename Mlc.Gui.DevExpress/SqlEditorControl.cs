﻿// ***********************************************************************
// Assembly         : LiveBom
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-23-2017
// ***********************************************************************
// <copyright file="SqlEditorControl.cs" company="MLC Development">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Office.Utils;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit.Services;
using Mlc.Gui.DevExpressTools;
using System;
using System.Drawing;
using System.Windows.Forms;
using Mlc.Shell;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class SqlEditorControl.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class SqlEditorControl : UserControl
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields        
        /// <summary>
        /// The character style
        /// </summary>
        private CharacterStyle characterStyle;
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlEditorControl"/> class.
        /// </summary>
        public SqlEditorControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties
        public string SqlText {
            get { return this.richEditControl.Text; }
            set { this.richEditControl.Text = value; }
            }
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.richEditControl.Text = null;
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        /// <summary>
        /// Handles the BeforePagePaint event of the richEditControl1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraRichEdit.BeforePagePaintEventArgs"/> instance containing the event data.</param>
        private void richEditControl1_BeforePagePaint(object sender, DevExpress.XtraRichEdit.BeforePagePaintEventArgs e)
        {
            try
            {
                if (e.CanvasOwnerType == DevExpress.XtraRichEdit.API.Layout.CanvasOwnerType.Printer)
                {
                    return;
                }

                SqlPagePainter customPagePainter = new SqlPagePainter(richEditControl, SystemColors.Info, this.characterStyle);
                customPagePainter.LineNumberPadding = 50;
                e.Painter = customPagePainter;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        /// <summary>
        /// Handles the DocumentLoaded event of the richEditControl1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void richEditControl1_DocumentLoaded(object sender, EventArgs e)
        {
            try
            {
                richEditControl.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
                richEditControl.ReplaceService<ISyntaxHighlightService>(new SqlSyntaxHighlightService(richEditControl.Document));
                richEditControl.Document.Sections[0].Page.Width = Units.MillimetersToDocumentsF(220f);
                richEditControl.Document.DefaultCharacterProperties.FontName = "Courier New";

                this.richEditControl.Views.SimpleView.Padding = new Padding(60, 4, 4, 0);
                this.richEditControl.Views.DraftView.Padding = new Padding(60, 4, 4, 0);
                richEditControl.Views.SimpleView.AllowDisplayLineNumbers = true;
                richEditControl.Views.DraftView.AllowDisplayLineNumbers = true;

                Section s = richEditControl.Document.Sections[0];
                s.LineNumbering.Start = 1;
                s.LineNumbering.CountBy = 1;
                s.LineNumbering.Distance = 60f;
                s.LineNumbering.RestartType = LineNumberingRestart.Continuous;

                this.characterStyle = richEditControl.Document.CharacterStyles["Line Number"];
                this.characterStyle.FontName = "Courier New";
                this.characterStyle.FontSize = 10;
                // this.characterStyle.ForeColor = Color.DarkGray;
                this.characterStyle.Bold = false;

            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }
}
