﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mlc.Shell.IO;

namespace Mlc.Gui.DevExpress
{
    public partial class _ViewLogger : UserControl
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="_ViewLogger"/> class.
        /// </summary>
        public _ViewLogger()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties
        //private Logger logger = new Logger();
        //public Logger Logger
        //{
        //    get { return this.logger; }
        //    set
        //    {
        //        this.logger = value;
        //        //this.gridControl1.data
        //            }
        //}

        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }
}
