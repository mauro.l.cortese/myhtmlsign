﻿using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Mlc.Data;
using System.Reflection;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Gestisce l'aspetto dei controlli GridView
    /// </summary>
    public sealed class GridSetup
    {
        /// <summary>GridView da gestire</summary>
        private GridView gridView = null;
        /// <summary>GridView da gestire</summary>
        private DsRowMapBase row = null;

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="gridView">Istanza GridView da gestire</param>
        public GridSetup(GridView gridView, DsRowMapBase row)
        {
            this.gridView = gridView;
            this.row = row;

            if (this.gridView != null)
                this.gridView.RowHeight = 24;
        }

        /// <summary>
        /// Initializes the grid.
        /// </summary>
        public void InitGrid(bool readOnly)
        {
            if (this.row == null) return;

            this.gridView.Columns.Clear();
            PropertyInfo[] propInfos = this.row.GetType().GetProperties();

            foreach (PropertyInfo prop in propInfos)
            {
                object[] attrs = prop.GetCustomAttributes(typeof(FieldAttribute), true);
                if (attrs.Length == 1)
                {
                    FieldAttribute fieldAttr = attrs[0] as FieldAttribute;
                    if (fieldAttr.Visible)
                    {
                        GridColumn gc = new GridColumn();
                        gc.Name = prop.Name; // string.Format("col{0}",prop.Name);
                        gc.FieldName = prop.Name;
                        gc.Visible = true;
                        gc.Caption = prop.Name;
                        gc.OptionsColumn.ReadOnly = readOnly;
                        switch (fieldAttr.HAlign)
                        {
                            case HorzAlignment.Left:
                                gc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                                gc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                                break;
                            case HorzAlignment.Center:
                                gc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                gc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                break;
                            case HorzAlignment.Right:
                                gc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                                gc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                                break;
                        }
                        this.gridView.Columns.Add(gc);
                    }
                }
            }
        }
    }
}