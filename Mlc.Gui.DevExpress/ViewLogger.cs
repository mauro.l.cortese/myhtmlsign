﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mlc.Shell.IO;

namespace Mlc.Gui.DevExpressTools
{
    public partial class ViewLogger : UserControl, IViewLogger
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewLogger"/> class.
        /// </summary>
        public ViewLogger()
        {
            InitializeComponent();
            this.sqlEditorControl.Clear();
        }

        #endregion

        #region Public Properties
        private LogList logList = null;
        public LogList LogList
        {
            get { return this.logList; }
            set
            {
                this.logList = value;
                if (this.logList != null)
                    this.gridControl1.DataSource = this.logList;
            }
        }

        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.sqlEditorControl.Clear();
        }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public void DataRefresh()
        {
            if (this.gridControl1 != null)
                this.gridControl1.RefreshDataSource();
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Log log = (Log)this.gridView1.GetRow(e.FocusedRowHandle);
            this.labelIndex.Text = log.Index.ToString();
            this.labelCommand.Text = log.CmdName;
            this.labelTime.Text = log.Time.ToString("hh.mm.ss ffff");
            //this.labelControl1.Text = string.Format("Log:{0} Command:{1} Time:{2}", log.Index, log.CmdName, log.Time.ToString("hh.mm.ss ffff"));
            this.sqlEditorControl.SqlText = log.Message;
        }
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
