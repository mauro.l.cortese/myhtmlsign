﻿        /// <summary>Valore mappato sul campo //DbFiledName></summary>
        protected //Type> //FieldName> = default(//Type>);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo //DbFiledName>
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual //Type> //PropertyName> { get { return this.//FieldName>; } set { this.//FieldName> = value; } }