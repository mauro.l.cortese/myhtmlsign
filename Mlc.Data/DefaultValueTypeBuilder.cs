using System;
using System.Drawing;

//using Mx.Core.Enumerations;
//using Mx.Core.Extensions;
//using Mx.Core.IO;
using Mlc.Shell;

namespace Mlc.Data
{
    /// <summary>
    /// Fornisce un metodo per ottenere il valore di default di un <see cref="System.Type"/> passato
    /// </summary>
    public class DefaultValueTypeBuilder
    {
        /// <summary>
        /// Restituisce il tipo di default
        /// </summary>
        /// <param name="type"><see cref="System.Type"/> di cui ottenre il valore di default</param>
        /// <returns>Valore di default per il tipo</returns>
        public object GetValue(Type type)
        {
            if (type.BaseType == typeof(Enum))
                type = typeof(Enum);

            object value = null;
            switch (type.GetTypeName())
            {
                case SysTypeName.Boolean:
                    value = default(bool);
                    break;
                case SysTypeName.Byte:
                    value = default(byte);
                    break;
                case SysTypeName.Char:
                    value = default(char);
                    break;
                case SysTypeName.Color:
                    value = default(Color);
                    break;
                case SysTypeName.DateTime:
                    value = DateTime.Now;
                    break;
                case SysTypeName.Decimal:
                    value = default(decimal);
                    break;
                case SysTypeName.Double:
                    value = default(double);
                    break;
                case SysTypeName.Enum:
                    value = default(Enum);
                    break;
                case SysTypeName.Exception:
                    value = new Exception();
                    break;
                case SysTypeName.Guid:
                    value = default(Guid);
                    break;
                case SysTypeName.Int16:
                    value = default(Int16);
                    break;
                case SysTypeName.Int32:
                    value = default(Int32);
                    break;
                case SysTypeName.Int64:
                    value = default(Int64);
                    break;
                case SysTypeName.Nullable:
                    value = default(Nullable);
                    break;
                case SysTypeName.Point:
                    value = default(Point);
                    break;
                case SysTypeName.PointF:
                    value = default(PointF);
                    break;
                case SysTypeName.Rectangle:
                    value = default(Rectangle);
                    break;
                case SysTypeName.RectangleF:
                    value = default(RectangleF);
                    break;
                case SysTypeName.SByte:
                    value = default(SByte);
                    break;
                case SysTypeName.Single:
                    value = default(Single);
                    break;
                case SysTypeName.Size:
                    value = default(Size);
                    break;
                case SysTypeName.SizeF:
                    value = default(SizeF);
                    break;
                case SysTypeName.String:
                    value = string.Empty;
                    break;
                case SysTypeName.TimeSpan:
                    value = default(TimeSpan);
                    break;
                case SysTypeName.UInt16:
                    value = default(UInt16);
                    break;
                case SysTypeName.UInt32:
                    value = default(UInt32);
                    break;
                case SysTypeName.UInt64:
                    value = default(UInt64);
                    break;
                //case SysTypeName.Fso:
                //    value = default(Fso);
                //    break;
                case SysTypeName.Font:
                    value = default(Font);
                    break;
                case SysTypeName.Icon:
                    value = default(Icon);
                    break;
                default:
                    throw new Exception("Tipo imprevisto");
            }
            return value;
        }
    }
}
