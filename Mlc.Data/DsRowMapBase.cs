using System;
using System.ComponentModel;
using System.Data;

namespace Mlc.Data
{
    /// <summary>
    /// Ereditare da questa classe per implementare tipi mappati sui dati di <see cref="System.Data.DataRow"/>
    /// </summary>
    public abstract class DsRowMapBase : IDataRowMapping
    {
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        public DsRowMapBase()
            :this(null)
        { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataRow">DataRow su cui mappare l'istanza</param>
        [Bindable(BindableSupport.No)]
        [Browsable(false)]
        public DsRowMapBase(DataRow dataRow)
        {
            this.DataRow = dataRow;
        }

        public object GetValue(string fieldName)
        {
            return this.GetValueProperty(fieldName);
        }

        #region Membri di ICloneable
        /// <summary>
        /// restituisce una nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/> copia di quella corrente 
        /// </summary>
        /// <returns>Nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/></returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        /// <summary>DataRow mappato sull'istanza</summary>
        internal DataRow dataRow = null;
        /// <summary>
        /// Restituisce o imposta il DataRow mappato sull'istanza
        /// </summary>
        [Field(Visible = false)]
        public DataRow DataRow
        {
            get
            {
                this.OutRowMapping();
                return this.dataRow;
            }
            set
            {
                this.dataRow = value;
                this.InRowMapping();
            }
        }

        public DataTable Table { get; internal set; }
    }
}
