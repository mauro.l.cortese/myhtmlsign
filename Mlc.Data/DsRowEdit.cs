using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Mlc.Shell;


namespace Mlc.Data
{
    /// <summary>
    /// Estensioni per i tipi che ereditano da <see cref="Mx.Core.Data.DsRowMapBase"/>
    /// </summary>
    public static class DsRowEdit
    {

        /// <summary>
        /// Writes the table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="listRowMap">The list row map.</param>
        /// <returns>DataTable.</returns>
        public static void WriteTable(DataTable table, List<DsRowMapBase> listRowMap)
        {
            foreach (DsRowMapBase rowMapBase in listRowMap)
            {
                if (table != null)
                    rowMapBase.Table = table;
                rowMapBase.WriteTable();
                if (table == null)
                    table = rowMapBase.Table;
            }
        }


        /// <summary>
        /// Writes the table.
        /// </summary>
        /// <param name="rowMapBase">The row map base.</param>
        public static void WriteTable(this DsRowMapBase rowMapBase)
        {
            DataTable table = rowMapBase.Table;
            if (table == null)
            {
                table = new DataTable();
                rowMapBase.Table = table;
            }

            if (table.Columns.Count == 0)
            {
                try
                {
                    Dictionary<string, PropertyInfo> props = rowMapBase.GetProperties();
                    foreach (PropertyInfo pInfo in props.Values)
                        if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
                            table.Columns.Add(pInfo.Name, pInfo.PropertyType);
                }
                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            }


            DataRow row = table.NewRow();
            table.Rows.Add(row);

            rowMapBase.dataRow = row;
            rowMapBase.OutRowMapping();
        }


        /// <summary>
        /// Aggiorna l'istanza con i dati del reader
        /// </summary>
        /// <param name="iRowMap">Istanza da aggiornare</param>
        /// <param name="reader">Reader con cui aggiornare l'istanza</param>
        public static void UpdateInnerRow(this DsRowMapBase iRowMap, IDataReader reader)
        {
            DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
            Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string fname = reader.GetName(i);
                if (props.ContainsKey(fname))
                {
                    Type pt = props[fname].PropertyType;
                    object value = reader[fname];
                    if (value == null || value is DBNull)
                        value = defaultValueType.GetValue(pt);

                    if (value.GetType() != pt)
                        value = Convert.ChangeType(value, pt);

                    props[fname].SetValue(iRowMap, value, null);
                }
            }
            OutRowMapping(iRowMap);
        }

        /// <summary>
        /// Aggiorna l'istanza con i dati del reader
        /// </summary>
        /// <param name="iRowMap">Istanza da aggiornare</param>
        /// <param name="dataRow">DataRow con cui aggiornare l'istanza</param>
        public static void UpdateInnerRow(this DsRowMapBase iRowMap, DataRow dataRow)
        {
            DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
            Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();

            DataTable dt = dataRow.Table;
            foreach (DataColumn col in dt.Columns)
            {
                string colName = col.ColumnName;
                if (props.ContainsKey(colName))
                {
                    object value = dataRow[colName];
                    if (value == null || value is DBNull)
                        value = defaultValueType.GetValue(iRowMap.dataRow.Table.Columns[colName].DataType);
                    props[colName].SetValue(iRowMap, value, null);
                }
            }
            OutRowMapping(iRowMap);
        }

        /// <summary>
        /// Copia i dati dell'istanza sul DataRow passato
        /// </summary>
        /// <param name="iRowMap">Istanza da cui copiare i dati</param>
        /// <param name="dataRow">DataRow in cui incollare i dati</param>
        public static void CopyToDataRow(this DsRowMapBase iRowMap, DataRow dataRow)
        {
            Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();
            DataTable dt = dataRow.Table;
            foreach (DataColumn col in dt.Columns)
            {
                string colName = col.ColumnName;
                if (props.ContainsKey(colName))
                    dataRow[colName] = props[colName].GetValue(iRowMap, null);
            }
        }


        /// <summary>
        /// Gets the value property.
        /// </summary>
        /// <param name="iRowMap">The i row map.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>System.Object.</returns>
        public static object GetValueProperty(this DsRowMapBase iRowMap, string fieldName)
        {
            object retVal = null;
            try
            {
                PropertyInfo prop = iRowMap.GetType().GetProperty(fieldName);
                retVal = prop.GetValue(iRowMap);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
            }
            return retVal;

        }


        /// <summary>
        /// Gets the value property.
        /// </summary>
        /// <param name="iRowMap">The i row map.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>System.Object.</returns>
        public static void SetValueProperty(this DsRowMapBase iRowMap, string fieldName, object value)
        {
            try
            {
                PropertyInfo prop = iRowMap.GetType().GetProperty(fieldName);
                prop.SetValue(iRowMap, value);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

        }

        /// <summary>
        /// Copia i dati dall'istanza al suo DataRow
        /// </summary>
        /// <param name="rowMap">Istanza per cui aggiornare il DataRow associato</param>
        public static void OutRowMapping(this DsRowMapBase rowMap)
        {
            if (rowMap.dataRow == null) return;
            foreach (PropertyInfo pInfo in rowMap.GetProperties().Values)
                if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
                    if (rowMap.dataRow.Table.Columns.Contains(pInfo.Name))
                        rowMap.dataRow[pInfo.Name] = pInfo.GetValue(rowMap, null);
        }

        /// <summary>
        /// Copia i dati del DataRow sull'istanza
        /// </summary>
        /// <param name="rowMap">Istanza da aggiornare in base al DataRow associato</param>
        public static void InRowMapping(this DsRowMapBase rowMap)
        {
            DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
            if (rowMap.dataRow == null) return;
            foreach (PropertyInfo pInfo in rowMap.GetProperties().Values)
                if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
                    try
                    {
                    if (rowMap.dataRow.Table.Columns.Contains(pInfo.Name))
                    {
                        object value = rowMap.dataRow[pInfo.Name];
                        if (value == null || value is DBNull)
                            value = defaultValueType.GetValue(rowMap.dataRow.Table.Columns[pInfo.Name].DataType);
                        if (pInfo.CanWrite)
                            pInfo.SetValue(rowMap, value, null);
                    }
                }
                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

    }
}
