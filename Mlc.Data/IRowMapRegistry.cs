﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mlc.Data
{
    /// <summary>
    /// Supporta un'istanza mappata ai dati di una riga <see cref="System.Data.DataRow"/>
    /// </summary>
    public interface IDataRowMapping : ICloneable
    {
        /// <summary>
        /// Restituisce o imposta il DataRow mappato sull'istanza
        /// </summary>
        DataRow DataRow { get; set; }
    }
}
