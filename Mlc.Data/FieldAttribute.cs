﻿// ***********************************************************************
// Assembly         : Mlc.Data
// Author           : Cortese Mauro Luigi
// Created          : 12-01-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-01-2017
// ***********************************************************************
// <copyright file="FieldAttribute.cs" company="MLC Development">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{

    /// <summary>
    /// Enum HorzAlignment
    /// </summary>
    public enum HorzAlignment
    {
        /// <summary>
        /// The left
        /// </summary>
        Left,
        /// <summary>
        /// The center
        /// </summary>
        Center,
        /// <summary>
        /// The right
        /// </summary>
        Right,
    }



    /// <summary>
    /// Class FieldAttribute.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldAttribute : Attribute
    {
        public FieldAttribute()
        {
            this.Visible = true;
            this.HAlign = HorzAlignment.Left;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FieldAttribute"/> is visible.
        /// </summary>
        /// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
        public bool Visible { get; set; }
        /// <summary>
        /// Gets or sets the h align.
        /// </summary>
        /// <value>The h align.</value>
        public HorzAlignment HAlign { get; set; }
    }
}
