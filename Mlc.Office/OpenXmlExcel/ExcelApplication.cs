﻿using System;
using System.IO;
using System.Diagnostics;

namespace Mlc.Office.Excel
{
    public class ExcelApplication
    {

        /// <summary>
        /// Starts the specified excel file.
        /// </summary>
        /// <param name="excelFile">The excel file.</param>
        public static Process Start(string excelFile)
        {
            FileInfo eFile = new FileInfo(excelFile);
            Process p = null;
            bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;
            if (isExcelInstalled && eFile.Exists)
                p = Process.Start(eFile.ToString());
            return p;
        }
    }
}
