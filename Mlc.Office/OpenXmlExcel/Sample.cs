﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
namespace Mlc.Office.Sample
{
    class Sample

    {
        static void Test()
    {

        //Read_From_Excel.getExcelFile();


        // https://www.codebyamir.com/blog/create-excel-files-in-c-sharp
        // https://stackoverflow.com/questions/tagged/epplus

        excelWriteNewFile();

        excelReadFile();


    }

    private static void excelReadFile()
    {
        using (var fs = new FileStream(@"D:\test.xlsx", FileMode.Open))
        {
            using (var package = new ExcelPackage(fs))
            {
                ExcelWorkbook workBook = package.Workbook;
                // Target a worksheet
                ExcelWorksheet worksheet = workBook.Worksheets["Worksheet1"];

                string value = worksheet.Cells[1, 1].GetValue<string>();
            }
        }

    }


    private static void excelWriteNewFile()
    {
        using (ExcelPackage excel = new ExcelPackage())
        {
            ExcelWorkbook workBook = excel.Workbook;

            workBook.Worksheets.Add("Worksheet1");
            workBook.Worksheets.Add("Worksheet2");
            workBook.Worksheets.Add("Worksheet3");

            var headerRow = new List<string[]>() { new string[] { "ID", "First Name", "Last Name", "DOB" } };

            // Determine the header range (e.g. A1:D1)
            string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

            // Target a worksheet
            var worksheet = excel.Workbook.Worksheets["Worksheet1"];

            // Popular header row data
            worksheet.Cells[headerRange].LoadFromArrays(headerRow);

            worksheet.Cells[headerRange].Style.Font.Bold = true;
            worksheet.Cells[headerRange].Style.Font.Size = 14;
            worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

            var cellData = new List<object[]> {
                    new object[] {0, "prova testo", 0, 1},
                    new object[] {10,"prova testo",7.42,1},
                    new object[] {20,"prova testo",5.24,1},
                    new object[] {30,"prova testo",4.16,1},
                    new object[] {40,"prova testo",4.16,1},
                    new object[] {50,"prova testo",4.16,1},
                    new object[] {60,"prova testo",4.16,1},
                    new object[] {70,"prova testo",4.16,1},
                    new object[] {80,"prova testo",4.16,1},
                    new object[] {90,"prova testo",4.16,1},
                    new object[] {100,"prova testo",4.16,1}
                };

            for (int i = 1; i <= 4; i++)
                worksheet.Column(i).AutoFit();

            worksheet.Cells[2, 1].LoadFromArrays(cellData);

            FileInfo excelFile = new FileInfo(@"D:\test.xlsx");
            excel.SaveAs(excelFile);

            bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;
            if (isExcelInstalled)
                System.Diagnostics.Process.Start(excelFile.ToString());
        }

    }
}
}
