﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using Mlc.Shell;
using Mlc.Data;

namespace Mlc.Office.Excel
{
    public class OpenXmlExcel
    {

        public string FileExcel { get; private set; }

        public OpenXmlExcel(string fileExcel)
        {
			// I use EPPlus in a noncommercial context
			// according to the Polyform Noncommercial license:
			ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

			this.FileExcel = fileExcel;
        }

        public void WriteTable(string workSheetName, DataTable table)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                ExcelWorkbook workBook = excel.Workbook;

                workBook.Worksheets.Add(workSheetName);

                List<string> fields = new List<string>();
                // write title
                foreach (DataColumn column in table.Columns)
                    fields.Add(column.Caption);


                var headerRow = new List<string[]>() { fields.ToArray() };

				// Determines the headers range (e.g. A1:D1)
				string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets[workSheetName];

                // Popular header row data
                worksheet.Cells.Style.Font.Size = 10;
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                worksheet.Cells[headerRange].Style.Font.Bold = true;
                worksheet.Cells[headerRange].Style.Font.Size = 12;
                // worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

                List<object[]> rows = new List<object[]>();
                foreach (DataRow row in table.Rows)
                {
                    List<object> cols = new List<object>();
                    foreach (DataColumn column in table.Columns)
                        cols.Add(row[column]);
                    rows.Add(cols.ToArray());
                }

                worksheet.Cells[2, 1].LoadFromArrays(rows);

                for (int i = 1; i <= table.Columns.Count; i++)
                    worksheet.Column(i).AutoFit();

                FileInfo excelFile = new FileInfo(this.FileExcel);
                excel.SaveAs(excelFile);
            }
        }

        public DataTable ReadTable(string workSheetName, Type rowMapType)
        {
            if (!File.Exists(this.FileExcel))
                return null;

            PropertyInfo[] pInfos = rowMapType.GetProperties();

            DataTable table = new DataTable();
            try
            {
                foreach (PropertyInfo pInfo in pInfos)
                    if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
                        table.Columns.Add(pInfo.Name, pInfo.PropertyType);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            if (table != null)
            {
                using (var fs = new FileStream(this.FileExcel, FileMode.Open))
                {
                    using (ExcelPackage package = new ExcelPackage(fs))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        ExcelWorksheet worksheet = workBook.Worksheets[workSheetName];

                        // Read titles

                        int r = 1;
                        Dictionary<string, int> titles = new Dictionary<string, int>();
                        for (int i = 1; i <= pInfos.Length; i++)
                        {
                            string title = worksheet.Cells[r, i].GetValue<string>();
                            if (!string.IsNullOrWhiteSpace(title))
                                titles.Add(title, i);
                        }

                        //Read values
                        bool exit = false;
                        r++;
                        while (!exit)
                        {
                            DataRow row = table.NewRow();
                            int nCol = 0;
                            foreach (DataColumn col in table.Columns)
                            {
                                object value = null;

                                value = worksheet.Cells[r, titles[col.ColumnName]].GetValue<object>();
                                nCol++;

                                if (nCol == 1 && value == null)
                                {
                                    exit = true;
                                    break;
                                }
                                else
                                    row[col.ColumnName] = Convert.ChangeType(value, col.DataType);
                            }
                            if (!exit)
                                table.Rows.Add(row);
                            r++;
                        }
                    }
                }
            }
            return table;
        }
    }
}
