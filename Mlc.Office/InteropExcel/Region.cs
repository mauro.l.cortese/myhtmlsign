using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Microsoft.Office.Interop.Excel;


namespace Mlc.Office.Excel
{
    /// <summary>
    /// Rappresenta una regione di celle (Range)
    /// </summary>
    public class Region
    {
        #region Campi
        // foglio del documento
        private _Worksheet worksheet = null;
        // range di celle
        private Range range = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        /// <param name="worksheet">foglio su cui operare</param>
        public Region(_Worksheet worksheet)
        {
            this.worksheet = worksheet;
        }
        #endregion

        #region Proprietà
        /// <summary>determina se la regione esiste</summary>
        private bool regionFounded;
        /// <summary>
        /// Restituisce true se la regione esiste, altrimenti false.
        /// </summary>                
        public bool RegionFounded { get { return regionFounded; } }

        /// <summary>
        /// Restituisce la riga d'inizio dell'intervallo.
        /// </summary>                
        public int Row { get { return this.range.Row; } }

        /// <summary>
        /// Restituisce il numero di righe dell'intervallo.
        /// </summary>                
        public int RowCount { get { return this.range.Rows.Count; } }

        /// <summary>
        /// Restituisce la colonna d'inizio dell'intervallo.
        /// </summary>                
        public int Column { get { return this.range.Column; } }

        /// <summary>
        /// Restituisce il numero di colonne dell'intervallo.
        /// </summary>                
        public int ColumnCount { get { return this.range.Columns.Count; } }        
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Ottiene l'intervallo di celle.
        /// </summary>
        /// <param name="nameRegion">nome dell'intervallo di celle</param>
        public void GetRegion(string nameRegion)
        {
            try
            {
                this.range = (Range)this.worksheet.get_Range(nameRegion, Missing.Value);
                this.regionFounded = true;
            }
            catch
            {

                this.regionFounded = false;
            }
        }

        /// <summary>
        /// Seleziona la regione di celle.
        /// </summary> 
        public void Select()
        {
            this.range.Select();
        }
        #endregion
    }
}
