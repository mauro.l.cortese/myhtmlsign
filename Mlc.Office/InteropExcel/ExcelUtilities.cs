using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mlc.Office.Excel
{
    /// <summary>
    /// Utilities per Excel
    /// </summary>
    public class ExcelUtilities
    {
        #region Campi
        /// <summary>Gestore di excel</summary>
        ExcelDocMgr excel = null;
        /// <summary>Collezione dei titoli per colonna</summary>
        Dictionary<string, int> titles = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza.
        /// </summary>
        /// <param name="excel">Gestore di excel</param>
        public ExcelUtilities(ExcelDocMgr excel)
        {
            this.excel = excel;
        }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce una collezione le cui chiavi sono composte dai valori dei titoli e i valori sono il numero della colonna in riferimento al titolo
        /// </summary>
        /// <param name="row">Riga da cui inizia la lettura</param>
        /// <param name="col">Colonna da cui inizia la lettura</param>
        /// <returns>Collezione di titoli con la posizione della colonna</returns>
        public Dictionary<string, int> GetRowTitleCollumns(int row, int col)
        {
            titles = new Dictionary<string, int>();
            object value = null;
            do
            {
                value = this.excel.GetCell(row, col);
                if (value!=null)
                    titles.Add(value.ToString(), col);
                col++;
			} while (value != null);

            return titles;
        }
        /// <summary>
        /// Restituisce una collezione le cui chiavi sono composte dai valori dei titoli e i valori il contenuto della cella
        /// in riferimento al titolo. NB. il metodo pressuppone che venga prima richiamato GetRowTitleCollumns per la riga di excel dei titoli
        /// sulla medesima istanza di questa classe.
        /// </summary>
        /// <param name="row">Riga da cui inizia la lettura</param>
        /// <param name="col">Colonna da cui inizia la lettura</param>
        /// <returns>Collezione di titoli con il valore delle celle corrispondenti</returns>
        public Dictionary<string, string> GetRowValueForTitle(int row, int col)
        {
            if (titles == null) return null;
            Dictionary<string, string> values = new Dictionary<string, string>();
            string value = null;
            foreach (KeyValuePair<string, int> de in this.titles)
            {
                value = this.excel.GetCell(row, de.Value).ToString();
                if (value == null) value = "";
                values.Add(de.Key.ToString(), value);
                col++;
            }
            return values;
        }
        #endregion
    }
}
