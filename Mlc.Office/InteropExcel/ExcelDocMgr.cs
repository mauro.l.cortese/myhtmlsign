using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

using MExcel = Microsoft.Office.Interop.Excel;

namespace Mlc.Office.Excel
{
    /// <summary>
    /// Classe per la gestione di documenti Excel
    /// </summary>
    public class ExcelDocMgr : IDisposable
    {
        #region Campi
        /// <summary>Applicazione Excel</summary>
        private MExcel.Application excelApp = null;
        /// <summary>Collezione di documenti dell'applicazione</summary>
        private MExcel.Workbooks workbooks = null;
        /// <summary>Documento dell'aplicazione</summary>
        private MExcel.Workbook workbook = null;
        /// <summary>Collezione dei fogli del documento</summary>
        private MExcel.Sheets sheets = null;
        /// <summary>Foglio del documento</summary>
        private MExcel.Worksheet worksheet = null;
        /// <summary>Memorizza la cultura corrente</summary>
        private CultureInfo memCultureInfo = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// 
        /// </summary>
        public ExcelDocMgr()
            : this(string.Empty)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public ExcelDocMgr(MExcel.Workbook workbook)
            : this(string.Empty)
        {
            this.excelApp = workbook.Application;
            this.workbook = workbook;
            this.workbooks = this.excelApp.Workbooks;
            this.sheets = this.workbook.Worksheets;
            this.worksheet = (MExcel.Worksheet)this.workbook.ActiveSheet;
        }

        /// <summary>
        /// Costruttore (necessario per registrazione COM)
        /// </summary>
        /// <param name="cultura">Cultura per inizializzare System.Globalization.CultureInfo </param>
        public ExcelDocMgr(string cultura)
        {
            // memorizza la cultura
            this.memCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            if (!string.IsNullOrWhiteSpace(cultura))
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultura);
        }
        #endregion

        #region Proprietà
        /// <summary>Regione di celle</summary>
        private Region region;
        /// <summary>
        /// Restituisce la regione di celle.
        /// </summary>                
        public Region Region { get { return region; } set { region = value; } }

        /// <summary>
        /// Restituisce il foglio di lavoro corrente
        /// </summary>
        public MExcel.Worksheet Worksheet { get { return this.worksheet; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Metodo per lanciare l'applicazione di Excel
        /// </summary>          
        /// <param name="showApp">Se vale true viene visualizzata l'applicazione di excel</param>
        public void LoadExcel(bool showApp)
        {
            if (excelApp == null)
                try { excelApp = (MExcel.Application)Marshal.GetActiveObject("Excel.Application"); }
                catch { }

            if (excelApp == null)
                excelApp = new MExcel.Application();

            // messaggio di errore se l'esecuzione di excel fallisce
            if (excelApp == null)
                throw new Exception("ERROR - Impossibile avviare Microsoft Excel");
            //new DialogBox().ShowError("ERROR - MLC_Excel !!", "Impossibile avviare Microsoft Excel",false);
            // imposto la visibilit� dell'applicazione excel.
            excelApp.Visible = showApp;
        }

        /// <summary>
        /// <para>Metodo per aprire un file esistente di Excel</para>
        /// </summary>
        /// <param name="file">Full path del file .xls da aprire</param>
        /// <returns>True se il file esiste e viene aperto con successo</returns>
        public bool OpenFileXLS(string file)
        {
            // testo l'esistenza del file
            FileInfo fi = new FileInfo(file);
            if (fi.Exists)
            {

                if (excelApp == null)
                    this.LoadExcel(false);

                try
                {
                    workbooks = excelApp.Workbooks;
                    try
                    {
                        workbooks.Open(fi.FullName, true, false, 1, "", "", false, MExcel.XlPlatform.xlWindows, "", false, false, 0, false, false, MExcel.XlCorruptLoad.xlNormalLoad);
                    }
                    catch
                    {
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        workbooks.Open(fi.FullName, true, false, 1, "", "", false, MExcel.XlPlatform.xlWindows, "", false, false, 0, false, false, MExcel.XlCorruptLoad.xlNormalLoad);
                    }
                    workbook = workbooks.get_Item(1);
                    sheets = workbook.Worksheets;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Salva il file in formato .csv (NON FUNZIONA !!!)
        /// </summary>
        /// <param name="file">Full path del file .csv da salvare</param>
        public void SaveCsv(string file)
        {
            workbook.SaveAs(file, "xlCSV", "", "", false, false, MExcel.XlSaveAsAccessMode.xlNoChange, MExcel.XlSaveConflictResolution.xlLocalSessionChanges, false, null, null, false);
            //ActiveWorkbook.SaveAs Filename:="C:\temp\TabellaVersioni.csv", FileFormat:=xlCSV, CreateBackup:=False
            //Filename      Argomento facoltativo di tipo Variant. Specifica il nome del file da salvare come stringa. Se non viene incluso il percorso completo, il file verr� salvato nella cartella corrente.
            //FileFormat      Argomento facoltativo di tipo Variant. Specifica il formato di file da utilizzare quando si salva il file. Vedere la propriet� FileFormat per visualizzare un elenco delle scelte valide. Per un file esistente, il formato predefinito � l'ultimo formato di file specificato, mentre per un file nuovo � il formato della versione di Excel utilizzata.
            //Password      Argomento facoltativo di tipo Variant. Specifica la password di protezione da assegnare al file, facendo distinzione tra maiuscole e minuscole, come stringa. La stringa pu� contenere un massimo di 15 caratteri.
            //WriteResPassword      Argomento facoltativo di tipo Variant. Specifica la password di diritto di scrittura del file, come stringa. Se non si specifica la password al momento dell'apertura di un file salvato con password, il file verr� aperto in sola lettura.
            //ReadOnlyRecommended      Argomento facoltativo di tipo Variant. Se ha valore True, all'apertura del file verr� visualizzato un messaggio in cui viene consigliato di aprire il file con l'attributo di sola lettura.
            //CreateBackup      Argomento facoltativo di tipo Variant. Se ha valore True verr� creato un file di backup.
            //AccessMode      Argomento facoltativo di tipo XlSaveAsAccessMode.
            //XlSaveAsAccessMode pu� essere una delle seguenti costanti XlSaveAsAccessMode. 
            //xlExclusive  (modalit� esclusiva) 
            //xlNoChange predefinita  (non cambia la modalit� di accesso) 
            //xlShared  (elenco condiviso) 
            //Se viene omesso, la modalit� di accesso non verr� cambiata. L'argomento viene ignorato se il nome del file non viene cambiato al salvataggio di un elenco condiviso. Per modificare la modalit� di accesso, utilizzare il metodo ExclusiveAccess.
            //ConflictResolution       Argomento facoltativo di tipo XlSaveConflictResolution.
            //XlSaveConflictResolution pu� essere una delle seguenti costanti XlSaveConflictResolution. 
            //xlUserResolution  (visualizza la finestra di dialogo di risoluzione dei conflitti) 
            //xlLocalSessionChanges (accetta automaticamente le modifiche dell'utente locale) 
            //xlOtherSessionChanges  (accetta modifiche diverse da quelle dell'utente locale) 
            //Se viene omesso, verr� visualizzata la finestra di dialogo di risoluzione dei conflitti.
            //AddToMru      Argomento facoltativo di tipo Variant. Se ha valore True questa cartella di lavoro verr� aggiunta all'elenco degli ultimi file usati. L'impostazione predefinita � False.
            //TextCodePage      Argomento facoltativo di tipo Variant. Non utilizzata nella versione inglese-americana di Microsoft Excel.
            //TextVisualLayout      Argomento facoltativo di tipo Variant. Non utilizzata nella versione inglese-americana di Microsoft Excel.
            //Local     Argomento facoltativo di tipo Variant. Se ha valore True i file verranno salvati nella lingua di Microsoft Excel (incluse le impostazioni del pannello di controllo). Se ha valore False (impostazione predefinita) i file verranno salvati nella lingua di Visual Basic, Applications Edition (VBA), che in genere � l'inglese (Stati Uniti) a meno che il progetto VBA da cui viene eseguito Workbooks.Open non sia un vecchio progetto VBA XL5/95 in una lingua diversa dall'inglese.
        }

        /// <summary>
        /// Salva il Worksheet nel formato corrente di Excel
        /// </summary>
        /// <param name="file">Path Completo per il Salvataggio del File</param>
        public void SaveXls(string file)
        {
            // if (!workbook.Saved)
            workbook.SaveAs(file,                                                       // Filename: Nome del file da salvare.                                        
                            workbook.FileFormat,                                        // FileFormat: Tipo di File da creare
                            "",                                                         // Password: Stringa contenente la Password per l'apertura
                            "",                                                         // WriteResPassword: Stringa contenente la Password per l'abilitazione alla Scrittura
                            false,                                                      // ReadOnlyRecommended: True=Messaggio per apertura in sola Lettura False=Apertura in lettura e scrittura
                            false,                                                      // CreateBackup: True=Crea il file di Backup , False= non crea backup
                            MExcel.XlSaveAsAccessMode.xlNoChange,                       // AccessMode: Tipo di Accesso da utilizzare (Type XlSaveAsAccessMode)
                            MExcel.XlSaveConflictResolution.xlLocalSessionChanges,      // ConflictResolution: Tipo di Politica per Risoluzione dei Conflitti (Type XlSaveConflictResolution)
                            false,                                                      // AddToMru: True Aggiunge il File alla lista degli ultimi file Aperti, False non lo aggiunge
                            Missing.Value,                                              // TextCodepage 
                            Missing.Value,                                              // TextVisualLayout
                            false                                                       // Local true salva i file nella lingia di Excel, False in quella del sistema di sviluppo
                            );


        }

        /// <summary>
        /// Imposta il foglio del ducumento di excel
        /// </summary>
        /// <param name="foglio">Numero del foglio da attivare</param>
        public void SetScheet(int foglio)
        {
            worksheet = (MExcel.Worksheet)sheets.get_Item(foglio);
            this.region = new Region(worksheet);
        }

        /// <summary>
        /// Imposta il foglio del ducumento di excel
        /// </summary>
        /// <param name="foglio">Nome del foglio da attivare</param>
        public void SetSheet(string foglio)
        {
            worksheet = (MExcel.Worksheet)sheets.get_Item(foglio);
            this.region = new Region(worksheet);
        }

        /// <summary>
        /// Metodo per assegnare un valore ad una cella
        /// </summary>
        /// <param name="row">numero di riga</param>
        /// <param name="col">numero di colonna</param>
        /// <param name="value">valore da assegnare alla cella</param>
        public void SetCell(int row, int col, object value)
        {
            worksheet.Cells[row, col] = value;
        }

        /// <summary>
        /// Imposta lo stile di una cella
        /// </summary>
        /// <param name="row">numero di riga</param>
        /// <param name="col">numero di colonna</param>
        /// <param name="bold">determina se il font � in grassetto o meno</param>
        public void SetCellStyle(int row, int col, bool bold)
        {
            MExcel.Range range = (MExcel.Range)worksheet.Cells[row, col];
            range.Font.Bold = bold;
        }

        /// <summary>
        /// Imposta gli allineamenti del testo di una cella
        /// </summary>
        /// <param name="row">Numero di riga</param>
        /// <param name="col">Numero di colonna</param>
        /// <param name="hAlign">Allineamento orrizontale</param>
        /// <param name="vAlign">Allineamento verticale</param>
        public void SetCellAlignment(int row, int col, CellHorizontalAlignment hAlign, CellVerticalAlignment vAlign)
        {
            MExcel.Range range = (MExcel.Range)worksheet.Cells[row, col];
            range.VerticalAlignment = MExcel.XlVAlign.xlVAlignCenter;
            range.HorizontalAlignment = MExcel.XlHAlign.xlHAlignCenter;
        }


        /// <summary>
        /// Restituisce gli allineamenti del testo di una cella
        /// </summary>
        /// <param name="row">Numero di riga</param>
        /// <param name="col">Numero di colonna</param>
        /// <param name="hAlign">Allineamento orrizontale</param>
        /// <param name="vAlign">Allineamento verticale</param>
        public void GetCellAlignment(int row, int col, out CellHorizontalAlignment hAlign, out CellVerticalAlignment vAlign)
        {
            MExcel.Range range = (MExcel.Range)worksheet.Cells[row, col];

            switch ((MExcel.XlVAlign)range.VerticalAlignment)
            {
                default:
                case MExcel.XlVAlign.xlVAlignBottom:
                    vAlign = CellVerticalAlignment.Bottom;
                    break;
                case MExcel.XlVAlign.xlVAlignCenter:
                    vAlign = CellVerticalAlignment.Center;
                    break;
                case MExcel.XlVAlign.xlVAlignDistributed:
                    vAlign = CellVerticalAlignment.Distributed;
                    break;
                case MExcel.XlVAlign.xlVAlignJustify:
                    vAlign = CellVerticalAlignment.Justify;
                    break;
                case MExcel.XlVAlign.xlVAlignTop:
                    vAlign = CellVerticalAlignment.Top;
                    break;
            }

            switch ((MExcel.XlHAlign)range.HorizontalAlignment)
            {
                case MExcel.XlHAlign.xlHAlignCenter:
                    hAlign = CellHorizontalAlignment.Center;
                    break;
                case MExcel.XlHAlign.xlHAlignDistributed:
                    hAlign = CellHorizontalAlignment.Distributed;
                    break;
                case MExcel.XlHAlign.xlHAlignJustify:
                    hAlign = CellHorizontalAlignment.Justify;
                    break;
                default:
                case MExcel.XlHAlign.xlHAlignFill:
                case MExcel.XlHAlign.xlHAlignGeneral:
                case MExcel.XlHAlign.xlHAlignCenterAcrossSelection:
                case MExcel.XlHAlign.xlHAlignLeft:
                    hAlign = CellHorizontalAlignment.Left;
                    break;
                case MExcel.XlHAlign.xlHAlignRight:
                    hAlign = CellHorizontalAlignment.Right;
                    break;
            }
        }


        /// <summary>
        /// Imposta il valore in una casella di testo all'interno del foglio Excel
        /// </summary>
        /// <param name="Nome">Nome della casella di testo</param>
        /// <param name="Valore">Stringa da inserire nella casella di testo</param>
        public bool SetTextBox(string Nome, string Valore)
        {
            bool retVal = false;
            // Prelevo l'oggetto casella di testo dal foglio excel
            try
            {
                MExcel.Shape textBox = this.worksheet.Shapes.Item(Nome);
                textBox.TextFrame.Characters(System.Type.Missing, System.Type.Missing).Text = Valore;
                retVal = true;
            }
            catch { }

            return retVal;
        }

        /// <summary>
        /// Restiruisce il testo contenuto in una casella di testo sul foglio Excel
        /// </summary>
        /// <param name="Nome">Nome della casella di testo</param>
        /// <returns>Stringa contenente il testo della casella</returns>
        public string GetTextBox(string Nome)
        {
            string retVal = null;

            // Prelevo l'Oggetto Casella di Testo dal Foglio Excel
            try
            {
                MExcel.Shape textBox = this.worksheet.Shapes.Item(Nome);
                retVal = textBox.TextFrame.Characters(System.Type.Missing, System.Type.Missing).Text;
            }
            catch { }
            return retVal;
        }

        /// <summary>
        /// Restituisce il valore di una cella
        /// </summary>
        /// <param name="row">numero di riga</param>
        /// <param name="col">numero di colonna</param>
        /// <returns>il valore della cella</returns>
        public object GetCell(int row, int col)
        {
            MExcel.Range range = (MExcel.Range)this.worksheet.Cells[row, col];
            return range.Value2;
        }

        /// <summary>
        /// Restituisce il valore di una cella
        /// </summary>
        /// <param name="row">numero di riga</param>
        /// <param name="col">numero di colonna</param>
        /// <returns>il valore della cella</returns>
        public string GetCellText(int row, int col)
        {
            object value = GetCell(row, col);
            if (value != null)
                return value.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// Restituisce la larghezza di una colonna
        /// </summary>
        /// <param name="col">Indice della colonna</param>
        /// <returns>La larghezza della colonna</returns>
        public float GetCollumnWidth(int col)
        {
            MExcel.Range range = (MExcel.Range)this.worksheet.Cells[col, col];
            string value = range.ColumnWidth.ToString();
            return Convert.ToSingle(value);
        }

        /// <summary>
        /// Imposta la larghezza di una colonna
        /// </summary>
        /// <param name="col">Indice della colonna</param>
        /// <param name="width">Larghezza della colonna</param>
        public void SetCollumnWidth(int col, int width)
        {
            MExcel.Range range = (MExcel.Range)this.worksheet.Cells[col, col];
            range.ColumnWidth = width;
        }

        /// <summary>
        /// Raggruppa un intervallo di righe
        /// </summary>         
        /// <param name="fromRow">Riga da cui inizia il gruppo (compresa)</param>
        /// <param name="toRow">Riga in cui termina il gruppo (compresa)</param>
        public void GroupRows(int fromRow, int toRow)
        {
            MExcel.Range rangeFrom = (MExcel.Range)this.worksheet.Cells[fromRow, 1]; // dalla riga ...
            MExcel.Range rangeTo = (MExcel.Range)this.worksheet.Cells[toRow, 1];     // ... alla riga 
            this.worksheet.Rows.get_Range(rangeFrom, rangeTo).Rows.Group(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
        }

        /// <summary>
        /// Raggruppa un intervallo di colonne
        /// </summary>         
        /// <param name="fromCol">Colonna da cui inizia il gruppo (compresa)</param>
        /// <param name="toCol">Colonna in cui termina il gruppo (compresa)</param>
        public void GroupCols(int fromCol, int toCol)
        {
            MExcel.Range rangeFrom = (MExcel.Range)this.worksheet.Cells[1, fromCol]; // dalla colonna ...
            MExcel.Range rangeTo = (MExcel.Range)this.worksheet.Cells[1, toCol];     // ... alla colonna 
            this.worksheet.Columns.get_Range(rangeFrom, rangeTo).Columns.Group(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
        }

        /// <summary>
        /// Elimina il raggruppamento ad un intervallo di righe
        /// </summary>         
        /// <param name="fromRow">Riga da cui inizia il gruppo (compresa)</param>
        /// <param name="toRow">Riga in cui termina il gruppo (compresa)</param>
        public void UnGroupRows(int fromRow, int toRow)
        {
            MExcel.Range rangeFrom = (MExcel.Range)this.worksheet.Cells[fromRow, 1]; // dalla riga ...
            MExcel.Range rangeTo = (MExcel.Range)this.worksheet.Cells[toRow, 1];     // ... alla riga 
            this.worksheet.Rows.get_Range(rangeFrom, rangeTo).Rows.Ungroup();
        }

        /// <summary>
        /// Elimina il raggruppamento ad un intervallo di colonne
        /// </summary>         
        /// <param name="fromCol">Colonna da cui inizia il gruppo (compresa)</param>
        /// <param name="toCol">Colonna in cui termina il gruppo (compresa)</param>
        public void UnGroupCols(int fromCol, int toCol)
        {
            MExcel.Range rangeFrom = (MExcel.Range)this.worksheet.Cells[1, fromCol]; // dalla colonna ...
            MExcel.Range rangeTo = (MExcel.Range)this.worksheet.Cells[1, toCol];     // ... alla colonna 
            this.worksheet.Columns.get_Range(rangeFrom, rangeTo).Columns.Ungroup();
        }

        /// <summary>
        /// Visualizza un dato livello di gruppo per le righe
        /// </summary>
        /// <param name="level">Livello da visualizzare</param>
        public void ShowLevelRows(int level)
        {
            this.worksheet.Outline.ShowLevels(level, Missing.Value);
        }

        /// <summary>
        /// Visualizza un dato livello di gruppo per le colonne
        /// </summary>
        /// <param name="level">Livello da visualizzare</param>
        public void ShowLevelColumns(int level)
        {
            this.worksheet.Outline.ShowLevels(Missing.Value, level);
        }

        #endregion

        #region Membri di IDisposable
        /// <summary>
        /// Rilascia le risorse
        /// </summary>
        public void Dispose()
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                // If user interacted with Excel it will not close when the app object is destroyed, so we close it explicitely
                if (workbook != null)
                {
                    workbook.Saved = true;
                    workbook.Save();
                    workbook.Close();
                    Marshal.ReleaseComObject(workbook);
                }
                if (this.excelApp == null) return;
                this.excelApp.UserControl = false;
                this.excelApp.Quit();
                Marshal.ReleaseComObject(this.excelApp);
            }
            catch { }
            finally
            { System.Threading.Thread.CurrentThread.CurrentCulture = this.memCultureInfo; }
        }
        #endregion

        public void ApplicationQuit()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            this.excelApp.Quit();
            Marshal.ReleaseComObject(this.excelApp);
        }
    }
}


