﻿namespace MyHtmlSign
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPageUsers = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabNavigationPageOptions = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditHtmlEditor = new DevExpress.XtraEditors.TextEdit();
            this.textEditAdmin = new DevExpress.XtraEditors.TextEdit();
            this.textEditDomain = new DevExpress.XtraEditors.TextEdit();
            this.textEditUserFolder = new DevExpress.XtraEditors.TextEdit();
            this.textEditFolderOutput = new DevExpress.XtraEditors.TextEdit();
            this.textEditDataFile = new DevExpress.XtraEditors.TextEdit();
            this.textEditHtmlMaster = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHtmlEditor = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabNavigationPageLog = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.richEditControlLog = new DevExpress.XtraRichEdit.RichEditControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItemOpenXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOpenHtml = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemPwd = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barButtonItemRun = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStop = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExpand = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemShowPreview = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHidePreview = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barEditItemProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barStaticItemInfo = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPageUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            this.tabNavigationPageOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHtmlEditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDomain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFolderOutput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHtmlMaster.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHtmlEditor)).BeginInit();
            this.tabNavigationPageLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(957, 569);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragDrop);
            this.gridControl1.DragEnter += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragEnter);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(20, 20);
            this.webBrowser.TabIndex = 1;
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPageUsers);
            this.tabPane1.Controls.Add(this.tabNavigationPageOptions);
            this.tabPane1.Controls.Add(this.tabNavigationPageLog);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 40);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPageUsers,
            this.tabNavigationPageOptions,
            this.tabNavigationPageLog});
            this.tabPane1.RegularSize = new System.Drawing.Size(963, 602);
            this.tabPane1.SelectedPage = this.tabNavigationPageUsers;
            this.tabPane1.Size = new System.Drawing.Size(963, 602);
            this.tabPane1.TabIndex = 18;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNavigationPageUsers
            // 
            this.tabNavigationPageUsers.BackgroundPadding = new System.Windows.Forms.Padding(2);
            this.tabNavigationPageUsers.Caption = "Utenti";
            this.tabNavigationPageUsers.Controls.Add(this.splitContainerControl);
            this.tabNavigationPageUsers.Name = "tabNavigationPageUsers";
            this.tabNavigationPageUsers.Size = new System.Drawing.Size(957, 569);
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.webBrowser);
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            this.splitContainerControl.Size = new System.Drawing.Size(957, 569);
            this.splitContainerControl.SplitterPosition = 500;
            this.splitContainerControl.TabIndex = 4;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // tabNavigationPageOptions
            // 
            this.tabNavigationPageOptions.BackgroundPadding = new System.Windows.Forms.Padding(2);
            this.tabNavigationPageOptions.Caption = "Impostazioni";
            this.tabNavigationPageOptions.Controls.Add(this.layoutControl2);
            this.tabNavigationPageOptions.Name = "tabNavigationPageOptions";
            this.tabNavigationPageOptions.Size = new System.Drawing.Size(957, 569);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEditHtmlEditor);
            this.layoutControl2.Controls.Add(this.textEditAdmin);
            this.layoutControl2.Controls.Add(this.textEditDomain);
            this.layoutControl2.Controls.Add(this.textEditUserFolder);
            this.layoutControl2.Controls.Add(this.textEditFolderOutput);
            this.layoutControl2.Controls.Add(this.textEditDataFile);
            this.layoutControl2.Controls.Add(this.textEditHtmlMaster);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(957, 569);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditHtmlEditor
            // 
            this.textEditHtmlEditor.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "HtmlEditor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditHtmlEditor.EditValue = global::MyHtmlSign.Properties.Settings.Default.HtmlEditor;
            this.textEditHtmlEditor.Location = new System.Drawing.Point(100, 151);
            this.textEditHtmlEditor.Name = "textEditHtmlEditor";
            this.textEditHtmlEditor.Size = new System.Drawing.Size(850, 20);
            this.textEditHtmlEditor.StyleController = this.layoutControl2;
            this.textEditHtmlEditor.TabIndex = 10;
            // 
            // textEditAdmin
            // 
            this.textEditAdmin.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "UserAdmin", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditAdmin.EditValue = global::MyHtmlSign.Properties.Settings.Default.UserAdmin;
            this.textEditAdmin.Location = new System.Drawing.Point(100, 127);
            this.textEditAdmin.Name = "textEditAdmin";
            this.textEditAdmin.Size = new System.Drawing.Size(850, 20);
            this.textEditAdmin.StyleController = this.layoutControl2;
            this.textEditAdmin.TabIndex = 9;
            this.textEditAdmin.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.textEditAdmin_EditValueChanging);
            // 
            // textEditDomain
            // 
            this.textEditDomain.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "Domain", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditDomain.EditValue = global::MyHtmlSign.Properties.Settings.Default.Domain;
            this.textEditDomain.Location = new System.Drawing.Point(100, 103);
            this.textEditDomain.Name = "textEditDomain";
            this.textEditDomain.Size = new System.Drawing.Size(850, 20);
            this.textEditDomain.StyleController = this.layoutControl2;
            this.textEditDomain.TabIndex = 8;
            this.textEditDomain.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.textEditDomain_EditValueChanging);
            // 
            // textEditUserFolder
            // 
            this.textEditUserFolder.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "ForlderUserTemplate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditUserFolder.EditValue = global::MyHtmlSign.Properties.Settings.Default.ForlderUserTemplate;
            this.textEditUserFolder.Location = new System.Drawing.Point(100, 79);
            this.textEditUserFolder.Name = "textEditUserFolder";
            this.textEditUserFolder.Size = new System.Drawing.Size(850, 20);
            this.textEditUserFolder.StyleController = this.layoutControl2;
            this.textEditUserFolder.TabIndex = 7;
            // 
            // textEditFolderOutput
            // 
            this.textEditFolderOutput.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "FolderCommonSigns", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditFolderOutput.EditValue = global::MyHtmlSign.Properties.Settings.Default.FolderCommonSigns;
            this.textEditFolderOutput.Location = new System.Drawing.Point(100, 55);
            this.textEditFolderOutput.Name = "textEditFolderOutput";
            this.textEditFolderOutput.Size = new System.Drawing.Size(850, 20);
            this.textEditFolderOutput.StyleController = this.layoutControl2;
            this.textEditFolderOutput.TabIndex = 6;
            // 
            // textEditDataFile
            // 
            this.textEditDataFile.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "DataFile", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditDataFile.EditValue = global::MyHtmlSign.Properties.Settings.Default.DataFile;
            this.textEditDataFile.Location = new System.Drawing.Point(100, 7);
            this.textEditDataFile.Name = "textEditDataFile";
            this.textEditDataFile.Size = new System.Drawing.Size(850, 20);
            this.textEditDataFile.StyleController = this.layoutControl2;
            this.textEditDataFile.TabIndex = 5;
            // 
            // textEditHtmlMaster
            // 
            this.textEditHtmlMaster.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::MyHtmlSign.Properties.Settings.Default, "FileHtmlMaster", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textEditHtmlMaster.EditValue = global::MyHtmlSign.Properties.Settings.Default.FileHtmlMaster;
            this.textEditHtmlMaster.Location = new System.Drawing.Point(100, 31);
            this.textEditHtmlMaster.Name = "textEditHtmlMaster";
            this.textEditHtmlMaster.Size = new System.Drawing.Size(850, 20);
            this.textEditHtmlMaster.StyleController = this.layoutControl2;
            this.textEditHtmlMaster.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItemHtmlEditor});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(957, 569);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditHtmlMaster;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem3.Text = "Folder HTM Master";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditFolderOutput;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem5.Text = "Folder signs";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditUserFolder;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem6.Text = "Folder User Sign";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditDataFile;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem4.Text = "File dati excel";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditDomain;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem8.Text = "Dominio";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEditAdmin;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(947, 24);
            this.layoutControlItem9.Text = "Utente admin";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItemHtmlEditor
            // 
            this.layoutControlItemHtmlEditor.Control = this.textEditHtmlEditor;
            this.layoutControlItemHtmlEditor.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItemHtmlEditor.Name = "layoutControlItemHtmlEditor";
            this.layoutControlItemHtmlEditor.Size = new System.Drawing.Size(947, 415);
            this.layoutControlItemHtmlEditor.Text = "HTML Editor";
            this.layoutControlItemHtmlEditor.TextSize = new System.Drawing.Size(90, 13);
            // 
            // tabNavigationPageLog
            // 
            this.tabNavigationPageLog.Caption = "Log";
            this.tabNavigationPageLog.Controls.Add(this.richEditControlLog);
            this.tabNavigationPageLog.Name = "tabNavigationPageLog";
            this.tabNavigationPageLog.Size = new System.Drawing.Size(0, 0);
            // 
            // richEditControlLog
            // 
            this.richEditControlLog.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControlLog.Appearance.Text.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richEditControlLog.Appearance.Text.Options.UseFont = true;
            this.richEditControlLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditControlLog.Location = new System.Drawing.Point(0, 0);
            this.richEditControlLog.MenuManager = this.barManager1;
            this.richEditControlLog.Name = "richEditControlLog";
            this.richEditControlLog.Size = new System.Drawing.Size(0, 0);
            this.richEditControlLog.TabIndex = 0;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemRun,
            this.barButtonItemOpenXlsx,
            this.barButtonItemOpenHtml,
            this.barEditItemPwd,
            this.barButtonItem1,
            this.barEditItemProgress,
            this.barButtonItemStop,
            this.barStaticItemInfo,
            this.barButtonItemExpand,
            this.barButtonItemShowPreview,
            this.barButtonItemHidePreview});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 11;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemProgressBar});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemOpenXlsx),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemOpenHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPwd),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemRun),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemStop),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemShowPreview),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemHidePreview)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // barButtonItemOpenXlsx
            // 
            this.barButtonItemOpenXlsx.Caption = "Apri file dati";
            this.barButtonItemOpenXlsx.Id = 1;
            this.barButtonItemOpenXlsx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenXlsx.ImageOptions.Image")));
            this.barButtonItemOpenXlsx.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenXlsx.ImageOptions.LargeImage")));
            this.barButtonItemOpenXlsx.Name = "barButtonItemOpenXlsx";
            this.barButtonItemOpenXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpenXlsx_ItemClick);
            // 
            // barButtonItemOpenHtml
            // 
            this.barButtonItemOpenHtml.Caption = "Apri file html";
            this.barButtonItemOpenHtml.Id = 2;
            this.barButtonItemOpenHtml.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenHtml.ImageOptions.Image")));
            this.barButtonItemOpenHtml.Name = "barButtonItemOpenHtml";
            this.barButtonItemOpenHtml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpenHtml_ItemClick);
            // 
            // barEditItemPwd
            // 
            this.barEditItemPwd.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemPwd.Caption = "Password";
            this.barEditItemPwd.Edit = this.repositoryItemTextEdit1;
            this.barEditItemPwd.EditWidth = 117;
            this.barEditItemPwd.Id = 3;
            this.barEditItemPwd.Name = "barEditItemPwd";
            this.barEditItemPwd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.PasswordChar = '*';
            // 
            // barButtonItemRun
            // 
            this.barButtonItemRun.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemRun.Caption = "Avvia generazione firme";
            this.barButtonItemRun.Id = 0;
            this.barButtonItemRun.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemRun.ImageOptions.Image")));
            this.barButtonItemRun.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemRun.ImageOptions.LargeImage")));
            this.barButtonItemRun.Name = "barButtonItemRun";
            this.barButtonItemRun.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRun_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Ricarica i dati";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItemStop
            // 
            this.barButtonItemStop.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemStop.Caption = "Stop";
            this.barButtonItemStop.Id = 6;
            this.barButtonItemStop.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemStop.ImageOptions.Image")));
            this.barButtonItemStop.Name = "barButtonItemStop";
            this.barButtonItemStop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStop_ItemClick);
            // 
            // barButtonItemExpand
            // 
            this.barButtonItemExpand.Caption = "Expandi tutto";
            this.barButtonItemExpand.Id = 8;
            this.barButtonItemExpand.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemExpand.ImageOptions.Image")));
            this.barButtonItemExpand.Name = "barButtonItemExpand";
            this.barButtonItemExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExpand_ItemClick);
            // 
            // barButtonItemShowPreview
            // 
            this.barButtonItemShowPreview.Caption = "Preview";
            this.barButtonItemShowPreview.Id = 9;
            this.barButtonItemShowPreview.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemShowPreview.ImageOptions.Image")));
            this.barButtonItemShowPreview.Name = "barButtonItemShowPreview";
            this.barButtonItemShowPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemShowPreview_ItemClick);
            // 
            // barButtonItemHidePreview
            // 
            this.barButtonItemHidePreview.Caption = "Nascondi Preview";
            this.barButtonItemHidePreview.Id = 10;
            this.barButtonItemHidePreview.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemHidePreview.ImageOptions.Image")));
            this.barButtonItemHidePreview.Name = "barButtonItemHidePreview";
            this.barButtonItemHidePreview.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            this.barButtonItemHidePreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHidePreview_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemProgress),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInfo)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barEditItemProgress
            // 
            this.barEditItemProgress.Caption = "barEditItem1";
            this.barEditItemProgress.Edit = this.repositoryItemProgressBar;
            this.barEditItemProgress.EditValue = 50;
            this.barEditItemProgress.EditWidth = 290;
            this.barEditItemProgress.Id = 5;
            this.barEditItemProgress.Name = "barEditItemProgress";
            // 
            // repositoryItemProgressBar
            // 
            this.repositoryItemProgressBar.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.repositoryItemProgressBar.Name = "repositoryItemProgressBar";
            this.repositoryItemProgressBar.ShowTitle = true;
            this.repositoryItemProgressBar.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            // 
            // barStaticItemInfo
            // 
            this.barStaticItemInfo.Caption = ".";
            this.barStaticItemInfo.Id = 7;
            this.barStaticItemInfo.Name = "barStaticItemInfo";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(963, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 642);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(963, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 602);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(963, 40);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 602);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // FormMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 667);
            this.Controls.Add(this.tabPane1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "My Html Sign";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPageUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            this.tabNavigationPageOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditHtmlEditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDomain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFolderOutput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHtmlMaster.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHtmlEditor)).EndInit();
            this.tabNavigationPageLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageUsers;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageOptions;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditUserFolder;
        private DevExpress.XtraEditors.TextEdit textEditFolderOutput;
        private DevExpress.XtraEditors.TextEdit textEditDataFile;
        private DevExpress.XtraEditors.TextEdit textEditHtmlMaster;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageLog;
        private DevExpress.XtraRichEdit.RichEditControl richEditControlLog;
        private System.Windows.Forms.WebBrowser webBrowser;
        private DevExpress.XtraEditors.TextEdit textEditAdmin;
        private DevExpress.XtraEditors.TextEdit textEditDomain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRun;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenXlsx;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenHtml;
        private DevExpress.XtraBars.BarEditItem barEditItemPwd;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.TextEdit textEditHtmlEditor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHtmlEditor;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarEditItem barEditItemProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private DevExpress.XtraBars.BarButtonItem barButtonItemStop;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInfo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExpand;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private DevExpress.XtraBars.BarButtonItem barButtonItemShowPreview;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHidePreview;
    }
}

