﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyHtmlSign {
    using System;
    
    
    /// <summary>
    ///   Classe di risorse fortemente tipizzata per la ricerca di stringhe localizzate e così via.
    /// </summary>
    // Questa classe è stata generata automaticamente dalla classe StronglyTypedResourceBuilder.
    // tramite uno strumento quale ResGen o Visual Studio.
    // Per aggiungere o rimuovere un membro, modificare il file con estensione ResX ed eseguire nuovamente ResGen
    // con l'opzione /str oppure ricompilare il progetto VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Master {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Master() {
        }
        
        /// <summary>
        ///   Restituisce l'istanza di ResourceManager nella cache utilizzata da questa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MyHtmlSign.Master", typeof(Master).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Esegue l'override della proprietà CurrentUICulture del thread corrente per tutte le
        ///   ricerche di risorse eseguite utilizzando questa classe di risorse fortemente tipizzata.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a :root {
        ///  --fore_color: #000000;
        ///  --background_color: #FFFFFF;
        ///  --border_color: #FFFFFF;
        ///  --left:120px
        ///}
        ///#contenitore-box{
        ///position:relative;
        ///}
        ///#box-1{
        ////*position:absolute;*/
        ///float:left;
        ///left:var(--left);
        ///top:10px;
        ///border:1px solid var(--border_color);
        ///padding:0em;
        ///width:200px;
        ///background-color:var(--background_color);
        ///color:var(--fore_color);
        ///}
        ///#box-2{
        ////*position:absolute;*/
        ///float:left;
        ///left:160px;
        ///top:10px;
        ///border:1px solid var(--border_color);
        ///width:260px;
        ///background-color:v [stringa troncata]&quot;;.
        /// </summary>
        internal static string myCss {
            get {
                return ResourceManager.GetString("myCss", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD HTML 4.01//EN&quot;&gt;
        ///&lt;!-- Fields
        ///	{Fullname}
        ///	{Role}
        ///	{MobileLabel}
        ///	{MobileNumber}
        ///	{PhoneNumber}
        ///	{Email}
        ///	{Web}
        ///--&gt;
        ///&lt;html&gt;
        ///	&lt;head&gt;
        ///	  &lt;title&gt;My first styled page&lt;/title&gt;
        ///		&lt;link rel=&quot;stylesheet&quot; href=&quot;my.css&quot;&gt;
        ///	&lt;/head&gt;
        ///	&lt;body&gt;
        ///		&lt;div id=&quot;top&quot;&gt;
        ///			&lt;div id=&quot;box-1&quot;&gt;
        ///				&lt;a href=&quot;https://www.live-tech.com/&quot;&gt;
        ///					&lt;img style=&quot;border: 0px; width: 157px; height: 39px; margin: 10px 0px 0px 10px;&quot;
        ///					alt=&quot;Zayon&quot; 
        ///					src=&quot;https://wiki.live-tech.com/signs_image [stringa troncata]&quot;;.
        /// </summary>
        internal static string myHtml {
            get {
                return ResourceManager.GetString("myHtml", resourceCulture);
            }
        }
    }
}
