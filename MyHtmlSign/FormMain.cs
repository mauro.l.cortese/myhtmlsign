﻿using System;
using System.Net.NetworkInformation;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mlc.Office.Excel;
using Mlc.Shell;
using Mlc.Data;
using Mlc.Gui.DevExpressTools;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraBars;


namespace MyHtmlSign
{
    public partial class FormMain : DevExpress.XtraEditors.XtraForm
    {

        string openedFileXlsx = null;

        public delegate void WriteLog(string msgLog, bool showInfo);
        public WriteLog writelogDelegate;

        public FormMain()
        {
            InitializeComponent();
            this.writelogDelegate = new WriteLog(this.writelog);
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                this.loadFile(files[0]);
            }
        }

        private void loadFile(string fileXlsx)
        {
            Document doc = this.richEditControlLog.Document;
            DataTable table = loadTable(fileXlsx);
            this.gridControl1.DataSource = table;
            this.writeLabelUser();
            this.loadLayout();
            this.gridView1.ExpandAllGroups();
            this.openedFileXlsx = fileXlsx;
        }

        private DataTable loadTable(string fileXlsx)
        {
            OpenXmlExcel oxe = new OpenXmlExcel(fileXlsx);
            DataTable table = oxe.ReadTable("Users", typeof(User));
            this.repositoryItemProgressBar.Maximum = table.Rows.Count;
            this.barEditItemProgress.EditValue = 0;
            return table;
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        /// <param name="fileXlsx">The file XLSX.</param>
        private void saveFile(string fileXlsx, DataTable table)
        {
            OpenXmlExcel oxe = new OpenXmlExcel(fileXlsx);
            oxe.WriteTable("Users", table);
        }

        ///// <summary>
        ///// Writes the HTML.
        ///// </summary>
        ///// <param name="table">The table.</param>
        //private void writeHtml(DataTable table)
        //{
        //    User user = new User();
        //    string folderCommon = this.textEditFolderOutput.Text;
        //    Directory.CreateDirectory(folderCommon);
        //    this.writelog(string.Format("Verifica cartella firme comuni: [{0}]", folderCommon));

        //    string htmlMasterText = File.ReadAllText(this.textEditHtmlMaster.Text);
        //    this.writelog("Lettura HTML master completata");

        //    repositoryItemProgressBar.Maximum = table.Rows.Count;

        //    foreach (DataRow row in table.Rows)
        //    {
        //        StringBuilder htmlMaster = new StringBuilder(htmlMasterText);
        //        user.DataRow = row;

        //        this.writelog("");
        //        this.writelog(string.Format("Processo dati utente:[{0}]", user.Fullname.ToUpper()));
        //        this.writelog(new string('=', 30));

        //        htmlMaster.Replace(User.Labels.Fullname, user.Fullname);
        //        htmlMaster.Replace(User.Labels.Role, user.Role);
        //        htmlMaster.Replace(User.Labels.MobileLabel, user.MobileLabel);
        //        htmlMaster.Replace(User.Labels.MobileNumber, user.MobileNumber);
        //        htmlMaster.Replace(User.Labels.PhoneNumber, user.PhoneNumber);
        //        htmlMaster.Replace(User.Labels.Email, user.Email);
        //        htmlMaster.Replace(User.Labels.Web, user.Web);
        //        htmlMaster.Replace(User.Labels.LogoImage, user.LogoImage);
        //        htmlMaster.Replace(User.Labels.BannerImage, user.BannerImage);
        //        htmlMaster.Replace(User.Labels.BannerLink, user.BannerLink);
        //        htmlMaster.Replace(User.Labels.TextDisclaimer, user.TextDisclaimer);

        //        this.writelog(string.Format("Salva firma nella cartella comune per l'utente: [{0}]", user.Fullname));

        //        string fileHtml = this.getCommonfileHtm(user);
        //        File.WriteAllText(fileHtml, htmlMaster.ToString());

        //        if (!string.IsNullOrEmpty(user.PcName) && barEditItemPwd.EditValue != null && !string.IsNullOrEmpty(barEditItemPwd.EditValue.ToString()))
        //        {
        //            string folderUser = this.textEditUserFolder.Text.Replace(User.Labels.Account, user.Account).Replace(User.Labels.PcName, user.PcName);
        //            this.writelog(string.Format("Tentativo di scrittura nella cartella utente: [{0}]", folderUser));
        //            using (new Impersonator(this.textEditAdmin.Text, this.textEditDomain.Text, barEditItemPwd.EditValue.ToString()))
        //            {
        //                try
        //                {
        //                    this.writelog(string.Format("Utente impersonato: [{0}\\{1}]", this.textEditDomain.Text, this.textEditAdmin.Text));
        //                    Directory.CreateDirectory(folderUser);
        //                    this.writelog("Verifica cartella");
        //                    fileHtml = string.Format(@"{0}\signature.htm", folderUser);
        //                    File.WriteAllText(fileHtml, htmlMaster.ToString());
        //                    this.writelog(string.Format("Scrittura file: [{0}] RIUSCITA.", fileHtml));
        //                    user.SignVersion = this.getHtmlVersion(fileHtml);
        //                    user.OutRowMapping();
        //                    Application.DoEvents();
        //                }
        //                catch (Exception ex)
        //                {
        //                    this.writelog(string.Format("Scrittura file: [{0}] FALLITA.", fileHtml));
        //                    this.writelog(ex.Message);
        //                    ExceptionsRegistry.GetInstance().Add(ex);
        //                }
        //            }
        //        }
        //        this.writelog(new string('=', 30));
        //    }
        //    this.writelog("Generazione firme COMPLETATA");
        //    this.saveFile(this.openedFileXlsx);
        //}

        /// <summary>
        /// Gets the HTML version.
        /// </summary>
        /// <param name="fileHtml">The file HTML.</param>
        /// <returns>System.String.</returns>
        private string getHtmlVersion(string fileHtml)
        {
            string version = null;
            if (!File.Exists(fileHtml))
                return version;
            try
            {
                List<string> lines = File.ReadLines(fileHtml).ToList();
                if (lines.Count > 0)
                {
                    string tag = lines[0].GetTrimForChar('<').GetTrimForChar('>');
                    version = tag.GetTagsString("[", "]")[0];
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return version;
        }

        private void gridControl1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            this.tabPane1.SelectedPage = this.tabNavigationPageUsers;
            this.loadFile(this.textEditDataFile.Text);
        }


        private void writelog(string msgLog, bool showInfo)
        {
            Application.DoEvents();
            Document doc = this.richEditControlLog.Document;
            doc.BeginUpdate();
            doc.AppendText(String.Format("{0} - {1}\r\n", DateTime.Now, msgLog));
            doc.EndUpdate();
            if (showInfo)
                barStaticItemInfo.Caption = msgLog;
            this.gridControl1.RefreshDataSource();
            Thread.Sleep(50);
            Application.DoEvents();

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DataRow row = this.gridView1.GetFocusedDataRow();
            User user = new MyHtmlSign.User(row);
            string fileHtm = this.getCommonfileHtm(user);
            this.webBrowser.Navigate(fileHtm);
        }

        private string getCommonfileHtm(User user)
        {
            string retVal = null;
            retVal = string.Format(@"{0}\{1}.htm", this.textEditFolderOutput.Text, user.Account);
            return retVal;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.saveLayout();
        }

        private void saveLayout()
        {
            new GridAppearance(this.gridView1).SaveLayout(this.getFileLayout());
        }

        private void loadLayout()
        {
            new GridAppearance(this.gridView1).LoadLayout(this.getFileLayout());
        }

        private string getFileLayout()
        {
            return string.Format("{0}\\GridDataUser.layout", Path.GetDirectoryName(Application.ExecutablePath));
        }

        private void textEditAdmin_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            this.writeLabelUser();
        }

        private void writeLabelUser()
        {
            this.barEditItemPwd.Caption = string.Format(@"Pwd: [{0}\{1}]", this.textEditDomain.Text, this.textEditAdmin.Text);
            Application.DoEvents();
        }

        private void textEditDomain_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            this.writeLabelUser();
        }

        private void barButtonItemRun_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            barButtonItemRun.Visibility = BarItemVisibility.OnlyInCustomizing;
            barButtonItemRun.Enabled = false;
            barButtonItemStop.Visibility = BarItemVisibility.Always;
            barButtonItemStop.Enabled = true;
            this.tabPane1.SelectedPage = this.tabNavigationPageLog;
            this.loadFile(this.openedFileXlsx);
            //Start the async operation here
            backgroundWorker.RunWorkerAsync();
        }

        private void barButtonItemOpenXlsx_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Process p = Process.Start(this.textEditDataFile.Text);
        }

        private void barButtonItemOpenHtml_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = this.textEditHtmlEditor.Text;
            startInfo.Arguments = this.textEditHtmlMaster.Text;
            Process.Start(startInfo);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.saveLayout();
            this.loadFile(this.textEditDataFile.Text);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            //DataRow row = this.gridView1.GetFocusedDataRow();
            //if (row != null)
            //{
            //    User user = new User(row);
            //    if (!string.IsNullOrEmpty(user.PcName) && barEditItemPwd.EditValue != null && !string.IsNullOrEmpty(barEditItemPwd.EditValue.ToString()))
            //    {
            //        string folderUser = this.textEditUserFolder.Text.Replace(User.Labels.Account, user.Account).Replace(User.Labels.PcName, user.PcName);
            //        string fileHtml = string.Format(@"{0}\signature.htm", folderUser);

            //        using (new Impersonator(this.textEditAdmin.Text, this.textEditDomain.Text, barEditItemPwd.EditValue.ToString()))
            //        {
            //            bool exist = File.Exists(fileHtml);
            //            if (exist)
            //            {
            //                string[] files = Directory.GetFiles(folderUser);
            //            }
            //        }
            //    }
            //}
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Here you play with the main UI thread
            barEditItemProgress.EditValue = e.ProgressPercentage;
            //barStaticItemInfo.Caption = "Processing..." + barEditItemProgress.EditValue.ToString() + "%";
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //If it was cancelled midway
            if (e.Cancelled)
                barStaticItemInfo.Caption = "Generazione delle firme annullata!";
            else if (e.Error != null)
                barStaticItemInfo.Caption = "Errore nella generazione delle firme.";
            else
                barStaticItemInfo.Caption = "Generazione delle firme completata!";

            barButtonItemRun.Visibility = BarItemVisibility.Always;
            barButtonItemRun.Enabled = true;
            barButtonItemStop.Visibility = BarItemVisibility.OnlyInCustomizing;
            barButtonItemStop.Enabled = false;

            this.loadFile(this.openedFileXlsx);
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            if (!File.Exists(this.openedFileXlsx))
                return;

            DataTable table = this.loadTable(this.openedFileXlsx);

            this.writelogAsync("Avvio generazione firme");

            User user = new User();
            string folderCommon = this.textEditFolderOutput.Text;
            Directory.CreateDirectory(folderCommon);
            this.writelogAsync(string.Format("Verifica cartella firme comuni: [{0}]", folderCommon));
          


            int cnt = 0;
            foreach (DataRow row in table.Rows)
            {


                if (backgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    backgroundWorker.ReportProgress(0);
                    return;
                }

                cnt++;
                backgroundWorker.ReportProgress(cnt);
                
                user.DataRow = row;

                this.writelogAsync("");
                this.writelogAsync(string.Format("Processo dati utente:[{0}]", user.Fullname.ToUpper()));
                this.writelogAsync(new string('=', 30), false);
                string fileMaster = Path.Combine(this.textEditHtmlMaster.Text, user.Master);
                string htmlMasterText = File.ReadAllText(fileMaster);
                StringBuilder htmlMaster = new StringBuilder(htmlMasterText);
                this.writelogAsync($"Lettura HTML master: {fileMaster} completata");

                htmlMaster.Replace(User.Labels.Fullname, user.Fullname);
                htmlMaster.Replace(User.Labels.Role, user.Role);
                htmlMaster.Replace(User.Labels.MobileLabel, user.MobileLabel);
                htmlMaster.Replace(User.Labels.MobileNumber, user.MobileNumber);
                htmlMaster.Replace(User.Labels.PhoneNumber, user.PhoneNumber);
                htmlMaster.Replace(User.Labels.Email, user.Email);
                htmlMaster.Replace(User.Labels.Email02, user.Email02);
                htmlMaster.Replace(User.Labels.Web, user.Web);
                htmlMaster.Replace(User.Labels.Web02, user.Web02);
                htmlMaster.Replace(User.Labels.LogoImage, user.LogoImage);
                htmlMaster.Replace(User.Labels.LogoImage02, user.LogoImage02);
                htmlMaster.Replace(User.Labels.BannerImage, user.BannerImage);
                htmlMaster.Replace(User.Labels.BannerLink, user.BannerLink);
                htmlMaster.Replace(User.Labels.TextDisclaimer, user.TextDisclaimer);

                this.writelogAsync(string.Format("Salva firma nella cartella comune per l'utente: [{0}]", user.Fullname));

                string fileHtml = this.getCommonfileHtm(user);
                File.WriteAllText(fileHtml, htmlMaster.ToString());
                /*
                if (!string.IsNullOrEmpty(user.PcName) && barEditItemPwd.EditValue != null && !string.IsNullOrEmpty(barEditItemPwd.EditValue.ToString()))
                {
                    string folderUser = this.textEditUserFolder.Text.Replace(User.Labels.Account, user.Account).Replace(User.Labels.PcName, user.PcName);
                    this.writelogAsync(string.Format("Tentativo di scrittura nella cartella utente: [{0}]", folderUser));
                    using (new Impersonator(this.textEditAdmin.Text, this.textEditDomain.Text, barEditItemPwd.EditValue.ToString()))
                    {
                        try
                        {
                            Ping ping = new Ping();
                            IPStatus status = default(IPStatus);
                            status = ping.Send(user.PcName).Status;
                            if (status == IPStatus.Success)
                            {
                                this.writelogAsync(string.Format("Utente impersonato: [{0}\\{1}]", this.textEditDomain.Text, this.textEditAdmin.Text));
                                this.writelogAsync(string.Format("Tentativo di accesso a: [{0}]", folderUser));
                                Directory.CreateDirectory(folderUser);
                                this.writelogAsync("Verifica cartella");
                                fileHtml = string.Format(@"{0}\signature.htm", folderUser);
                                File.WriteAllText(fileHtml, htmlMaster.ToString());
                                this.writelogAsync(string.Format("Scrittura file: [{0}] RIUSCITA.", fileHtml));
                                user.SignVersion = this.getHtmlVersion(fileHtml);
                                user.OutRowMapping();
                                this.writelogAsync(string.Format("Versione della firma letta in remoto: [{0}]", user.SignVersion));
                            }
                            else
                            {
                                this.writelogAsync(string.Format("Scrittura file: [{0}] FALLITA.", fileHtml));
                                this.writelogAsync(string.Format("PC [{0}] Non raggiunbile!", user.PcName));
                            }
                            Application.DoEvents();
                        }
                        catch (Exception ex)
                        {
                            this.writelogAsync(string.Format("Scrittura file: [{0}] FALLITA.", fileHtml));
                            this.writelogAsync(ex.Message);
                            ExceptionsRegistry.GetInstance().Add(ex);
                        }
                    }
                }
                */
                this.writelogAsync(new string('=', 30), false);
            }


            this.saveFile(this.openedFileXlsx, table);

            this.writelogAsync("Generazione firme COMPLETATA");

            backgroundWorker.ReportProgress(table.Rows.Count);
            //this.writeHtml((DataTable)this.gridControl1.DataSource);
        }

        private void writelogAsync(string message)
        {
            bool showInfo = !string.IsNullOrEmpty(message);
            this.writelogAsync(message, showInfo);
        }

        private void writelogAsync(string message, bool showInfo)
        {
            this.Invoke(this.writelogDelegate, new object[] { message, showInfo });
        }

        private void barButtonItemStop_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (backgroundWorker.IsBusy)
            {
                //Stop/Cancel the async operation here
                backgroundWorker.CancelAsync();
            }
        }

        private void barButtonItemExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.gridView1.ExpandAllGroups();
        }

        private void barButtonItemShowPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.splitContainerControl.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both;
            this.barButtonItemHidePreview.Visibility = BarItemVisibility.Always;
            this.barButtonItemShowPreview.Visibility = BarItemVisibility.OnlyInCustomizing;
        }

        private void barButtonItemHidePreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.splitContainerControl.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            this.barButtonItemHidePreview.Visibility = BarItemVisibility.OnlyInCustomizing;
            this.barButtonItemShowPreview.Visibility = BarItemVisibility.Always;
        }
    }
}
