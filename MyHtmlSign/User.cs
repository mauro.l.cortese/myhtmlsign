using System;
using System.Data;
using Mlc.Data;

namespace MyHtmlSign
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public class User : DsRowMapBase
	{
		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public User()
		{ }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataRow">DataRow su cui mappare l'istanza</param>
        public User(DataRow dataRow)
            : base(dataRow)
        { }
		#endregion

		#region Proprietà
        protected String account = default(String);
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Account { get { return this.account; } set { this.account = value; } }

        protected String fullname = default(String);
		[Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Fullname { get { return this.fullname; } set { this.fullname = value; } }

        protected String role = default(String);
		[Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Role { get { return this.role; } set { this.role = value; } }

        protected String mobileLabel = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String MobileLabel { get { return this.mobileLabel; } set { this.mobileLabel = value; } }

        protected String mobileNumber = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String MobileNumber { get { return this.mobileNumber; } set { this.mobileNumber = value; } }

        protected String phoneNumber = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String PhoneNumber { get { return this.phoneNumber; } set { this.phoneNumber = value; } }

        protected String email = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Email { get { return this.email; } set { this.email = value; } }

        protected String email02 = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Email02 { get { return this.email02; } set { this.email02 = value; } }

        protected String web = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Web { get { return this.web; } set { this.web = value; } }

        protected String web02 = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Web02 { get { return this.web02; } set { this.web02 = value; } }

        protected String department = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Department { get { return this.department; } set { this.department = value; } }

        protected String bannerImage = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String BannerImage { get { return this.bannerImage; } set { this.bannerImage = value; } }

        protected String bannerLink = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String BannerLink { get { return this.bannerLink; } set { this.bannerLink = value; } }

        protected String logoImage = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String LogoImage { get { return this.logoImage; } set { this.logoImage = value; } }

        protected String logoImage02 = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String LogoImage02 { get { return this.logoImage02; } set { this.logoImage02 = value; } }

        protected String textDisclaimer = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String TextDisclaimer { get { return this.textDisclaimer; } set { this.textDisclaimer = value; } }

        protected String master = default(String);
        [Field(Visible = true, HAlign = HorzAlignment.Left)]
        public virtual String Master { get { return this.master; } set { this.master = value; } }

        //protected String pcName = default(String);
        //[Field(Visible = true, HAlign = HorzAlignment.Left)]
        //public virtual String PcName { get { return this.pcName; } set { this.pcName = value; } }

        //protected String signVersion = default(String);
        //[Field(Visible = true, HAlign = HorzAlignment.Left)]
        //public virtual String SignVersion { get { return this.signVersion; } set { this.signVersion = value; } }


        #endregion

        #region Tipi nidificati
        /// <summary>Costanti pubbliche nomi prorietà</summary>
        public class Properties
		{
            public const string Account = "Account";
            public const string Fullname = "Fullname";
            public const string Role = "Role";
            public const string MobileLabel = "MobileLabel";
            public const string MobileNumber = "MobileNumber";
            public const string PhoneNumber = "PhoneNumber";
            public const string Email = "Email";
            public const string Email02 = "Email02";
            public const string Web = "Web";
            public const string Web02 = "Web02";
            public const string Department = "Department";
            public const string BannerImage = "BannerImage";
            public const string BannerLink = "BannerLink";
            public const string LogoImage = "LogoImage";
            public const string LogoImage02 = "LogoImage02";
            public const string TextDisclaimer = "TextDisclaimer";
            public const string PcName = "PcName";
            public const string SignVersion = "SignVersion";
        }

        public class Labels
        {
            public const string Account = "{Account}";
            public const string Fullname = "{Fullname}";
            public const string Role = "{Role}";
            public const string MobileLabel = "{MobileLabel}";
            public const string MobileNumber = "{MobileNumber}";
            public const string PhoneNumber = "{PhoneNumber}";
            public const string Email = "{Email}";
            public const string Email02 = "{Email02}";
            public const string Web = "{Web}";
            public const string Web02 = "{Web02}";
            public const string Department = "{Department}";
            public const string BannerImage = "{BannerImage}";
            public const string BannerLink = "{BannerLink}";
            public const string LogoImage = "{LogoImage}";
            public const string LogoImage02 = "{LogoImage02}";
            public const string TextDisclaimer = "{TextDisclaimer}";
            public const string PcName = "{PcName}";
            public const string SignVersion = "{SignVersion}";
        }
        #endregion
    }
}
